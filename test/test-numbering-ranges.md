<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

### Speed tests range 1 -10,000

### Server tests: 10,001 - 10,200

### Helper TTL: 10,201 - 10,300

### Helper Journal: 10,301 - 10,400

### Helper Session: 10,401 - 10,500


### API tests 

#### General: 11,000

#### String: 11,500

#### Hash: 12,500


### Socket tests

#### General: 101,000

#### String: 101,500

#### Hash: 112,500

