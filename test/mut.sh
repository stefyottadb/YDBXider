#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property        #
#    of its copyright holder(s), and is made available          #
#    under a license.  If you do not know the terms of          #
#    the license, please stop and do not read further.          #
#                                                               #
#################################################################

exitCode=0

# Switch to this directory to store the jobs files (so we can get them out for artifcats)
pushd /mylogs

if ! yottadb -r ^%ydbxiderTestApiString
then
	exitCode=1
fi
mv _ydbxiderServer.mjo ydbxiderTestApiString.mjo
mv _ydbxiderServer.mje ydbxiderTestApiString.mje

if ! yottadb -r ^%ydbxiderTestApiHash
then
	exitCode=1
fi
mv _ydbxiderServer.mjo ydbxiderTestApiHash.mjo
mv _ydbxiderServer.mje ydbxiderTestApiHash.mje

if ! yottadb -r ^%ydbxiderTestApiTtl
then
	exitCode=1
fi
mv _ydbxiderServer.mjo ydbxiderTestApiTtl.mjo
mv _ydbxiderServer.mje ydbxiderTestApiTtl.mje

popd # /mylogs

exit $exitCode
