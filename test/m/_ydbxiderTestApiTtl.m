;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#	This source code contains the intellectual property	         #
;#	of its copyright holder(s), and is made available            #
;#	under a license.  If you do not know the terms of            #
;#	the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
	;
%ydbxiderTestApiTtl
	; Requires M-Unit
	;
test if $text(^%ut)="" quit
	do en^%ut($text(+0),3)
	;
	write !
	;
	quit
	;
STARTUP
	do STARTUP^%ydbxiderTestUtils
	;
	quit
	;
	;
SHUTDOWN
	do SHUTDOWN^%ydbxiderTestUtils
	;
	quit
	;
	;
TTL0	;@test --------------------
	;
	quit
TTL1	;@test --------------------API <string> TTL
	;
	quit
TTL2	;@test --------------------
	;
	quit
	;
	;
T12500	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T12501	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T12502	;@test with 1 key with no expiration
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="ttlKey"
	set xider("value")="ttlKeyValue"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key only
	set xider("key")="ttlKey"
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key exists, but has no TTL")
	;
	quit
	;
	;
T12503	;@test with 1 key that doesn't exists
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="ttlKeyNotExists"
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key doesn't exists")
	;
	quit
	;
	;
T12504	;@test with 1 key with expiration
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="ttlKey"
	set xider("value")="ttlKeyValue"
	set xider("params","EX")=5
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key only
	set xider("key")="ttlKey"
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,4,"4 seconds expiration left")
	;
	hang 2
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 seconds expiration left")
	;
	hang 4
	;
	;execute the call
	set ret=$$TTL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key is gone")
	;
	quit
	;
	;
T12505	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do TTL^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
PTTL0	;@test --------------------
	;
	quit
PTTL1	;@test --------------------API <string> PTTL
	;
	quit
PTTL2	;@test --------------------
	;
	quit
	;
	;
T12525	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T12526	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T12527	;@test with 1 key with no expiration
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="pttlKey"
	set xider("value")="pttlKeyValue"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key only
	set xider("key")="pttlKey"
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key exists, but has no TTL")
	;
	quit
	;
	;
T12528	;@test with 1 key that doesn't exists
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="pttlKeyNotExists"
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key doesn't exists")
	;
	quit
	;
	;
T12529	;@test with 1 key with expiration
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="pttlKey"
	set xider("value")="pttlKeyValue"
	set xider("params","EX")=5
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key only
	set xider("key")="pttlKey"
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,4999,"4999 milliseconds expiration left")
	;
	hang 2
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,2999,"2999 milliseconds expiration left")
	;
	hang 4
	;
	;execute the call
	set ret=$$PTTL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key is gone")
	;
	quit
	;
	;
T12530	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do PTTL^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
