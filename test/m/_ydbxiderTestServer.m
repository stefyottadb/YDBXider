;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;# This source code contains the intellectual property	         #
;# of its copyright holder(s), and is made available             #
;# under a license.  If you do not know the terms of             #
;# the license, please stop and do not read further.             #
;#                                                               #
;#################################################################
%ydbxiderTestServer ;
	; Requires M-Unit
	;
test if $text(^%ut)="" quit
	do en^%ut($text(+0),3)
	quit
	;
test12	;@TEST testing the test new zhalt
	do eq^%ut(1,1,"this should pass")
	;
	quit
	;
	;
	; -----------------------------------------
	; UTILS
	; -----------------------------------------
getSessions
	zwr ^%ydbxider("S",*)
	;
	quit
