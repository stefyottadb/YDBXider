;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;# This source code contains the intellectual property           #
;# of its copyright holder(s), and is made available             #
;# under a license.  If you do not know the terms of             #
;# the license, please stop and do not read further.             #
;#                                                               #
;#################################################################
	;
%ydbxiderTestApiHash
	; Requires M-Unit
	;
test if $text(^%ut)="" quit
	do en^%ut($text(+0),3)
	;
	write !
	;
	quit
	;
STARTUP
	do STARTUP^%ydbxiderTestUtils
	;
	quit
	;
	;
SHUTDOWN
	do SHUTDOWN^%ydbxiderTestUtils
	;
	quit
	;
	;
HSET0	;@test ------------------
	quit
	;
	;
HSET1	;@test ------------------API <hash> HSET
	quit
	;
	;
HSET2	;@test ------------------
	quit
	;
	;
T13000	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13002	;@test with no data
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key only
	set xider("key")="hset key"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no data")
	;
	quit
	;
	;
T13003	;@test with data with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hset key"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"value")="myvalue2"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,-4,"some data has no field")
	;
	quit
	;
	;
T13004	;@test with field + key > 975 chars
	new lField,lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lField,lKey)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+50 set lField=lField_"x"
	for ix=1:1:100 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	; set the data
	set xider("data",1,"field")="field1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")=lField
	set xider("data",2,"value")="myvalue2"
	;l
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,-5,"some field / key combination is too long")
	;
	quit
	;
	;
T13005	;@test with data with no data
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hset key"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield2"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,-6,"some data has no value")
	;
	quit
	;
	;
T13006	;@test with key as string data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="strkey"
	set xider("value")="strvalue"
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="strkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,-7,"somkey exists, but not hash data-type")
	;
	quit
	;
	;
T13007	;@test with non-existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hsetkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield2"
	set xider("data",2,"value")="myvalue2"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 records created")
	;
	quit
	;
	;
T13008	;@test with existing key and 1 existing record
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hsetkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield3"
	set xider("data",2,"value")="myvalue3"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,1,"1 records created")
	;
	quit
	;
	;
T13009	;@test with existing key and existing records
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hsetkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield3"
	set xider("data",2,"value")="myvalue3"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	; verify response
	do eq^%ut(ret,0,"0 records created")
	;
	quit
	;
	;
T13010	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HSET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HSETNX0	;@test ------------------
	quit
	;
	;
HSETNX1	;@test --------------API <hash> HSETNX
	quit
	;
	;
HSETNX2	;@test ------------------
	quit
	;
	;
T13025	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13027	;@test with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no field")
	;
	quit
	;
	;
T13028	;@test with field / key > 900 chars
	new lField,lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lKey,lField)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the data
	set xider("key")=lKey
	set xider("field")=lField
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field + key length > 900")
	;
	quit
	;
	;
T13029	;@test with no value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-5,"no value")
	;
	quit
	;
	;
T13030	;@test with not existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="setnxfield"
	set xider("value")="setnxvalue"
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,0,"key not found")
	;
	quit
	;
	;
T13031	;@test with existing key, but <string> data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("value")="stringvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("field")="setnxfield"
	set xider("value")="setnxvalue"
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-7,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13032	;@test with existing key and existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hashkey"
	set xider("data",1,"field")="hashfield1"
	set xider("data",1,"value")="hashvalue1"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hashkey"
	set xider("field")="hashfield1"
	set xider("value")="setnxvalue"
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-8,"field already exists")
	;
	quit
	;
	;
T13033	;@test with existing key, but no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hashkey"
	set xider("data",1,"field")="hashfield1"
	set xider("data",1,"value")="hashvalue1"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hashkey"
	set xider("field")="hashfield2"
	set xider("value")="setnxvalue"
	;
	;execute the call
	set ret=$$HSETNX^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	;
	quit
	;
	;
T13034	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HSETNX^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HGET0	;@test ------------------
	quit
	;
	;
HGET1	;@test --------------API <hash> HGET
	quit
	;
	;
HGET2	;@test ------------------
	quit
	;
	;
T13050	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13051	;@test with field / key > 900 chars
	new lField,lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lKey,lField)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the data
	set xider("key")=lKey
	set xider("field")=lField
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field + key length > 900")
	;
	quit
	;
	;
T13052	;@test with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no field")
	;
	quit
	;
	;
T13053	;@test with field > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the data
	set xider("key")="setnxkey"
	set xider("field")=lKey
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13054	;@test with not existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnx2key"
	set xider("field")="setnxfield"
	set xider("value")="setnxvalue"
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key not found")
	;
	quit
	;
	;
T13055	;@test with existing key, but <string> data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("value")="stringvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13056	;@test with non existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("data",1,"field")="stringfield"
	set xider("data",1,"value")="stringvalue"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,-7,"field doesn't exists")
	;
	quit
	;
	;
T13057	;@test with existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("data",1,"field")="stringfield"
	set xider("data",1,"value")="stringvalue"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("field")="stringfield"
	;
	;execute the call
	set ret=$$HGET^xider
	;
	; verify response
	do eq^%ut(ret,1,"data is in xider(""ret"")")
	;
	quit
	;
	;
T13058	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HGET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HDEL0	;@test ------------------
	quit
	;
	;
HDEL1	;@test --------------API <hash> HDEL
	quit
	;
	;
HDEL2	;@test ------------------
	quit
	;
	;
T13075	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13077	;@test with no fields
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="hashnofields"
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no fields")
	;
	quit
	;
	;
T13078	;@test with field > 975 chars
	new lField,lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lField,lKey)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lField=lField_"x"
	for ix=1:1:50 set lKey=lKey_"x"
	;
	; set the fields
	set xider("key")=lKey
	set xider("fields",1)=lField
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field too long")
	;
	quit
	;
	;
T13079	;@test with bad key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="hashnofields"
	set xider("fields",1)="myfield"
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key not found")
	;
	quit
	;
	;
T13080	;@test with <string> key
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	set xider("key")="mystringkey2"
	set xider("value")="mystrvalue"
	do SET^xider
	;
	; set the data
	set xider("key")="mystringkey2"
	set xider("fields",1)="myfield"
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13081	;@test with 3 fields, delete only 2
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	set xider("key")="T13081"
	set xider("data",1,"field")="T13081-1"
	set xider("data",1,"value")="T13081-1-d"
	set xider("data",2,"field")="T13081-2"
	set xider("data",2,"value")="T13081-2-d"
	set xider("data",3,"field")="T13081-3"
	set xider("data",3,"value")="T13081-3-d"
	;
	do HSET^xider
	;
	kill xider
	;
	; set the data
	set xider("key")="T13081"
	set xider("fields",1)="T13081-1"
	set xider("fields",2)="T13081-2"
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 records deleted")
	do eq^%ut(^%ydbxiderK("T13081"),1,"1 record left")
	;
	quit
	;
	;
T13082	;@test with 3 fields, delete last record, skip non existing
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	; set the data
	set xider("key")="T13081"
	set xider("fields",1)="T13081-1"
	set xider("fields",2)="T13081-2"
	set xider("fields",2)="T13081-3"
	;
	;execute the call
	set ret=$$HDEL^xider
	;
	; verify response
	do eq^%ut(ret,1,"1 records deleted")
	do eq^%ut($data(^%ydbxiderK("T13081")),0,"got deleted")
	;
	quit
	;
	;
T13083	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HDEL^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HLEN0	;@test ------------------
	quit
	;
	;
HLEN1	;@test --------------API <hash> HLEN
	quit
	;
	;
HLEN2	;@test ------------------
	quit
	;
	;
T13100	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HLEN^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13101	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$HLEN^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13102	;@test with bad key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="nonexisthlen"
	;execute the call
	set ret=$$HLEN^xider
	;
	; verify response
	do eq^%ut(ret,0,"key not found")
	;
	quit
	;
	;
T13103	;@test with key as string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="mystringkey2"
	;
	;execute the call
	set ret=$$HLEN^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13104	;@test with 3 fields
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	set xider("key")="T13081"
	set xider("data",1,"field")="T13081-1"
	set xider("data",1,"value")="T13081-1-d"
	set xider("data",2,"field")="T13081-2"
	set xider("data",2,"value")="T13081-2-d"
	set xider("data",3,"field")="T13081-3"
	set xider("data",3,"value")="T13081-3-d"
	;
	do HSET^xider
	;
	kill xider
	;
	set xider("key")="T13081"
	;
	;execute the call
	set ret=$$HLEN^xider
	;

	; verify response
	do eq^%ut(ret,3,"3 records found")
	;
	quit
	;
	;
T13105	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HLEN^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HSTRLEN0	;@test ------------------
	quit
	;
	;
HSTRLEN1	;@test ------------API <hash> HSTRLEN
	quit
	;
	;
HSTRLEN2	;@test ------------------
	quit
	;
	;
T13125	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13127	;@test with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no field")
	;
	quit
	;
	;
T13128	;@test with key / field > 975 chars
	new lKey,lField,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lKey,lField)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the data
	set xider("key")=lKey
	set xider("field")=lField
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13129	;@test with not existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnx2key"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key not found")
	;
	quit
	;
	;
T13130	;@test with existing key, but <string> data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("value")="stringvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13131	;@test with non existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("data",1,"field")="stringfield"
	set xider("data",1,"value")="stringvalue"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-7,"field doesn't exists")
	;
	quit
	;
	;
T13132	;@test with existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("data",1,"field")="stringfield"
	set xider("data",1,"value")="stringvalue"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("field")="stringfield"
	;
	;execute the call
	set ret=$$HSTRLEN^xider
	;
	; verify response
	do eq^%ut(ret,11,"data is in xider(""ret"")")
	;
	quit
	;
	;
T13133	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HSTRLEN^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HKEYS0	;@test ------------------
	quit
	;
	;
HKEYS1	;@test ------------API <hash> HKEYS
	quit
	;
	;
HKEYS2	;@test ------------------
	quit
	;
	;
T13150	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HKEYS^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13151	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$HKEYS^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13152	;@test with not existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hkeykey"
	;
	;execute the call
	set ret=$$HKEYS^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key not found")
	;
	quit
	;
	;
T13153	;@test with existing key, but <string> data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("value")="stringvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	;
	;execute the call
	set ret=$$HKEYS^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13154	;@test with 3 keys hash
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="hkey key"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield2"
	set xider("data",2,"value")="myvalue2"
	set xider("data",3,"field")="myfield3"
	set xider("data",3,"value")="myvalue3"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	kill xider
	;
	; set the key only
	set xider("key")="hkey key"
	;
	;execute the call
	set ret=$$HKEYS^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut(xider("ret",1),"myfield1","success")
	do eq^%ut(xider("ret",2),"myfield2","success")
	do eq^%ut(xider("ret",3),"myfield3","success")
	;
	quit
	;
	;
T13155	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HKEYS^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HVALS0	;@test ------------------
	quit
	;
	;
HVALS1	;@test ------------API <hash> HVALS
	quit
	;
	;
HVALS2	;@test ------------------
	quit
	;
	;
T13175	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HVALS^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13176	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$HVALS^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13177	;@test with not existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hkeykey"
	;
	;execute the call
	set ret=$$HVALS^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key not found")
	;
	quit
	;
	;
T13178	;@test with existing key, but <string> data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("value")="stringvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	;
	;execute the call
	set ret=$$HVALS^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13179	;@test with 3 keys hash
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="hkey key"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield2"
	set xider("data",2,"value")="myvalue2"
	set xider("data",3,"field")="myfield3"
	set xider("data",3,"value")="myvalue3"
	;
	;execute the call
	set ret=$$HSET^xider
	;
	kill xider
	;
	; set the key only
	set xider("key")="hkey key"
	;
	;execute the call
	set ret=$$HVALS^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut(xider("ret",1),"myvalue1","success")
	do eq^%ut(xider("ret",2),"myvalue2","success")
	do eq^%ut(xider("ret",3),"myvalue3","success")
	;
	quit
	;
	;
T13180	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HVALS^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HEXISTS0	;@test ------------------
	quit
	;
	;
HEXISTS1	;@test --------------API <hash> HEXISTS
	quit
	;
	;
HEXISTS2	;@test ------------------
	quit
	;
	;
T13200	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13202	;@test with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no field")
	;
	quit
	;
	;
T13203	;@test with field > 975 chars
	new lKey,lField,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lField,lKey)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the data
	set xider("key")=lKey
	set xider("field")=lField
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13204	;@test with not existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnx99key"
	set xider("field")="setnxfield"
	set xider("value")="setnxvalue"
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key not found")
	;
	quit
	;
	;
T13205	;@test with existing key, but <string> data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("value")="stringvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13206	;@test with non existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("data",1,"field")="stringfield"
	set xider("data",1,"value")="stringvalue"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("field")="setnxfield"
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-7,"field doesn't exists")
	;
	quit
	;
	;
T13207	;@test with existing field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("data",1,"field")="stringfield"
	set xider("data",1,"value")="stringvalue"
	;
	set ret=$$HSET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="stringkey2"
	set xider("field")="stringfield"
	;
	;execute the call
	set ret=$$HEXISTS^xider
	;
	; verify response
	do eq^%ut(ret,0,"field exists")
	;
	quit
	;
	;
T13208	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HEXISTS^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HMSET0	;@test ------------------
	quit
	;
	;
HMSET1	;@test ------------------API <hash> HMSET
	quit
	;
	;
HMSET2	;@test ------------------
	quit
	;
	;
T13225	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13227	;@test with no data
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key only
	set xider("key")="hset key"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no data")
	;
	quit
	;
	;
T13228	;@test with data with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hset key"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"value")="myvalue2"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,-4,"some data has no field")
	;
	quit
	;
	;
T13229	;@test with field > 975 chars
	new lField,lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lKey,lField)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lField=lField_"x"
	for ix=1:1:50 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	; set the data
	set xider("data",1,"field")="field1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")=lField
	set xider("data",2,"value")="myvalue2"
	;l
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,-5,"some field is too long")
	;
	quit
	;
	;
T13230	;@test with data with no data
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hset key"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield2"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,-6,"some data has no value")
	;
	quit
	;
	;
T13231	;@test with key as string data-type
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="strkey"
	set xider("value")="strvalue"
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="strkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,-7,"somkey exists, but not hash data-type")
	;
	quit
	;
	;
T13232	;@test with non-existing key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hmsetkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield2"
	set xider("data",2,"value")="myvalue2"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 records created")
	;
	quit
	;
	;
T13233	;@test with existing key and 1 existing record
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hmsetkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield3"
	set xider("data",2,"value")="myvalue3"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,1,"1 records created")
	;
	quit
	;
	;
T13234	;@test with existing key and existing records
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="hsetkey"
	;
	; set the data
	set xider("data",1,"field")="myfield1"
	set xider("data",1,"value")="myvalue1"
	set xider("data",2,"field")="myfield3"
	set xider("data",2,"value")="myvalue3"
	;
	;execute the call
	set ret=$$HMSET^xider
	;
	; verify response
	do eq^%ut(ret,0,"0 records created")
	;
	quit
	;
	;
T13235	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HMSET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HMGET0	;@test ------------------
	quit
	;
	;
HMGET1	;@test --------------API <hash> HMGET
	quit
	;
	;
HMGET2	;@test ------------------
	quit
	;
	;
T13250	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HMGET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13252	;@test with no fields
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="hashnofields"
	;
	;execute the call
	set ret=$$HMGET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no fields")
	;
	quit
	;
	;
T13253	;@test with field > 975 chars
	new lField,lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lKey,lField)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the fields
	set xider("key")=lKey
	set xider("fields",1)=lField
	;
	;execute the call
	set ret=$$HMGET^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field too long")
	;
	quit
	;
	;
T13254	;@test with bad key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="hashnofields"
	set xider("fields",1)="myfield"
	;
	;execute the call
	set ret=$$HMGET^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key not found")
	;
	quit
	;
	;
T13255	;@test with <string> key
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	set xider("key")="mystringkey2"
	set xider("value")="mystrvalue"
	do SET^xider
	;
	; set the data
	set xider("key")="mystringkey2"
	set xider("fields",1)="myfield"
	;
	;execute the call
	set ret=$$HMGET^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13256	;@test with <string> key
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	; set the data
	set xider("key")="hkey key"
	set xider("fields",1)="myfield"
	set xider("fields",2)="myfield1"
	set xider("fields",3)="myfield3"
	;
	;execute the call
	set ret=$$HMGET^xider
	;
	; verify response
	do eq^%ut(ret,0,"key exists, but not hash data-type")
	do eq^%ut(xider("ret",1),"","field not found")
	do eq^%ut(xider("ret",2),"myvalue1","field found")
	do eq^%ut(xider("ret",3),"myvalue3","field found")
	;
	quit
	;
	;
T13257	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HMGET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HGETALL0	;@test ------------------
	quit
	;
	;
HGETALL1	;@test --------------API <hash> HGETALL
	quit
	;
	;
HGETALL2	;@test ------------------
	quit
	;
	;
T13275	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HGETALL^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13276	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$HGETALL^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13277	;@test with bad key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="hashnofields"
	set xider("fields",1)="myfield"
	;
	;execute the call
	set ret=$$HGETALL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key not found")
	;
	quit
	;
	;
T13278	;@test with <string> key
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	set xider("key")="mystringkey2"
	set xider("value")="mystrvalue"
	do SET^xider
	;
	; set the data
	set xider("key")="mystringkey2"
	;
	;execute the call
	set ret=$$HGETALL^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13279	;@test with valid key
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	; set the data
	set xider("key")="T13081"
	;
	;execute the call
	set ret=$$HGETALL^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut(xider("ret","T13081-1"),"T13081-1-d","first entry")
	do eq^%ut(xider("ret","T13081-2"),"T13081-2-d","second entry")
	do eq^%ut(xider("ret","T13081-3"),"T13081-3-d","third entry")
	;
	quit
	;
	;
T13280	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HGETALL^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HRANDFIELD0	;@test ------------------
	quit
	;
	;
HRANDFIELD1	;@test --------------API <hash> HRANDFIELD
	quit
	;
	;
HRANDFIELD2	;@test ------------------
	quit
	;
	;
T13300	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13301	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13302	;@test with count as string
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="testkey"
	set xider("count")="this is a string"
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-3,"invalid count")
	;
	quit
	;
	;
T13303	;@test with count as empty string
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="testkey"
	set xider("count")=""
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-3,"invalid count")
	;
	quit
	;
	;
T13304	;@test with count as 0
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="T13081"
	set xider("count")=0
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut($data(xider("ret")),1,"success")
	;
	quit
	;
	;
T13305	;@test with withValues and no count
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="testkey"
	set xider("withValues")=""
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-4,"invalid count")
	;
	quit
	;
	;
T13306	;@test with bad key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="hashnofields"
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key not found")
	;
	quit
	;
	;
T13307	;@test with <string> key
	new ret
	;
	; init the API
	do init^xider
	;
	kill xider
	;
	set xider("key")="mystringkey2"
	set xider("value")="mystrvalue"
	do SET^xider
	;
	; set the data
	set xider("key")="mystringkey2"
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key exists, but not hash data-type")
	;
	quit
	;
	;
T13308	;@test with valid key, no count
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="T13081"
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut($data(xider("ret",1)),1,"success")
	;
	quit
	;
	;
T13309	;@test with valid key and positive count > # of fields
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; add more entries
	set xider("key")="T13081"
	set xider("data",1,"field")="T13081-4"
	set xider("data",2,"field")="T13081-5"
	set xider("data",3,"field")="T13081-6"
	set xider("data",4,"field")="T13081-7"
	set xider("data",5,"field")="T13081-8"
	set xider("data",6,"field")="T13081-9"
	set xider("data",7,"field")="T13081-10"
	set xider("data",1,"value")="T13081-4-d"
	set xider("data",2,"value")="T13081-5-d"
	set xider("data",3,"value")="T13081-6-d"
	set xider("data",4,"value")="T13081-7-d"
	set xider("data",5,"value")="T13081-8-d"
	set xider("data",6,"value")="T13081-9-d"
	set xider("data",7,"value")="T13081-10-d"
	set ret=$$HSET^xider()
	;
	kill xider
	set xider("key")="T13081"
	set xider("count")=20
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut(xider("ret",1),"T13081-1","success")
	do eq^%ut(xider("ret",2),"T13081-10","success")
	do eq^%ut(xider("ret",3),"T13081-2","success")
	do eq^%ut(xider("ret",4),"T13081-3","success")
	do eq^%ut(xider("ret",5),"T13081-4","success")
	do eq^%ut(xider("ret",6),"T13081-5","success")
	do eq^%ut(xider("ret",7),"T13081-6","success")
	do eq^%ut(xider("ret",8),"T13081-7","success")
	do eq^%ut(xider("ret",9),"T13081-8","success")
	do eq^%ut(xider("ret",10),"T13081-9","success")
	;
	quit
	;
	;
T13310	;@test with valid key and positive count < # of fields
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="T13081"
	set xider("count")=5
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut($data(xider("ret")),11,"success")
	;
	quit
	;
	;
T13311	;@test with valid key and negative count
	new ret,cnt
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="T13081"
	set xider("count")=-55
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	set cnt=0,ix="" for  set ix=$order(xider("ret",ix)) quit:ix=""  set cnt=cnt+1
	do eq^%ut(cnt,-xider("count"),"success")
	;
	quit
	;
	;
T13312	;@test with count > # of fields && withValues
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="T13081"
	set xider("count")=20
	set xider("withValues")=""
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut(xider("ret",1,"T13081-1"),"T13081-1-d","success")
	do eq^%ut(xider("ret",2,"T13081-10"),"T13081-10-d","success")
	do eq^%ut(xider("ret",3,"T13081-2"),"T13081-2-d","success")
	do eq^%ut(xider("ret",4,"T13081-3"),"T13081-3-d","success")
	do eq^%ut(xider("ret",5,"T13081-4"),"T13081-4-d","success")
	do eq^%ut(xider("ret",6,"T13081-5"),"T13081-5-d","success")
	do eq^%ut(xider("ret",7,"T13081-6"),"T13081-6-d","success")
	do eq^%ut(xider("ret",8,"T13081-7"),"T13081-7-d","success")
	do eq^%ut(xider("ret",9,"T13081-8"),"T13081-8-d","success")
	do eq^%ut(xider("ret",10,"T13081-9"),"T13081-9-d","success")
	;
	quit
	;
	;
T13313	;@test with count < # of fields && withValues
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="T13081"
	set xider("count")=5
	set xider("withValues")=""
	;
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	do eq^%ut($zlength(xider("ret",1,$order(xider("ret",1,""))))>0,1,"success")
	do eq^%ut($zlength(xider("ret",2,$order(xider("ret",2,""))))>0,1,"success")
	do eq^%ut($zlength(xider("ret",3,$order(xider("ret",3,""))))>0,1,"success")
	do eq^%ut($zlength(xider("ret",4,$order(xider("ret",4,""))))>0,1,"success")
	do eq^%ut($zlength(xider("ret",5,$order(xider("ret",5,""))))>0,1,"success")
	;
	quit
	;
	;
T13314	;@test with negative count && withValues
	new ret,cnt
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="T13081"
	set xider("count")=-55
	set xider("withValues")=""
	;execute the call
	set ret=$$HRANDFIELD^xider
	;
	; verify response
	do eq^%ut(ret,0,"success")
	set cnt=0,ix="" for  set ix=$order(xider("ret",ix)) quit:ix=""  set cnt=cnt+1
	do eq^%ut(cnt,-xider("count"),"success")
	do eq^%ut($zlength(xider("ret",1,$order(xider("ret",1,""))))>0,1,"success")
	;
	quit
	;
	;
T13315	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HRANDFIELD^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HINCRBY0	;@test ------------------
	quit
	;
	;
HINCRBY1	;@test --------------API <hash> HINCRBY
	quit
	;
	;
HINCRBY2	;@test ------------------
	quit
	;
	;
T13325	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13327	;@test with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no field")
	;
	quit
	;
	;
T13328	;@test with field > 975 chars
	new lKey,lField,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lKey,lField)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the data
	set xider("key")=lKey
	set xider("field")=lField
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13329	;@test no increment provided
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-5,"no increment")
	;
	quit
	;
	;
T13330	;@test increment as string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	set xider("increment")="test"
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-6,"no increment")
	;
	quit
	;
	;
T13331	;@test increment as empty string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	set xider("increment")=""
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-5,"no increment")
	;
	quit
	;
	;
T13332	;@test increment as 0
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	set xider("increment")=0
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-6,"no increment")
	;
	quit
	;
	;
T13333	;@test against a string key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="mystringkey2"
	set xider("field")="incrtest"
	set xider("increment")=2
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-7,"data-type not a hash")
	;
	quit
	;
	;
T13334	;@test against a string value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="T13081"
	set xider("field")="T13081-1"
	set xider("increment")=2
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-8,"value not integer")
	;
	quit
	;
	;
T13340	;@test increment by 1 on non ex field in not ex key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hincrbykey"
	set xider("field")="hincrbyfield"
	set xider("increment")=1
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),1,"value is 1")
	do eq^%ut(^%ydbxiderK("hincrbykey","hincrbyfield"),1,"value is 1")
	;
	quit
	;
	;
T13341	;@test increment by 5 on existing node
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hincrbykey"
	set xider("field")="hincrbyfield"
	set xider("increment")=5
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),6,"value is 6")
	do eq^%ut(^%ydbxiderK("hincrbykey","hincrbyfield"),6,"value is 6")

	;
	quit
	;
	;
T13342	;@test decrement by 3 on existing node
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hincrbykey"
	set xider("field")="hincrbyfield"
	set xider("increment")=-3
	;
	;execute the call
	set ret=$$HINCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),3,"value is 3")
	do eq^%ut(^%ydbxiderK("hincrbykey","hincrbyfield"),3,"value is 3")

	;
	quit
	;
	;
T13343	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HINCRBY^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
HINCRBYFLOAT0	;@test ------------------
	quit
	;
	;
HINCRBYFLOAT1	;@test --------------API <hash> HINCRBYFLOAT
	quit
	;
	;
HINCRBYFLOAT2	;@test ------------------
	quit
	;
	;
T13350	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T13352	;@test with no field
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no field")
	;
	quit
	;
	;
T13353	;@test with field > 975 chars
	new lKey,lField,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set (lField,lKey)=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	for ix=1:1:50 set lField=lField_"x"
	;
	; set the data
	set xider("key")=lKey
	set xider("field")=lField
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-4,"field length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T13354	;@test no increment provided
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-5,"no increment")
	;
	quit
	;
	;
T13355	;@test increment as string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	set xider("increment")="test"
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-6,"no increment")
	;
	quit
	;
	;
T13356	;@test increment as empty string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	set xider("increment")=""
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-5,"no increment")
	;
	quit
	;
	;
T13357	;@test increment as 0
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="setnxkey"
	set xider("field")="incrtest"
	set xider("increment")=0
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-6,"no increment")
	;
	quit
	;
	;
T13358	;@test against a string key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="mystringkey2"
	set xider("field")="incrtest"
	set xider("increment")=2.5
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-7,"data-type not a hash")
	;
	quit
	;
	;
T13359	;@test against a string value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="T13081"
	set xider("field")="T13081-1"
	set xider("increment")=2.5
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-8,"value not integer")
	;
	quit
	;
	;
T13360	;@test increment by 1.5 on non ex field in not ex key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hincrbykey2"
	set xider("field")="hincrbyfield2"
	set xider("increment")=1.5
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),1.5,"value is 1.5")
	do eq^%ut(^%ydbxiderK("hincrbykey2","hincrbyfield2"),1.5,"value is 1.5")
	;
	quit
	;
	;
T13361	;@test increment by 5.5 on existing node
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hincrbykey2"
	set xider("field")="hincrbyfield2"
	set xider("increment")=5.5
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),7,"value is 7")
	do eq^%ut(^%ydbxiderK("hincrbykey2","hincrbyfield2"),7,"value is 7")

	;
	quit
	;
	;
T13362	;@test decrement by 1.5 on existing node
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="hincrbykey2"
	set xider("field")="hincrbyfield2"
	set xider("increment")=-1.5
	;
	;execute the call
	set ret=$$HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),5.5,"value is 5.5")
	do eq^%ut(^%ydbxiderK("hincrbykey2","hincrbyfield2"),5.5,"value is 5.5")

	;
	quit
	;
	;
T13363	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do HINCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
