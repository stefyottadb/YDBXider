;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#	This source code contains the intellectual property	         #
;#	of its copyright holder(s), and is made available            #
;#	under a license.  If you do not know the terms of            #
;#	the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
	;
%ydbxiderTestApiString
	; Requires M-Unit
	;
test if $text(^%ut)="" quit
	do en^%ut($text(+0),3)
	;
	write !
	;
	quit
	;
STARTUP
	do STARTUP^%ydbxiderTestUtils
	;
	quit
	;
	;
SHUTDOWN
	do SHUTDOWN^%ydbxiderTestUtils
	;
	quit
	;
	;
SET0	;@test --------------------
	quit
	;
	;
SET1	;@test --------------------API <string> SET
	quit
	;
	;
SET2	;@test --------------------
	quit
	;
	;
T11500	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11501	;@test with only key set
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="testKey"
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,-2,"no value was supplied")
	;
	quit
	;
	;
T11502	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,-6,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11503	;@test with key and value set, verify
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	set xider("key")="myKey"
	set xider("value")="myValue"
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,0,"Success string")
	;
	quit
	;
	;
T11504	;@test with params: XX, no record, verify
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	set xider("key")="myKey2"
	set xider("value")="myValue"
	set xider("params","XX")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,-4,"Null length string")
	;
	quit
	;
	;
T11505	;@test with params: XX, with record, verify response
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKey"
	set xider("value")="myValue"
	set xider("params","XX")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,0,"Success string")
	;
	quit
	;
	;
T11506	;@test with params: NX, no record, verify
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	set xider("key")="myKey"
	set xider("value")="myValue"
	set xider("params","NX")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"Null length string")
	;
	quit
	;
	;
T11507	;@test with params: NX, with record, verify response
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKey2"
	set xider("value")="myValue"
	set xider("params","NX")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,0,"Success string")
	;
	quit
	;
	;
T11508	;@test with params: GET, no record, verify
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKey3"
	set xider("value")="myValue"
	set xider("params","GET")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,-5,"Null length string")
	;
	quit
	;
	;
T11509	;@test with params: GET, with record, verify
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKey2"
	set xider("value")="myValue"
	set xider("params","GET")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	; verify response
	do eq^%ut(ret,1,"ok with value")
	do eq^%ut(xider("ret"),"myValue","return existing string")
	;
	quit
	;
	;
T11510	;@test with params: EX 3, verify aft 1 sec, 4 secs
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKeyTtl"
	set xider("value")="myValueWithTtl"
	set xider("params","EX")="3"
	;
	;execute the call
	set ret=$$SET^xider
	;
	;
	hang 1
	;
	set before=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(before,1,"Data is present")
	;
	hang 4
	;
	set after=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(after,0,"Data is gone")
	;
	quit
	;
	;
T11511	;@test with params: PX 3000, verify aft 1 sec, 4 secs
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKeyTtl"
	set xider("value")="myValueWithTtl"
	set xider("params","PX")="3000"
	;
	;execute the call
	set ret=$$SET^xider
	;
	;
	hang 1
	;
	set before=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(before,1,"Data is present")
	;
	hang 4
	;
	set after=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(after,0,"Data is gone")
	;
	quit
	;
	;
T11512	;@test with params: EXAT $ZUT+3, verify aft 1 sec, 4 secs
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKeyTtl"
	set xider("value")="myValueWithTtl"
	set xider("params","EXAT")=$zut+3E6/1E6
	;
	;execute the call
	set ret=$$SET^xider
	;
	;
	hang 1
	;
	set before=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(before,1,"Data is present")
	;
	hang 4
	;
	set after=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(after,0,"Data is gone")
	;
	quit
	;
	;
T11513	;@test with params: PXAT $ZUT+3000, verify aft 1 sec, 4 secs
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; reuse previously created record
	set xider("key")="myKeyTtl"
	set xider("value")="myValueWithTtl"
	set xider("params","PXAT")=$zut+3E6/1E3
	;
	;execute the call
	set ret=$$SET^xider
	;
	;
	hang 1
	;
	set before=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(before,1,"Data is present")
	;
	hang 4
	;
	set after=$data(^%ydbxiderK("myKeyTtl"))
	do eq^%ut(after,0,"Data is gone")
	;
	quit
	;
	;
T11514	;@test call with params: KEEPTTL, verify
	new ret
	;
	; init the API
	kill xider
	do init^xider
	;
	; create record with long ttl
	set xider("key")="myKeyTtl"
	set xider("value")="myValueWithTtl"
	set xider("params","EX")=20
	;
	;execute the call
	set ret=$$SET^xider
	;
	kill xider
	set xider("key")="myKeyTtl"
	set xider("value")="myValueWithTtl"
	set xider("params","KEEPTTL")=""
	;
	;execute the call
	set ret=$$SET^xider
	;
	hang 2
	;
	; verify
	set exists=$data(^%ydbxiderKT("myKeyTtl"))
	do eq^%ut(exists,1,"Data is present")
	;
	;
	quit
	;
	;
T11517	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do SET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
GET0	;@test --------------------
	quit
	;
	;
GET1	;@test --------------------API <string> GET
	;
	quit
	;
	;
GET2	;@test --------------------
	quit
	;
	;
T11525	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$GET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11526	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$GET^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11527	;@test with valid key, verify response
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create a key / value pair
	set xider("key")="get-test-key"
	set xider("value")="get-test-value"
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="get-test-key"
	;
	;execute the call
	set ret=$$GET^xider
	;
	; verify response
	do eq^%ut(ret,1,"return value")
	do eq^%ut(xider("ret"),"get-test-value","return value")
	;
	quit
	;
	;
T11528	;@test with invalid key, verify response
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="get-bad-key"
	;
	;execute the call
	set ret=$$GET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no key was found")
	;
	quit
	;
	;
T11529	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do GET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
DEL0	;@test --------------------
	;
	quit
DEL1	;@test --------------------API <string> DEL
	;
	quit
DEL2	;@test --------------------
	;
	quit
	;
	;
T11550	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no keys were supplied")
	;
	quit
	;
	;
T11551	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)=lKey
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11552	;@test with 3 keys, 2 too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)=lKey
	set xider("keys",3)=lKey
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11553	;@test with one valid but missing key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("keys",1)="thisIsAmissingKey"
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(ret,0,"no records deleted")
	;
	quit
	;
	;
T11554	;@test with valid key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create an entry
	set xider("key")="testKey"
	set xider("value")="this is some random data"
	set ret=$$SET^xider
	;
	; set the key only
	kill xider
	set xider("keys",1)="testKey"
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(1,1,"1 record deleted")
	;
	quit
	;
	;
T11555	;@test with 3 keys, 2 valid, one not
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create two entries
	set xider("key")="testKey1"
	set xider("value")="this is some random data1"
	set ret=$$SET^xider
	set xider("key")="testKey3"
	set xider("value")="this is some random data3"
	set ret=$$SET^xider
	;
	; set the key only
	kill xider
	set xider("keys",1)="testKey1"
	set xider("keys",2)="testKey"
	set xider("keys",3)="testKey3"
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 records deleted")
	;
	quit
	;
	;

T11558	;@test with 3 keys, 2 valid, one not, check cnt
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create two entries
	set xider("key")="testKey1"
	set xider("value")="this is some random data1"
	set ret=$$SET^xider
	set xider("key")="testKey3"
	set xider("value")="this is some random data3"
	set ret=$$SET^xider
	;
	; set the key only
	kill xider
	set xider("keys",1)="testKey1"
	set xider("keys",2)="testKey"
	set xider("keys",3)="testKey3"
	;
	;execute the call
	set ret=$$DEL^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 records deleted")
	;
	quit
	;
	;
T11559	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do DEL^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
EXISTS0	;@test --------------------
	quit
	;
	;
EXISTS1	;@test ----------------API <string> EXISTS
	;
	quit
	;
	;
EXISTS2	;@test --------------------
	quit
	;
	;
T11600	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$EXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11601	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)=lKey
	;
	;execute the call
	set ret=$$EXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11602	;@test with 3 keys, 2 too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)=lKey
	set xider("keys",3)=lKey
	;
	;execute the call
	set ret=$$EXISTS^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11603	;@test with valid key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create an entry
	set xider("key")="testKey"
	set xider("value")="this is some random data"
	set ret=$$SET^xider
	;
	; set the key only
	kill xider
	set xider("keys",1)="testKey"
	;
	;execute the call
	set ret=$$EXISTS^xider
	;
	; verify response
	do eq^%ut(ret,1,"1 record found")
	;
	quit
	;
	;
T11604	;@test with 3 keys, 2 valid, one not
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create two entries
	set xider("key")="testKey1"
	set xider("value")="this is some random data1"
	set ret=$$SET^xider
	set xider("key")="testKey3"
	set xider("value")="this is some random data3"
	set ret=$$SET^xider
	;
	; set the key only
	kill xider
	set xider("keys",1)="testKey1"
	set xider("keys",2)="testKeynoexists"
	set xider("keys",3)="testKey3"
	;
	;execute the call
	set ret=$$EXISTS^xider
	;
	; verify response
	do eq^%ut(ret,2,"2 records found")
	;
	quit
	;
	;
T11605	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do EXISTS^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
APPEND0	;@test --------------------
	quit
	;
	;
APPEND1	;@test ----------------API <string> APPEND
	;
	quit
	;
	;
APPEND2	;@test --------------------
	quit
	;
	;
T11625	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11626	;@test with key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11627	;@test with key, no value
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="mykey"
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	; verify response
	do eq^%ut(ret,-2,"no value was supplied")
	;
	quit
	;
	;
T11628	;@test with non-existing key, value
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key and the value
	set xider("key")="mykeynotexists"
	set xider("value")="this is 16 chars"
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	; verify response
	do eq^%ut(ret,16,"16 chars")
	;
	quit
	;
	;
T11629	;@test with existing key, value
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="myKey"
	set xider("value")="myValue"
	;
	;execute the call
	set ret=$$SET^xider
	;
	; set the key and the value
	set xider("key")="myKey"
	set xider("value")="this is 15 chars"
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	; verify response
	do eq^%ut(ret,23,"23 chars")
	;
	quit
	;
	;
T11630	;@test with existing key, not string data type
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="myKey"
	set xider("value")="myValue"
	;
	;execute the call
	set ret=$$SET^xider
	set ^%ydbxiderKDT("myKey")="array"
	;
	; set the key and the value
	set xider("key")="myKey"
	set xider("value")="this is 15 chars"
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key is not a string")
	;
	quit
	;
	;
T11631	;@test verify counter gets increased
	new ret
	;
	; init the API
	do init^xider
	kill xider
	kill ^%ydbxiderK
	;
	; set the key and the value
	set xider("key")="mykeynotexistsappend"
	set xider("value")="this is 16 chars"
	;
	;execute the call
	set ret=$$APPEND^xider
	;
	quit
	;
	;
T11632	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do APPEND^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
STRLEN0	;@test --------------------
	quit
	;
	;
STRLEN1	;@test ----------------API <string> STRLEN
	;
	quit
	;
	;
STRLEN2	;@test --------------------
	quit
	;
	;
T11650	;@test with no parameters
	new ret
	;
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$STRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11651	;@test with key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$STRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11652	;@test with invalid key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="idontexists"
	;
	;execute the call
	set ret=$$STRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-3,"returns 0 length")
	;
	quit
	;
	;
T11653	;@test with valid string
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create a key
	set xider("key")="strlen"
	set xider("value")="1234567"
	set ret=$$SET^xider
	;
	; set the key
	set xider("key")="strlen"
	;
	;execute the call
	set ret=$$STRLEN^xider
	;
	; verify response
	do eq^%ut(ret,7,"returns 7 length")
	;
	quit
	;
	;
T11654	;@test with key not string
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; create a key
	set xider("key")="strlen"
	set xider("value")="1234567"
	set ret=$$SET^xider
	set ^%ydbxiderKDT("strlen")="array"
	;
	; set the key
	set xider("key")="strlen"
	;
	;execute the call
	set ret=$$STRLEN^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key is not a string")
	;
	quit
	;
	;
T11655	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do STRLEN^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
GETRANGE0 ;@test --------------------
	quit
	;
	;
GETRANGE1	;@test ----------------API <string> GETRANGE
	;
	quit
	;
	;
GETRANGE2	;@test --------------------
	quit
	;
	;
T11675	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11676	;@test with key too long
	new ret
	;
	new lKey,ix
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11677	;@test with start as string, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")="thisisastring"
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-3,"bad start was supplied")
	;
	quit
	;
	;
T11678	;@test with start as 0, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=0
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-3,"start 0 was supplied")
	;
	quit
	;
	;
T11679	;@test with start as 5, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, no end")
	;
	quit
	;
	;
T11680	;@test with start as -5, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=-5
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, no end")
	;
	quit
	;
	;
T11681	;@test with end as string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	set xider("end")="thisisastring"
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, end as string")
	;
	quit
	;
	;
T11682	;@test with end as 0
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	set xider("end")=0
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, end as 0")
	;
	quit
	;
	;
T11683	;@test with end as 5
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	set xider("end")=5
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-5,"valid start, end as -5")
	;
	quit
	;
	;
T11684	;@test with end as -5
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	set xider("end")=-5
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-5,"valid start, end as -5")
	;
	quit
	;
	;
T11685	;@test with s:2, e:4
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="getrangeKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=2
	set xider("end")=4
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return 234")
	do eq^%ut(xider("ret"),"234","should return 234")
	;
	quit
	;
	;
T11686	;@test with s:-2, e:4
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="getrangeKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=-2
	set xider("end")=4
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return empty string")
	do eq^%ut(xider("ret"),"","should return empty string")
	;
	quit
	;
	;
T11687	;@test with s:1, e:-1
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="getrangeKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=1
	set xider("end")=-1
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return the whole string")
	do eq^%ut(xider("ret"),"1234567890","should return the whole string")
	;
	quit
	;
	;
T11688	;@test with s:-8, e:5
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="getrangeKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=-8
	set xider("end")=5
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return empty string")
	do eq^%ut(xider("ret"),"345","should return empty string")
	;
	quit
	;
	;
T11689	;@test with invalid key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="idontexists"
	set xider("start")=5
	set xider("end")=5
	;
	;execute the call
	set ret=$$GETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-5,"returns 0 length")
	;
	quit
	;
	;
T11690	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do GETRANGE^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
SUBSTR0 ;@test --------------------
	quit
	;
	;
SUBSTR1	;@test ----------------API <string> SUBSTR
	;
	quit
	;
	;
SUBSTR2	;@test --------------------
	quit
	;
	;
T11700	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11701	;@test with key too long
	new ret
	;
	new lKey,ix
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11702	;@test with start as string, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")="thisisastring"
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-3,"bad start was supplied")
	;
	quit
	;
	;
T11703	;@test with start as 0, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=0
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-3,"start 0 was supplied")
	;
	quit
	;
	;
T11704	;@test with start as 5, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, no end")
	;
	quit
	;
	;
T11705	;@test with start as -5, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=-5
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, no end")
	;
	quit
	;
	;
T11706	;@test with end as string
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	set xider("end")="thisisastring"
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, end as string")
	;
	quit
	;
	;
T11707	;@test with end as 0
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="getrangeKey"
	set xider("start")=5
	set xider("end")=0
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-4,"valid start, end as 0")
	;
	quit
	;
	;
T11708	;@test with end as 5
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="substrKey"
	set xider("start")=5
	set xider("end")=5
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-5,"valid start, end as -5")
	;
	quit
	;
	;
T11709	;@test with end as -5
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="substrKey"
	set xider("start")=5
	set xider("end")=-5
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-5,"valid start, end as -5")
	;
	quit
	;
	;
T11710	;@test with s:2, e:4
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="substrKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="substrKey"
	set xider("start")=2
	set xider("end")=4
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return 234")
	do eq^%ut(xider("ret"),"234","should return 234")
	;
	quit
	;
	;
T11711	;@test with s:-2, e:4
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="substrKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="substrKey"
	set xider("start")=-2
	set xider("end")=4
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return empty string")
	do eq^%ut(xider("ret"),"","should return empty string")
	;
	quit
	;
	;
T11712	;@test with s:1, e:-1
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="substrKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="substrKey"
	set xider("start")=1
	set xider("end")=-1
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return the whole string")
	do eq^%ut(xider("ret"),"1234567890","should return the whole string")
	;
	quit
	;
	;
T11713	;@test with s:-8, e:5
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="substrKey"
	set xider("value")="1234567890"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key
	set xider("key")="substrKey"
	set xider("start")=-8
	set xider("end")=5
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,1,"should return empty string")
	do eq^%ut(xider("ret"),"345","should return empty string")
	;
	quit
	;
	;
T11714	;@test with invalid key
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="idontexists"
	set xider("start")=5
	set xider("end")=5
	;
	;execute the call
	set ret=$$SUBSTR^xider
	;
	; verify response
	do eq^%ut(ret,-5,"returns 0 length")
	;
	quit
	;
	;
T11715	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do SUBSTR^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
SETRANGE0	;@test --------------------
	quit
	;
	;
SETRANGE1	;@test ----------------API <string> SETRANGE
	quit
	;
	;
SETRANGE2	;@test --------------------
	quit
	;
	;
T11725	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11726	;@test with key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11727	;@test with offset as string, no value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="setrangeKey"
	set xider("start")="thisisastring"
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-3,"bad offset was supplied")
	;
	quit
	;
	;
T11728	;@test with offset as 0, no max
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="setrangeKey"
	set xider("offset")=0
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-3,"start 0 was supplied")
	;
	quit
	;
	;
T11729	;@test with start as 5, no value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="setrangeKey"
	set xider("offset")=5
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,-4,"no value")
	;
	quit
	;
	;
T11730	;@test with start as 5, 3 char value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create existing entry
	set xider("key")="setrangeKey"
	set xider("value")=1234567890
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="setrangeKey"
	set xider("offset")=5
	set xider("value")="tre"
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,10,"new string length")
	do eq^%ut(^%ydbxiderK("setrangeKey"),"12345tre90","no value")
	;
	quit
	;
	;
T11731	;@test with start as 25, 3 char value
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create existing entry
	set xider("key")="setrangeKey"
	set xider("value")=1234567890
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="setrangeKey"
	set xider("offset")=25
	set xider("value")="tre"
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,28,"new length")
	do eq^%ut(^%ydbxiderK("setrangeKey"),"1234567890"_$char(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)_"tre","no value")
	;
	quit
	;
	;
T11732	;@test with start as 25, 3 char value, non ex key
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; set the key
	set xider("key")="setrangeKey2"
	set xider("offset")=25
	set xider("value")="tre"
	;
	;execute the call
	set ret=$$SETRANGE^xider
	;
	; verify response
	do eq^%ut(ret,28,"new length")
	do eq^%ut(^%ydbxiderK("setrangeKey2"),$char(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)_"tre","no value")
	;
	quit
	;
	;
T11733	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do SETRANGE^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
RENAME0	;@test --------------------
	quit
	;
	;
RENAME1	;@test ----------------API <string> RENAME
	quit
	;
	;
RENAME2	;@test --------------------
	quit
	;
	;
T11750	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$RENAME^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11751	;@test with key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$RENAME^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11752	;@test with no newkey
	new ret
	;
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	;
	;execute the call
	set ret=$$RENAME^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no key was supplied")
	;
	quit
	;
	;
T11753	;@test with newkey too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")="testrename"
	set xider("newkey")=lKey
	;
	;execute the call
	set ret=$$RENAME^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11754	;@test with no key record
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	set xider("newkey")="newtestrename"
	;
	;execute the call
	set ret=$$RENAME^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key doesn't exists")
	;
	quit
	;
	;
T11755	;@test with valid data
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create a key
	set xider("key")="testrename"
	set xider("value")="rename-value"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	set xider("newkey")="newtestrename"
	;
	;execute the call
	set ret=$$RENAME^xider
	;
	; verify response
	do eq^%ut(ret,0,"renamed")
	;
	; verify data
	do eq^%ut($get(^%ydbxiderK("testrename")),"","validate old key")
	do eq^%ut(^%ydbxiderK("newtestrename"),"rename-value","validate new key")
	;
	quit
	;
	;
T11756	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do RENAME^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
RENAMENX0	;@test --------------------
	quit
	;
	;
RENAMENX1	;@test ----------------API <string> RENAMENX
	quit
	;
	;
RENAMENX2	;@test --------------------
	quit
	;
	;
T11775	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11776	;@test with key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11777	;@test with no newkey
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no key was supplied")
	;
	quit
	;
	;
T11778	;@test with newkey too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key
	set xider("key")="testrename"
	set xider("newkey")=lKey
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,-4,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11779	;@test with no key record
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	set xider("newkey")="newtestrename"
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,-5,"key doesn't exists")
	;
	quit
	;
	;
T11780	;@test with valid data
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create a key
	set xider("key")="testrename"
	set xider("value")="rename-value"
	;
	set ret=$$SET^xider
	;
	; create a key
	set xider("key")="newtestrename"
	set xider("value")="olvalue"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	set xider("newkey")="newtestrename"
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,-6,"new key already exists")
	;
	quit
	;
	;
T11781	;@test with non existing newkey
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create a key
	set xider("key")="testrename"
	set xider("value")="rename-value"
	;
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="testrename"
	set xider("newkey")="newtestrenamenx"
	;
	;execute the call
	set ret=$$RENAMENX^xider
	;
	; verify response
	do eq^%ut(ret,0,"renamed")
	;
	quit
	;
	;
T11782	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do RENAMENX^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
GETDEL0	;@test --------------------
	quit
	;
	;
GETDEL1	;@test --------------------API <string> GETDEL
	;
	quit
	;
	;
GETDEL2	;@test --------------------
	quit
	;
	;
T11800	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$GETDEL^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11801	;@test with key > 975 chars
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$GETDEL^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11802	;@test with valid key, verify response
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create a key / value pair
	set xider("key")="get-test-key"
	set xider("value")="get-test-value"
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="get-test-key"
	;
	;execute the call
	set ret=$$GETDEL^xider
	;
	; verify response
	do eq^%ut(ret,1,"return value")
	do eq^%ut(xider("ret"),"get-test-value","return value")
	;
	quit
	;
	;
T11803	;@test with invalid key, verify response
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="get-bad-key"
	;
	;execute the call
	set ret=$$GETDEL^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no key was found")
	;
	quit
	;
	;
T11804	;@test with valid key, verify response and deletion
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	; create a key / value pair
	set xider("key")="get-test-key"
	set xider("value")="get-test-value"
	set ret=$$SET^xider
	;
	; kill shared array
	kill xider
	;
	set xider("key")="get-test-key"
	;
	;execute the call
	set ret=$$GETDEL^xider
	;
	; verify response
	do eq^%ut(xider("ret"),"get-test-value","return value")
	;
	; and deletion
	do eq^%ut($data(^%ydbxiderK("get-test-key")),0,"old node")
	;
	quit
	;
	;
T11805	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do GETDEL^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
MSET0	;@test --------------------
	;
	quit
MSET1	;@test --------------------API <string> MSET
	;
	quit
MSET2	;@test --------------------
	;
	quit
	;
	;
T11825	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$MSET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11826	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)=lKey
	;
	;execute the call
	set ret=$$MSET^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11837	;@test with 3 keys, 2 too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)=lKey
	set xider("keys",3)=lKey
	;
	;execute the call
	set ret=$$MSET^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11838	;@test with 3 keys and 2 values
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)="another"
	set xider("keys",3)="andanother"
	set xider("values",1)="thisIsAgoodKey"
	set xider("values",3)="andanother"
	;
	;execute the call
	set ret=$$MSET^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key 2 is missing the value")
	;
	quit
	;
	;
T11839	;@test with 3 keys and 3 values
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)="another"
	set xider("keys",3)="andanother"
	set xider("values",1)="thisIsAgoodKey"
	set xider("values",2)="andanother"
	set xider("values",3)="andanother"
	;
	;execute the call
	set ret=$$MSET^xider
	;
	; verify response
	do eq^%ut(ret,0,"all good")
	;
	quit
	;
	;
T11840	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do MSET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
MSETNX0	;@test --------------------
	;
	quit
MSETNX1	;@test --------------------API <string> MSETNX
	;
	quit
MSETNX2	;@test --------------------
	;
	quit
	;
	;
T11850	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$MSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11851	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)=lKey
	;
	;execute the call
	set ret=$$MSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11852	;@test with 3 keys, 2 too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)=lKey
	set xider("keys",3)=lKey
	;
	;execute the call
	set ret=$$MSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11853	;@test with 3 keys and 2 values
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)="another"
	set xider("keys",3)="andanother"
	set xider("values",1)="thisIsAgoodKey"
	set xider("values",3)="andanother"
	;
	;execute the call
	set ret=$$MSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-3,"key 2 is missing the value")
	;
	quit
	;
	;
T11854	;@test with 3 keys and 3 values
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey2"
	set xider("keys",2)="another2"
	set xider("keys",3)="andanother2"
	set xider("values",1)="thisIsAgoodKey"
	set xider("values",2)="andanother"
	set xider("values",3)="andanother"
	;
	;execute the call
	set ret=$$MSETNX^xider
	;
	; verify response
	do eq^%ut(ret,0,"all good")
	;
	quit
	;
	;
T11855	;@test with 3 keys and 1 exists
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="thisIsAgoodKey"
	set xider("value")="somedata"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)="another"
	set xider("keys",3)="andanother"
	set xider("values",1)="thisIsAgoodKey"
	set xider("values",2)="andanother"
	set xider("values",3)="andanother"
	;
	;execute the call
	set ret=$$MSETNX^xider
	;
	; verify response
	do eq^%ut(ret,-4,"one key already exists")
	;
	quit
	;
	;
T11856	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do MSETNX^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
MGET0	;@test --------------------
	;
	quit
MGET1	;@test --------------------API <string> MGET
	;
	quit
MGET2	;@test --------------------
	;
	quit
	;
	;
T11875	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$MGET^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11876	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)=lKey
	;
	;execute the call
	set ret=$$MGET^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11877	;@test with 3 keys, 2 too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("keys",1)="thisIsAgoodKey"
	set xider("keys",2)=lKey
	set xider("keys",3)=lKey
	;
	;execute the call
	set ret=$$MGET^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11878	;@test with 3 keys, all empty
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("keys",1)="key1"
	set xider("keys",2)="key2"
	set xider("keys",3)="key3"
	;
	;execute the call
	set ret=$$MGET^xider
	;
	; verify response
	do eq^%ut(ret,0,"success, but no data")
	do eq^%ut(xider("ret",1),"","no data")
	do eq^%ut(xider("ret",2),"","no data")
	do eq^%ut(xider("ret",3),"","no data")
	;
	;
	quit
	;
	;
T11879	;@test with 3 keys, 2 with data, 1 empty
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="key1"
	set xider("value")="key1data"
	set ret=$$SET^xider
	;
	set xider("key")="key2"
	set xider("value")="key2data"
	set ret=$$SET^xider
	;
	kill xider
	;
	; set the key only
	set xider("keys",1)="key1"
	set xider("keys",2)="key2"
	set xider("keys",3)="key3"
	;
	;execute the call
	set ret=$$MGET^xider
	;
	; verify response
	do eq^%ut(ret,0,"success, but some data")
	do eq^%ut(xider("ret",1),"key1data","data")
	do eq^%ut(xider("ret",2),"key2data","data")
	do eq^%ut(xider("ret",3),"","no data")
	;
	;
	quit
	;
	;
T11880	;@test with 3 keys, 1 with data, 1 no string dt, 1 empty
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	set xider("key")="key1"
	set xider("value")="key1data"
	set ret=$$SET^xider
	;
	kill xider
	;
	set xider("key")="key2"
	set xider("value")="key2data"
	set ret=$$SET^xider
	set ^%ydbxiderKDT("key2")="array"
	;
	kill xider
	;
	; set the key only
	set xider("keys",1)="key1"
	set xider("keys",2)="key2"
	set xider("keys",3)="key3"
	;
	;execute the call
	set ret=$$MGET^xider
	;
	; verify response
	do eq^%ut(ret,0,"success, but some data")
	do eq^%ut(xider("ret",1),"key1data","data")
	do eq^%ut(xider("ret",2),"","no data")
	do eq^%ut(xider("ret",3),"","no data")
	;
	;
	quit
	;
	;
T11881	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do MGET^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
INCR0	;@test --------------------
	;
	quit
INCR1	;@test --------------------API <string> INCR
	;
	quit
INCR2	;@test --------------------
	;
	quit
	;
	;
T11900	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11901	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11902	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest99"
	set xider("value")="with a string"
	set ret=$$SET^xider
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(ret,-3,"value not an integer")
	;
	quit
	;
	;
T11903	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98"
	set xider("value")="000"
	set ret=$$SET^xider
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(ret,-3,"value not an integer")
	;
	quit
	;
	;
T11904	;@test with non string data type
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98"
	set xider("value")=23
	set ret=$$SET^xider
	set ^%ydbxiderKDT(xider("key"))="array"
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(ret,-4,"value not a string data-type")
	;
	quit
	;
	;
T11905	;@test with not existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest"
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(xider("ret"),1,"from 0 to 1")
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(xider("ret"),2,"from 1 to 2")
	;
	quit
	;
	;
T11906	;@test with existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest2"
	set xider("value")=5
	set ret=$$SET^xider
	;
	;execute the call
	set ret=$$INCR^xider
	;
	; verify response
	do eq^%ut(xider("ret"),6,"from 5 to 6")
	;
	quit
	;
	;
T11907	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do INCR^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
INCRBY0	;@test --------------------
	;
	quit
INCRBY1	;@test --------------------API <string> INCRBY
	;
	quit
INCRBY2	;@test --------------------
	;
	quit
	;
	;
T11925	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11926	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11927	;@test with key and no increment
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="incrbykey"
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no increment")
	;
	quit
	;
	;
T11928	;@test with key and increment not as integer
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="incrbykey"
	set xider("increment")="000"
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-4,"no increment")
	;
	quit
	;
	;
T11929	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest99a"
	set xider("value")="with a string"
	set ret=$$SET^xider
	;
	;execute the call
	set xider("increment")=2
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-5,"value not an integer")
	;
	quit
	;
	;
T11930	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98a"
	set xider("value")="000"
	set ret=$$SET^xider
	;
	;execute the call
	set xider("increment")=2
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-5,"value not an integer")
	;
	quit
	;
	;
T11931	;@test with non string data type
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98a"
	set xider("increment")=23
	set ret=$$SET^xider
	set ^%ydbxiderKDT(xider("key"))="array"
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(ret,-6,"value not a string data-type")
	;
	quit
	;
	;
T11932	;@test with not existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtestby"
	set xider("increment")=5
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),5,"from 0 to 5")
	;
	;execute the call
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),10,"from 5 to 10")
	;
	;execute the call
	set xider("increment")=-7
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),3,"from 10 to 3")
	;
	quit
	;
	;
T11933	;@test with existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest2"
	set xider("value")=5
	set ret=$$SET^xider
	;
	;execute the call
	set xider("increment")=3
	set ret=$$INCRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),8,"from 5 to 8")
	;
	quit
	;
	;
T11934	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do INCRBY^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
INCRBYFLOAT0	;@test --------------------
	;
	quit
INCRBYFLOAT1	;@test -----------------API <string> INCRBYFLOAT
	;
	quit
INCRBYFLOAT2	;@test --------------------
	;
	quit
	;
	;
T11950	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11951	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11952	;@test with key and no increment
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="incrbykey"
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no increment")
	;
	quit
	;
	;
T11953	;@test with key and increment non-fp
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="incrbykey"
	set xider("increment")="000"
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-4,"no increment")
	;
	quit
	;
	;
T11954	;@test with non-fp value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest99a"
	set xider("value")="with a string"
	set ret=$$SET^xider
	;
	;execute the call
	set xider("increment")=2
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-5,"value not a fp")
	;
	quit
	;
	;
T11955	;@test with non-fp value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98a"
	set xider("value")="000"
	set ret=$$SET^xider
	;
	;execute the call
	set xider("increment")=2
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-5,"value not an integer")
	;
	quit
	;
	;
T11956	;@test with non string data type
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98a"
	set xider("increment")=23
	set ret=$$SET^xider
	set ^%ydbxiderKDT(xider("key"))="array"
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(ret,-6,"value not a string data-type")
	;
	quit
	;
	;
T11957	;@test with not existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtestby3"
	set xider("increment")=0.5
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),0.5,"from 0 to .5")
	;
	;execute the call
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),1,"from .5 to 1")
	;
	;execute the call
	set xider("increment")=-2.5
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),-1.5,"from 10 to 3")
	;
	quit
	;
	;
T11958	;@test with existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest3"
	set xider("value")=5.2
	set ret=$$SET^xider
	;
	;execute the call
	set xider("increment")=-2.5
	set ret=$$INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut(xider("ret"),2.7,"from 5 to 6")
	;
	quit
	;
	;
T11959	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do INCRBYFLOAT^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
DECR0	;@test --------------------
	;
	quit
DECR1	;@test --------------------API <string> DECR
	;
	quit
DECR2	;@test --------------------
	;
	quit
	;
	;
T11975	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11976	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11977	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest99"
	set xider("value")="with a string"
	set ret=$$SET^xider
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(ret,-3,"value not an integer")
	;
	quit
	;
	;
T11978	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98"
	set xider("value")="000"
	set ret=$$SET^xider
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(ret,-3,"value not an integer")
	;
	quit
	;
	;
T11979	;@test with non string data type
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest98"
	set xider("value")=23
	set ret=$$SET^xider
	set ^%ydbxiderKDT(xider("key"))="array"
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(ret,-4,"value not a string data-type")
	;
	quit
	;
	;
T11980	;@test with not existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="decrtest"
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(xider("ret"),-1,"from 0 to -1")
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(xider("ret"),-2,"from -1 to -2")
	;
	quit
	;
	;
T11981	;@test with existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="incrtest2"
	set xider("value")=5
	set ret=$$SET^xider
	;
	;execute the call
	set ret=$$DECR^xider
	;
	; verify response
	do eq^%ut(xider("ret"),4,"from 5 to 4")
	;
	quit
	;
	;
T11982	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do DECR^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
DECRBY0	;@test --------------------
	;
	quit
DECRBY1	;@test --------------------API <string> DECRBY
	;
	quit
DECRBY2	;@test --------------------
	;
	quit
	;
	;
T11985	;@test with no parameters
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-1,"no key was supplied")
	;
	quit
	;
	;
T11986	;@test with 1 key too long
	;
	new lKey,ix,ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; build a long key
	set lKey=""
	for ix=1:1:xiderSTRINGMAXKEYLENGTH+1 set lKey=lKey_"x"
	;
	; set the key only
	set xider("key")=lKey
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-2,"key length > xiderSTRINGMAXKEYLENGTH")
	;
	quit
	;
	;
T11987	;@test with key and no increment
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="decrbykey"
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-3,"no decrement")
	;
	quit
	;
	;
T11988	;@test with key and increment not as integer
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key only
	set xider("key")="decrbykey"
	set xider("decrement")="000"
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-4,"no decrement")
	;
	quit
	;
	;
T11989	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="decrtest99a"
	set xider("value")="with a string"
	set ret=$$SET^xider
	;
	;execute the call
	set xider("decrement")=2
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-5,"value not an integer")
	;
	quit
	;
	;
T11990	;@test with non-integer value
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="decrtest98a"
	set xider("value")="000"
	set ret=$$SET^xider
	;
	;execute the call
	set xider("decrement")=2
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-5,"value not an integer")
	;
	quit
	;
	;
T11991	;@test with non string data type
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="decrtest98a"
	set xider("decrement")=23
	set ret=$$SET^xider
	set ^%ydbxiderKDT(xider("key"))="array"
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(ret,-6,"value not a string data-type")
	;
	quit
	;
	;
T11992	;@test with not existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="decrtestby"
	set xider("decrement")=5
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),-5,"from 0 to -5")
	;
	;execute the call
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),-10,"from -5 to -10")
	;
	;execute the call
	set xider("decrement")=-7
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),-3,"from -10 to -3")
	;
	quit
	;
	;
T11993	;@test with existing key
	;
	new ret
	;
	; init the API
	do init^xider
	kill xider
	;
	; set the key
	set xider("key")="decrtest2"
	set xider("value")=5
	set ret=$$SET^xider
	;
	;execute the call
	set xider("decrement")=-2
	set ret=$$DECRBY^xider
	;
	; verify response
	do eq^%ut(xider("ret"),7,"from 5 to 7")
	;
	quit
	;
	;
T11994	;@test as procedure
	new ret
	;
	; init the API
	do init^xider
	;
	; kill shared array
	kill xider
	;
	;execute the call
	do DECRBY^xider
	;
	; verify response
	do eq^%ut($data(xider("status")),1,"status got populated")
	;
	quit
	;
	;
