/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const {expect} = require('chai');
const {execSync, exec} = require('child_process')
const libs = require('../../libs.js')
const ydb = require('nodem').Ydb()

describe("Server: sessions using nodem", async () => {
    it("Test # 11000: Launch the server, verify that the helper entry exists, initialize the nodem, verify", async () => {
        // launch the server
        exec('/YDBXIDER/commands/xider-server')

        // wait
        await libs.delay(500)

        // dump globals
        let globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')

        // verify
        expect(globals.length === 9).to.be.true
        expect(globals[0]).to.have.string('^%ydbxider("S")=0')
        expect(globals[3]).to.have.string('Helper process')
        expect(globals[7]).to.have.string('"H"')

        // initialize the nodem library and initialize the API
        ydb.procedure('init^xider')

        // dump globals
        globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')

        expect(globals.length === 16).to.be.true
        expect(globals[0]).to.have.string('^%ydbxider("S")=1')
        expect(globals[7]).to.have.string('"A"')

    })

    it("Test # 11001: Launch the server, verify that the helper entry exists, initialize the nodem, verify, terminate, verify", async () => {
        // launch the server
        exec('/YDBXIDER/commands/xider-server')

        // wait
        await libs.delay(500)

        // dump globals
        let globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')

        // verify
        expect(globals.length === 9).to.be.true
        expect(globals[0]).to.have.string('^%ydbxider("S")=0')
        expect(globals[3]).to.have.string('Helper process')
        expect(globals[7]).to.have.string('"H"')

        ydb.procedure('init^xider')

        // dump globals
        globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')

        expect(globals.length === 16).to.be.true
        expect(globals[0]).to.have.string('^%ydbxider("S")=1')
        expect(globals[7]).to.have.string('"A"')

        ydb.procedure('terminate^xider')

        // dump globals
        globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')
        expect(globals[0]).to.have.string('^%ydbxider("S")=0')
        expect(globals[3]).to.have.string('Helper process')
        expect(globals[7]).to.have.string('"H"')
    })

    it("Test # 11002: Launch the server, verify that the helper entry exists, initialize the nodem with no parameters, verify", async () => {
        // launch the server
        exec('/YDBXIDER/commands/xider-server')

        // wait
        await libs.delay(500)

        // dump globals
        let globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')

        // verify
        expect(globals.length === 9).to.be.true
        expect(globals[0]).to.have.string('^%ydbxider("S")=0')
        expect(globals[3]).to.have.string('Helper process')
        expect(globals[7]).to.have.string('"H"')

        ydb.procedure('init^xider')

        // dump globals
        globals = execSync('yottadb -run getSessions^%ydbxiderTestServer').toString().split('\n')

        expect(globals.length === 16).to.be.true
        expect(globals[0]).to.have.string('^%ydbxider("S")=1')
        expect(globals[3]).to.have.string('N/A')
        expect(globals[4]).to.have.string('N/A')
        expect(globals[5]).to.have.string('N/A')
        expect(globals[7]).to.have.string('"A"')

    })

})
