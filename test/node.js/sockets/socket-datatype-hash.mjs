/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

import nodem from 'nodem'
import {createClient} from 'redis';
import {expect} from 'chai';
import {exec} from 'child_process'
import libs from "../../libs.js";

// launch the server
const serverProc = exec('/YDBXIDER/commands/xider-server --reset-db')

// wait
await libs.delay(500)


describe("SOCKET <hash>: HSET", async () => {
    it("Test # 101700: HSET to non existing key single field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HSET('HSETkey1', 'field1', 'value1')
        expect(res === 1).to.be.true

        await client.disconnect();
    })

    it("Test # 101701: HSET to non existing key, multiple fields, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HSET('HSETkey1', [
            'field1', 'value1',
            'field2', 'value2',
        ])
        expect(res === 1).to.be.true

        await client.disconnect();
    })

    it("Test # 101702: HSET to existing key, new field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HSET('HSETkey1', 'field3','value3')

        expect(res === 1).to.be.true

        await client.disconnect();
    })

    it("Test # 101703: HSET to existing key and field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HSET('HSETkey1', 'field1','newvalue1')

        expect(res === 0).to.be.true

        await client.disconnect();
    })

    it("Test # 101704: HSET to existing key, some existing field, some non existing fields", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HSET('HSETkey1', [
            'field1', 'value1',
            'field2', 'value2',
            'field5', 'value5',
            'field3', 'value3',
            'field4', 'value4',
        ])
        expect(res === 2).to.be.true

        await client.disconnect();
    })

    it("Test # 101705: HSET to existing string data-type", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('stringkey','stringValue')

        try {
            const res = await client.HSET('stringkey', 'field1','value1')
        } catch (err) {
            expect(err.message).to.have.string("WRONGTYPE Operation against a key holding the wrong kind of value")
        }

        await client.disconnect();
    })
})

describe("SOCKET <hash>: HGET", async () => {
    it("Test # 101750: HGET to non existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HGET('HGETkey1', 'field1')
        expect(res ).to.be.null

        await client.disconnect();
    })

    it("Test # 101751: HGET to existing key, no field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HGET('HSETkey1', 'field99')
        expect(res ).to.be.null

        await client.disconnect();
    })

    it("Test # 101751: HGET to existing key and field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HGET('HSETkey1', 'field1')
        expect(res ).to.have.string('value1')

        await client.disconnect();
    })

    it("Test # 101753: HGET to string data-type, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        try {
            const res = await client.HGET('stringkey', 'field99')
        } catch (err) {
            expect(err.message).to.have.string("WRONGTYPE Operation against a key holding the wrong kind of value")
        }

        await client.disconnect();
    })
})

describe("SOCKET <hash>: HDEL", async () => {
    it("Test # 101800: HDEL to non existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HDEL('nokey', 'field1')
        expect(res).to.be.null

        await client.disconnect();
    })

    it("Test # 101801: HDEL to key, non existing field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HDEL('HSETkey1', 'nonexfield1')
        expect(res===0).to.be.true

        await client.disconnect();
    })

    it("Test # 101802: HDEL to key, existing field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HDEL('HSETkey1', 'field1')
        expect(res===1).to.be.true

        await client.disconnect();
    })

    it("Test # 101802: HDEL to key, existing field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HDEL('HSETkey1', [
            'field3',
            'field2',
            'field99',
            'field101',
            'field102',
        ])
        expect(res===2).to.be.true

        await client.disconnect();
    })

    it("Test # 101804: HDEL to string data-type", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        try {
            const res = await client.HGET('stringkey', 'field99')
        } catch (err) {
            expect(err.message).to.have.string("WRONGTYPE Operation against a key holding the wrong kind of value")
        }

        await client.disconnect();
    })
})

describe("SOCKET <hash>: HINCRBY", async () => {
    it("Test # 101850: HINCRBY by 1 to non existing key and field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('noinc', 'field1',1)
        expect(res===1).to.be.true

        await client.disconnect();
    })

    it("Test # 101851: HINCRBY by 1 to existing key, non existing field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('HSETkey1', 'incfield',1)
        expect(res===1).to.be.true

        await client.disconnect();
    })

    it("Test # 101852: HINCRBY by 1 to existing key / string field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('HSETkey1', 'field1',1)
        expect(res===1).to.be.true

        await client.disconnect();
    })

    it("Test # 101853: HINCRBY by 2 to non existing key and field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('noinc2', 'field1',2)
        expect(res===2).to.be.true

        await client.disconnect();
    })

    it("Test # 101854: HINCRBY by 2 to existing key, non existing field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('HSETkey1', 'incfield2',2)
        expect(res===2).to.be.true

        await client.disconnect();
    })

    it("Test # 101855: HINCRBY by 2 to existing key / string field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('HSETkey1', 'field2',2)
        expect(res===2).to.be.true

        await client.disconnect();
    })

    it("Test # 101856: HINCRBY by -5 to non existing key and field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('noinc3', 'field1',-5)
        expect(res===-5).to.be.true

        await client.disconnect();
    })

    it("Test # 101857: HINCRBY by -5 to existing key, non existing field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('HSETkey1', 'incfield3',-5)
        expect(res===-5).to.be.true

        await client.disconnect();
    })

    it("Test # 101858: HINCRBY by -5 to existing key / string field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.HINCRBY('HSETkey1', 'field2',-5)
        expect(res===-3).to.be.true

        await client.disconnect();
    })

    it("Test # 101859: HINCRBY by 0 to existing key / string field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        try {
            const res = await client.HINCRBY('HSETkey1', 'field2',0)
        } catch (err) {
            expect(err.message).to.have.string("ERR value is not an integer or out of range")
        }

        await client.disconnect();
    })

    it("Test # 101860: HINCRBY by aaa to existing key / string field, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        try {
            const res = await client.HINCRBY('HSETkey1', 'field2','aaa')
        } catch (err) {
            expect(err.message).to.have.string("ERR value is not an integer or out of range")
        }

        await client.disconnect();
    })

    it("Test # 101861: HINCRBY to string data-type, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        try {
            const res = await client.HGET('stringkey', 'field99')
        } catch (err) {
            expect(err.message).to.have.string("WRONGTYPE Operation against a key holding the wrong kind of value")
        }

        await client.disconnect();
    })
})
