/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

import nodem from 'nodem'
import {createClient} from 'redis';
import {expect} from 'chai';
import {exec} from 'child_process'
import libs from "../../libs.js";

let ydb = nodem.Ydb()
ydb.open()

// launch the server
const serverProc = exec('/YDBXIDER/commands/xider-server --reset-db')

// wait
await libs.delay(500)


describe("SOCKET <string>: SET", async () => {
    it("Test # 101500: SET: set a key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey1', 'hello world')
        expect(res).to.have.string('OK')

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkey1")
        expect(val).to.have.string('hello world')
    })

    it("Test # 101501: overwrite an existing key, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey1', 'hello frank')
        expect(res).to.have.string('OK')

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkey1")
        expect(val).to.have.string('hello frank')
    })

    it("Test # 101502: set a new key using the NX parameter, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey2', 'hello NX', {
            NX: true
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkey2")
        expect(val).to.have.string('hello NX')
    })

    it("Test # 101503: set an existing key using the NX parameter, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey1', 'hello NX', {
            NX: true
        })
        expect(res).to.be.null

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkey1")
        expect(val).to.have.string('hello frank')
    })

    it("Test # 101504: set a new key using the XX parameter, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey3', 'hello XX', {
            XX: true
        })
        expect(res).to.be.null

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkey3")
        expect(val).to.have.string('')
    })

    it("Test # 101505: set an existing key using the XX parameter, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey1', 'hello XX', {
            XX: true
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkey1")
        expect(val).to.have.string('hello XX')
    })

    it("Test # 101506: set a new key with an EX value, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey5', 'hello XX', {
            EX: 2
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        let val = ydb.get("^%ydbxiderK", "SETkey5")
        expect(val).to.have.string('hello XX')

        await libs.delay(3)

        val = ydb.get("^%ydbxiderK", "SETkey5")
        expect(val).to.have.string('')
    })

    it("Test # 101507: set a new key with the PX value, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkey5', 'hello XX', {
            PX: 2000
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        let val = ydb.get("^%ydbxiderK", "SETkey1")
        expect(val).to.have.string('hello XX')

        await libs.delay(3)

        val = ydb.get("^%ydbxiderK", "SETkey1")
        expect(val).to.have.string('')
    })

    it("Test # 101508: set a new key with EX=4, overwrite: TTL should disappear", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        let res = await client.SET('SETkeyEx10', 'hello XX', {
            EX: 4
        })
        expect(res).to.have.string("OK")

        await libs.delay(1)

        res = await client.SET('SETkeyEx10', 'hello no ttl')
        expect(res).to.have.string("OK")

        await client.disconnect();

        let val = ydb.get("^%ydbxiderK", "SETkeyEx10")
        expect(val).to.have.string('hello no ttl')

        await libs.delay(4)

        val = ydb.get("^%ydbxiderK", "SETkeyEx10")
        expect(val).to.have.string('hello no ttl')
    })

    it("Test # 101509: set a new key with EX=10, overwrite with KEEPTTL: TTL should remain", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        let res = await client.SET('SETkeyEx10', 'hello XX', {
            EX: 4
        })
        expect(res).to.have.string("OK")

        await libs.delay(1)

        res = await client.SET('SETkeyEx10', 'hello keep ttl', {
            KEEPTTL: true
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        let val = ydb.get("^%ydbxiderK", "SETkeyEx10")
        expect(val).to.have.string('hello keep ttl')

        await libs.delay(4)

        val = ydb.get("^%ydbxiderK", "SETkeyEx10")
        expect(val).to.have.string('')
    })

    it("Test # 101510: set a new key with an EXAT value, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        let res = await client.SET('SETkeyEx10', 'hello EXAT', {
            EXAT: Date.now / 1000 + 4
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        let val = ydb.get("^%ydbxiderK", "SETkeyEx10")
        expect(val).to.have.string('hello EXAT')

        await libs.delay(4)

        val = ydb.get("^%ydbxiderK", "SETkeyEx10")
        expect(val).to.have.string('')
    })

    it("Test # 101511: set a new key with the PXAT value, verify", async () => {
        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        let res = await client.SET('SETkeyPXAT', 'hello PXAT', {
            PXAT: Date.now + 4000
        })
        expect(res).to.have.string("OK")

        await client.disconnect();

        let val = ydb.get("^%ydbxiderK", "SETkeyPXAT")
        expect(val).to.have.string('hello PXAT')

        await libs.delay(4)

        val = ydb.get("^%ydbxiderK", "SETkeyPXAT")
        expect(val).to.have.string('')
    })

    it("Test # 101512: set a new key using the GET parameter, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.SET('SETkeyGET', 'hello get', {
            GET: true
        })
        expect(res).to.be.null

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETkeyGET")
        expect(val).to.have.string('hello get')
    })

    it("Test # 101513: set a value over an existing key using the GET parameter, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('SETGET', 'old string')

        const res = await client.SET('SETGET', 'new string', {
            GET: true
        })
        expect(res).to.have.string('old string')

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "SETGET")
        expect(val).to.have.string('new string')
    })

    it("Test # 101514: set a value over an existing <hash> data-type", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.HSET('myHash', 'field1', 'valuwe1')

        const res = await client.SET('myHash', 'new string')
        expect(res).to.have.string('OK')

        await client.disconnect();

        const val = ydb.get("^%ydbxiderK", "myHash")
        expect(val).to.have.string('new string')
    })
})

describe("SOCKET <string>: GET", async () => {
    it("Test # 101550: get a non-existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.GET('GETkey')
        expect(res).to.be.null

        await client.disconnect();
    })

    it("Test # 101551: get an existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('GETkey', 'GETvalue')

        const res = await client.GET('GETkey')
        expect(res).to.have.string("GETvalue")

        await client.disconnect();
    })

    it("Test # 105552: get an <hash> data-type, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        ydb.set('^%ydbxiderK', 'hashkey', 22)
        ydb.set('^%ydbxiderKDT', 'hashkey', 'hash')

        try {
            const res = await client.GET('hashkey')
        } catch (err) {
            expect(err.message).to.have.string("WRONGTYPE Operation against a key holding the wrong kind of value")
        }

        await client.disconnect();
    })
})

describe("SOCKET <string>: DEL", async () => {
    it("Test # 101600: del a non-existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.DEL('DELkey')
        expect(res === 0).to.be.true

        await client.disconnect();
    })

    it("Test # 101601: del an existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.DEL('hashkey')
        expect(res === 1).to.be.true

        await client.disconnect();
    })

    it("Test # 105602: del one existing and one non-existing key, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('akey', 22)

        const res = await client.DEL('akey', 'DELkey')
        expect(res === 1).to.be.true

        await client.disconnect();
    })

    it("Test # 105603: del 3 existing keys and 4 non-existing keys, verify", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('key1', 22)
        await client.SET('key2', 22)
        await client.SET('key3', 22)

        const res = await client.DEL([
            'key1',
            'nokey1',
            'key2',
            'nokey2',
            'nokey3',
            'key3',
            'nokey4',
        ])
        expect(res === 3).to.be.true

        await client.disconnect();
    })

    it("Test # 105603: DEL a <hash> data-type", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        ydb.set('^%ydbxiderK', 'hashkey', 22)
        ydb.set('^%ydbxiderKDT', 'hashkey', 'hash')

        const res = await client.DEL('hashkey')
        expect(res === 1).to.be.true

        await client.disconnect();
    })
})

describe("SOCKET <string>: INCR", async () => {
    it("Test # 101650: INCR a non-existing key", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        const res = await client.INCR('INCRkey')
        expect(res === 1).to.be.true

        await client.disconnect();
    })

    it("Test # 101651: INCR a key with bad value", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('INCRkey', 'aaa')

        try {
            const res = await client.INCR('INCRkey')
        } catch (err) {
            expect(err.message).to.have.string('value not a integer')
        }

        await client.disconnect();
    })

    it("Test # 101652: INCR a key with a numeric value", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        await client.SET('INCRkey', 22)

        const res = await client.INCR('INCRkey')
        expect(res === 23).to.be.true

        await client.disconnect();
    })

    it("Test # 101653: INCR against a  data-type", async () => {

        let client = createClient({url: 'redis://localhost:3000'})
        await client.connect()

        ydb.set('^%ydbxiderK', 'hashkey', 22)
        ydb.set('^%ydbxiderKDT', 'hashkey', 'hash')


        try {
            const res = await client.INCR('hashkey')
        } catch (err) {
            expect(err.message).to.have.string("WRONGTYPE Operation against a key holding the wrong kind of value")
        }

        await client.disconnect();
    })

})
