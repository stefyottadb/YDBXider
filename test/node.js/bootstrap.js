/*
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#   This source code contains the intellectual property         #
#   of its copyright holder(s), and is made available           #
#   under a license.  If you do not know the terms of           #
#   the license, please stop and do not read further.           #
#                                                               #
#################################################################
*/

const { expect } = require('chai');
const _ = require('lodash');
const globalVariables = _.pick(global, ['expect']);

before (async function () {
  global.expect = expect;

});

// close browser and reset global variables
after (async function () {
  global.expect = globalVariables.expect;
});

