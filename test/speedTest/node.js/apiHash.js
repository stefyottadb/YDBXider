/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const ydb = require('nodem').Ydb()
const {stdout} = require('process')
const {expect} = require('chai');

describe("API Speed", async () => {
    it("Test # 10: Measure speed using API and nodem using procedures", async () => {
        ydb.open()

        ydb.set('xider','noParamsValidation',0)
        ydb.procedure('init^xider')

        execTest('HSET')
        execTest('HSETNX')
        execTest('HGET')
        execTest('HDEL')
        execTest('HLEN')
        execTest('HSTRLEN')
        execTest('HKEYS')
        execTest('HVALS')
        execTest('HEXISTS')
        execTest('HMSET')
        execTest('HMGET')
        execTest('HGETALL')
        execTest('HRANDFIELD')
        execTest('HINCRBY')
        execTest('HINCRBYFLOAT')

        ydb.procedure('terminate^xider')
    })

    it("Test # 11: Measure speed using API and nodem using procedures and no param validation", async () => {
        ydb.set('xider','noParamsValidation',1)
        ydb.procedure('init^xider')

        execTest('HSET')
        execTest('HSETNX')
        execTest('HGET')
        execTest('HDEL')
        execTest('HLEN')
        execTest('HSTRLEN')
        execTest('HKEYS')
        execTest('HVALS')
        execTest('HEXISTS')
        execTest('HMSET')
        execTest('HMGET')
        execTest('HGETALL')
        execTest('HRANDFIELD')
        execTest('HINCRBY')
        execTest('HINCRBYFLOAT')

        ydb.procedure('terminate^xider')
    })
})


const execTest = function (command, asFunction = false) {
    try {
        let results = []
        let max = 1000;

        console.log('______________________________')
        console.log('\n\n\nUsing: nodem: ' + command)

        for (let run = 0; run < 50; run++) {
            stdout.write(".")

            let start = Date.now();

            for (let key = 1; key < max; key++) {
                switch (command) {
                    case 'HSET': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'data', 1, 'field', 'sayhello')
                        ydb.set('xider', 'data', 1, 'value', 'hello world')
                        ydb.set('xider', 'data', 2, 'field', 'sayhello2')
                        ydb.set('xider', 'data', 2, 'value', 'hello world2')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HSETNX': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'field', 'sayhellonx')
                        ydb.set('xider', 'value', 'hello world')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HGET': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'field', 'sayhellonx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HDEL': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'fields', 1, 'sayhellonx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HLEN': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HSTRLEN': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'field', 'sayhellonx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HKEYS': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HVALS': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HEXISTS': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'field', key + 'nx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HMSET': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'data', 1, 'field', 'sayhello')
                        ydb.set('xider', 'data', 1, 'value', 'hello world')
                        ydb.set('xider', 'data', 2, 'field', 'sayhello2')
                        ydb.set('xider', 'data', 2, 'value', 'hello world2')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HMGET': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'fields', 1, 'sayhello')
                        ydb.set('xider', 'fields', 2, 'hello world')
                        ydb.set('xider', 'fields', 3, 'sayhello2')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HGETALL': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HRANDFIELD': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HINCRBY': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'field', key + 'nx')
                        ydb.set('xider', 'increment', 3)
                        ydb.function(command + '^xider')

                        break
                    }
                    case 'HINCRBYFLOAT': {
                        ydb.set('xider', 'key', key + 'nx')
                        ydb.set('xider', 'field', key + 'nx')
                        ydb.set('xider', 'increment', 3.5)
                        ydb.function(command + '^xider')

                        break
                    }
                }
            }

            let finish = Date.now();

            let elap = (finish - start) / 1000;
            results[run] = Math.trunc((max / elap))
        }

        let result = 0
        results.forEach(run => result += run)
        result = result / results.length

        console.log('\nMean speed value (calls/sec x 50):', result)
        console.log('______________________________')
    } catch (e) {
        expect(1 === 2).to.be.true
    }
}
