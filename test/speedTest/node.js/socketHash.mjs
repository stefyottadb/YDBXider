/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

import {stdout, exit} from "process"
import {createClient} from 'redis';
import {expect} from 'chai'

describe("Speed comparison", async () => {
    it("Test # 25: Measure HSET speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HSET")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HSET")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 26: Measure HGET speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HGET")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HGET")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 27: Measure HDEL speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HDEL")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HDEL")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 28: Measure HINCRBY speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HINCRBY")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HINCRBY")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 29: Measure HSETNX speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HSETNX")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HSETNX")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 30: Measure HLEN speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HLEN")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HLEN")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 31: Measure HSTRLEN speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HSTRLEN")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HSTRLEN")

        computePercentage(xiderResult, redisResult)
    })

    /*
    it("Test # 32: Measure HKEYS speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HKEYS")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HKEYS")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 33: Measure HVALS speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HVALS")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HVALS")

        computePercentage(xiderResult, redisResult)
    })
     */

    it("Test # 34: Measure HEXISTS speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HEXISTS")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HEXISTS")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 35: Measure HMGET speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HMGET")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HMGET")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 36: Measure HGETALL speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HGETALL")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HGETALL")

        computePercentage(xiderResult, redisResult)
    })


    /*
    it("Test # 37: Measure HINCRBYFLOAT speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "HINCRBYFLOAT")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "HINCRBYFLOAT")

        computePercentage(xiderResult, redisResult)
    })

     */

})

const compute = async function (client, service, command) {
    let max = 1000;
    let runs = 50
    let results = []
    let value

    try {
        client.on('error', err => {
            console.log("\n", service, ' client Error', err)

            exit(1)
        })
        await client.connect()

    } catch (err) {
        return
    }

    console.log()
    console.log('\nUsing: ', service)

    for (let run = 0; run < runs; run++) {
        stdout.write(".")

        let start = Date.now();

        for (let key = 1; key < max; key++) {
            try {
                if (command === 'HSET') value = await client.HSET("hash" + key.toString() + '-' + run.toString(), "field1", "value1", "field2", "value2")
                if (command === 'HSETNX') value = await client.HSETNX("hash" + key.toString() + '-' + run.toString(), "field1", "value1", "field2", "value2")
                if (command === 'HGET') value = await client.HGET("hash" + key.toString() + '-' + run.toString(), "field1", "value1")
                if (command === 'HDEL') value = await client.HDEL("hash" + key.toString() + '-' + run.toString(), "field1")
                if (command === 'HINCRBY') value = await client.HINCRBY("hash" + key.toString() + '-' + run.toString(), "field1", 22)
                if (command === 'HLEN') value = await client.HLEN("hash" + key.toString() + '-' + run.toString(), "field1")
                if (command === 'HSTRLEN') value = await client.HSTRLEN("hash" + key.toString() + '-' + run.toString(), "field1")
                if (command === 'HKEYS') value = await client.HKEYS("hash" + key.toString() + '-' + run.toString())
                if (command === 'HVALS') value = await client.HVALS("hash" + key.toString() + '-' + run.toString())
                if (command === 'HEXISTS') value = await client.HEXISTS("hash" + key.toString() + '-' + run.toString(), "field1")
                if (command === 'HMSET') value = await client.HMSET("hash" + key.toString() + '-' + run.toString(), "field1","newvalue")
                if (command === 'HMGET') value = await client.HMGET("hash" + key.toString() + '-' + run.toString(), "field1","field2")
                if (command === 'HGETALL') value = await client.HGETALL("hash" + key.toString() + '-' + run.toString())
                if (command === 'HINCRBYFLOAT') value = await client.HINCRBYFLOAT("hash" + key.toString() + '-' + run.toString(),"fieldnew",2.12)

            } catch (err) {
                console.log(err)
            }
        }

        let finish = Date.now();
        let elap = (finish - start) / 1000;

        results[run] = Math.trunc((max / elap))
    }

    let result = 0
    results.forEach(run => result += run)
    result = result / results.length

    await client.disconnect();

    return result
}

const computePercentage = function (xiderResult, redisResult) {
    console.log('\nRedis: ', redisResult, 'commands per second')
    console.log('Xider: ', xiderResult, 'commands per second')

    const percentage = (xiderResult - redisResult) / ((redisResult + xiderResult) / 2) * 100
    console.log('\nPercentage: ', percentage.toFixed(2), ' % (positive > faster, negative > slower)')

}
