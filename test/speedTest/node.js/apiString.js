/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

const ydb = require('nodem').Ydb()
const {stdout} = require('process')
const {expect} = require('chai');

describe("API Speed", async () => {
    it("Test # 1: Measure speed using API and nodem using procedures and no validation", async () => {

        ydb.set('xider','noParamsValidation',1)
        ydb.procedure('init^xider')
        ydb.kill('xider')

        execTest('SET')
        execTest('GET')
        execTest('EXISTS')
        execTest('APPEND')
        execTest('STRLEN')
        execTest('GETRANGE')
        execTest('SUBSTR')
        execTest('SETRANGE')
        execTest('RENAME')
        execTest('RENAMENX')
        execTest('GETDEL')
        execTest('MSET')
        execTest('MSETNX')
        execTest('MGET')
        execTest('TTL')
        execTest('PTTL')
        execTest('EXPIRE')
        execTest('INCR')
        execTest('INCRBY')
        execTest('INCRBYFLOAT')
        execTest('DECR')
        execTest('DECRBY')
        execTest('DEL')

        ydb.procedure('terminate^xider')
    })

    it("Test # 2: Measure speed using API and nodem using procedures", async () => {
        ydb.kill('xider')
        ydb.set('xider','noParamsValidation',0)
        ydb.procedure('init^xider')

        execTest('SET')
        execTest('GET')
        execTest('EXISTS')
        execTest('APPEND')
        execTest('STRLEN')
        execTest('GETRANGE')
        execTest('SUBSTR')
        execTest('SETRANGE')
        execTest('RENAME')
        execTest('RENAMENX')
        execTest('GETDEL')
        execTest('MSET')
        execTest('MSETNX')
        execTest('MGET')
        execTest('TTL')
        execTest('PTTL')
        execTest('EXPIRE')
        execTest('INCR')
        execTest('INCRBY')
        execTest('INCRBYFLOAT')
        execTest('DECR')
        execTest('DECRBY')
        execTest('DEL')

        ydb.procedure('terminate^xider')
    })
})

const execTest = function (command, asFunction = false) {
    try {
        let results = []
        let max = 1000;

        console.log('______________________________')
        console.log('\n\n\nUsing: nodem: ' + command)

        for (let run = 0; run < 50; run++) {
            stdout.write(".")

            let start = Date.now();

            for (let key = 1; key < max; key++) {
                switch (command) {
                    case 'SET': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'value', 'hello world')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'GET': {
                        ydb.set('xider', 'key', key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'DEL': {
                        ydb.set('xider', 'keys', 1, key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'EXISTS': {
                        ydb.set('xider', 'keys', 1, key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'DEL': {
                        ydb.set('xider', 'keys', 1, key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'EXISTS': {
                        ydb.set('xider', 'keys', 1, key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'STRLEN': {
                        ydb.set('xider', 'key', key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'APPEND': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'value', 'new string')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'GETRANGE': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'start', 2)
                        ydb.set('xider', 'end', 2)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'SUBSTR': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'start', 1)
                        ydb.set('xider', 'end', 5)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'SETRANGE': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'offset', 3)
                        ydb.set('xider', 'value', "appendedData")
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'RENAME': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'newkey', 'new' + key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'RENAMENX': {
                        ydb.set('xider', 'key', key)
                        ydb.set('xider', 'newkey', 'new' + key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'GETDEL': {
                        ydb.set('xider', 'key', key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'MSET': {
                        ydb.set('xider', 'keys', 1, key)
                        ydb.set('xider', 'values', 1, key)
                        ydb.set('xider', 'keys', 2, key)
                        ydb.set('xider', 'values', 2, key)
                        ydb.set('xider', 'keys', 3, key)
                        ydb.set('xider', 'values', 3, key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'MSETNX': {
                        ydb.set('xider', 'keys', 1, key)
                        ydb.set('xider', 'values', 1, key)
                        ydb.set('xider', 'keys', 2, key)
                        ydb.set('xider', 'values', 2, key)
                        ydb.set('xider', 'keys', 3, key)
                        ydb.set('xider', 'values', 3, key)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'MGET': {
                        ydb.set('xider', 'keys', 1, key + 'a')
                        ydb.set('xider', 'keys', 2, key + 'b')
                        ydb.set('xider', 'keys', 3, key + 'c')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'TTL': {
                        ydb.set('xider', 'key', key + 'a')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'PTTL': {
                        ydb.set('xider', 'key', key + 'a')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'EXPIRE': {
                        ydb.set('xider', 'key', key + 'a')
                        ydb.set('xider', 'time', 10)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'INCR': {
                        ydb.set('xider', 'key', key + 'a')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'INCRBY': {
                        ydb.set('xider', 'key', key + 'a')
                        ydb.set('xider', 'increment', 10)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'INCRBYFLOAT': {
                        ydb.set('xider', 'key', key + 'a')
                        ydb.set('xider', 'increment', .25)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'DECR': {
                        ydb.set('xider', 'key', key + 'a')
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                    case 'DECRBY': {
                        ydb.set('xider', 'key', key + 'a')
                        ydb.set('xider', 'decrement', 10)
                        if (asFunction === false ) ydb.procedure(command + '^xider')
                        else ydb.function(command + '^xider')

                        break
                    }
                }
            }

            let finish = Date.now();

            let elap = (finish - start) / 1000;
            results[run] = Math.trunc((max / elap))
        }

        let result = 0
        results.forEach(run => result += run)
        result = result / results.length

        console.log('\nMean speed value (calls/sec x 50):', result)
        console.log('______________________________')
    } catch (e) {
        console.log(e)
        expect(1 === 2).to.be.true
    }
}
