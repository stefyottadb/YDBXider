/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/

import {stdout, exit} from "process"
import {createClient} from 'redis';
import {expect} from 'chai'

describe("Speed comparison", async () => {
    it("Test # 1: Measure SET speed against Redis", async () => {
        // XIDER
        let client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "SET")

        // REDIS
        client = createClient()
        const redisResult = await compute(client, "Redis", "SET")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 2: Measure GET speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "GET")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "GET")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 3: Measure DEL speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "DEL")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "DEL")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 4: Measure INCR speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "INCR")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "INCR")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 8: Measure EXISTS speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "EXISTS")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "EXISTS")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 9: Measure APPEND speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "APPEND")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "APPEND")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 10: Measure STRLEN speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "STRLEN")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "STRLEN")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 11: Measure GETRANGE speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "GETRANGE")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "GETRANGE")

        computePercentage(xiderResult, redisResult)
    })


    it("Test # 12: Measure SETRANGE speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "SETRANGE")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "SETRANGE")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 13: Measure RENAME speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "RENAME")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "RENAME")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 14: Measure RENAMENX speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "RENAMENX")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "RENAMENX")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 15: Measure MSET speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "MSET")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "MSET")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 16: Measure MSETNX speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "MSETNX")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "MSETNX")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 17: Measure MGET speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "MGET")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "MGET")

        computePercentage(xiderResult, redisResult)
    })


    it("Test # 18: Measure INCRBY speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "INCRBY")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "INCRBY")

        computePercentage(xiderResult, redisResult)
    })

    /*
    it("Test # 19: Measure INCRBYFLOAT speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "INCRBYFLOAT")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "INCRBYFLOAT")

        computePercentage(xiderResult, redisResult)
    })

     */


    it("Test # 20: Measure DECR speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "DECR")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "DECR")

        computePercentage(xiderResult, redisResult)
    })

    it("Test # 21: Measure DECRBY speed against Redis", async () => {
        // REDIS first
        let client = createClient()
        const redisResult = await compute(client, "Redis", "DECRBY")

        // XIDER
        client = createClient({url: 'redis://localhost:3000'})
        const xiderResult = await compute(client, "Xider", "DECRBY")

        computePercentage(xiderResult, redisResult)
    })
})

const compute = async function (client, service, command) {
    let max = 1000 // 1000
    let runs = 50 // 50
    let results = []
    let value

    try {
        client.on('error', err => {
            console.log("\n", service, ' client Error', err)

            exit(1)
        })
        await client.connect()

    } catch (err) {
        return
    }

    console.log()
    console.log('\nUsing: ', service)

    for (let run = 0; run < runs; run++) {
        stdout.write(".")

        let start = Date.now();

        for (let key = 1; key < max; key++) {
            try {
                if (command === 'SET') await client.SET(key.toString(), 'hello world')
                if (command === 'GET') value = await client.GET(key.toString())
                if (command === 'DEL') value = await client.DEL(key.toString())
                if (command === 'INCR') value = await client.INCR("inca"+ key.toString())
                if (command === 'EXISTS') value = await client.EXISTS("exists" + key.toString())
                if (command === 'APPEND') value = await client.APPEND("append" + key.toString(), 'new')
                if (command === 'STRLEN') value = await client.STRLEN("append" + key.toString())
                if (command === 'GETRANGE') value = await client.GETRANGE(key.toString(), 2, 5)
                if (command === 'SUBSTR') value = await client.SUBSTR(key.toString(), 2, 5)
                if (command === 'SETRANGE') value = await client.SETRANGE(key.toString(), 2, 'test')
                if (command === 'RENAME') {
                    try {
                        value = await client.RENAME(key.toString(), 'newkey')
                    } catch (err) {}
                }
                if (command === 'RENAMENX') {
                    try {
                        value = await client.RENAMENX(key.toString(), 'newkey')
                    } catch (err) {}
                }
                if (command === 'MSET') await client.MSET([
                    "m1"+key.toString(), 'hello world',
                    "m2"+key.toString(), 'hello world',
                ])
                if (command === 'MSETNX') await client.MSETNX([
                    "m1"+key.toString(), 'hello world',
                    "m2"+key.toString(), 'hello world',
                ])
                if (command === 'MGET') await client.MGET([
                    "m1"+key.toString(),
                    "m2"+key.toString(),
                ])
                if (command === 'INCRBY') value = await client.INCRBY("ewkey2"+ key.toString(),5)

                if (command === 'INCRBYFLOAT') {
                    value = await client.INCRBYFLOAT("ewkey3"+ key.toString(),1.23)
                }
                if (command === 'DECR') value = await client.DECR("aa"+ key.toString())
                if (command === 'DECRBY') value = await client.DECRBY("aaa"+ key.toString(),23)

            } catch (err) {
                console.log(err)
            }
        }

        let finish = Date.now();
        let elap = (finish - start) / 1000;

        results[run] = Math.trunc((max / elap))
    }

    let result = 0
    results.forEach(run => result += run)
    result = result / results.length

    await client.disconnect();

    return result
}

const computePercentage = function (xiderResult, redisResult) {
    console.log('\nRedis: ', redisResult, 'commands per second')
    console.log('Xider: ', xiderResult, 'commands per second')

    const percentage = (xiderResult - redisResult) / ((redisResult + xiderResult) / 2) * 100
    console.log('\nPercentage: ', percentage.toFixed(2), ' % (positive > faster, negative > slower)')

}
