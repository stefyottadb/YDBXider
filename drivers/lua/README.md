# Lua driver for Xider

The Lua driver for Xider (xider-lua) is built to look like redis-lua on at least a peripheral level so that you can run the same or very similar Lua code against Xider, only much faster. It achieves the speed by making in-process calls to Xider on the same machine. If you need to access to remote machines, use the redis-lua Lua module instead, to access Xider's RESP socket protocol.

To use xider-lua in your program, simply change `require 'redis'` to `require 'xider'`.

## Installation

Prerequisites:

* Lua 5.1 or higher is required
* [YottaDB](https://yottadb.com/product/get-started/)
* [lua-yottadb](https://github.com/anet-be/lua-yottadb)
* [Xider](../../README.md)

If you installed Xider using the `cmake` installer, then `$ydb_dist/ydb_env_set` will set up your environment and `ydb_routines` as required. Otherwise, prepare your shell directory and environment:

```sh
$ cd drivers/lua
$ export ydb_routines="$PWD/../../routines $ydb_routines"
```

To use this module in an application, until an installer is written for this driver, you will need to run the following line to add to your LUA_PATH, substituting your YDBXider directory:

```sh
export LUA_PATH="<YDBXider>/drivers/lua/?.lua;$LUA_PATH"
```

## Operation

Now you are ready to experiment with Xider via Lua:

```lua
$ lua
> db = require('xider').connect()
> db:set('x',5)
OK
> db:get('x')
5
```

## CLI

You can run xider-lua as a command-line interface (akin to redis-cli):

```sh
$ ./xider.lua SET x 3 -- get x   # note the use of -- to separate commands
OK
3
```

## Testing

To do …

## Running the server

Since the Python driver calls Xider using fast in-process calls, it is not absolutely necessary to run the Xider server in the background, but it is advisable to run the server in at least the `--api-only` mode per the [server API documentation](../../docs/API.md#running-the-xider-server).
