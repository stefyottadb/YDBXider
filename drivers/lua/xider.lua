#!/usr/bin/env lua
-- Xider driver for Lua

---------------------------------------------------------------------------------------
-- Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.
-- All rights reserved.
--
--  This source code contains the intellectual property
--  of its copyright holder(s), and is made available
--  under a license.  If you do not know the terms of
--  the license, please stop and do not read further.
---------------------------------------------------------------------------------------

local ydb = require 'yottadb'

local _M = {}  -- define object for module data and functions

_M.logging_DEBUG = 10
_M.logging_INFO = 20
_M.logging_WARNING = 30
_M.logging = _M.logging_WARNING   -- when run as a script this is decremented by 10 each --verbose or -v command line parameter

-- Note the following driver versions all increment independently
_M._VERSION = 'xider-lua 0.0.1'
_M.xider_VERSION = nil   -- to be populated by xider_init(), called by connect()
_M.xider_keyFieldMaxLength = nil   -- to be populated by xider_init(), called by connect()

local callin_table = [[
    init: void init^xider()
    terminate: void terminate^xider()
    call: long call^xider(I:string*)
]]

-- Provide access to Xider's M functions and parameters
local M = ydb.require(callin_table)
local xider = ydb.node('xider')
local xider_ret = xider.ret   -- predefine these: it's faster
local xiderRet = ydb.node('xiderRet')
local xiderStatus = ydb.node('xiderStatus')
local xiderMulti = ydb.node('xiderMulti')
local xider_multi_queue = nil  -- initial definition of queue of commands sent between MULTI and EXEC or nil of outside a transaction

function _M.xider_init()
    --- Initialize Xider; store Xider meta constants in _M._VERSION and _M.keyFieldMaxLength.
    xider.driverName.__ = 'Lua'
    xider.driverVersion.__ = _M._VERSION:match(' ([^ ]+)')
    xider.description.__ = 'Redis-compatible Python driver for fast in-process API for Xider database'
    xider.noParamsValidation.__ = 0
    M.init()
    _M.keyFieldMaxLength = tonumber(xider.keyFieldMaxLength.__)
    _M.xider_VERSION = xider.version.__
    xider_multi_queue = nil  -- reinitialize queue of commands sent between MULTI and EXEC or nil of outside a transaction
end

function _M.xider_terminate()
    -- Opposite of init(): terminate use of xider, which removes the xider session.
    -- If this doesn't get called then the session will be removed when the Helper process
    -- detects that the PID doesn't exists anymore.
    M.terminate()
end

-- Map args to Xider-specific argnames for this command; cf. MR #152.
-- This is a table of argument names applicable to each different Xider command.
-- The code in command() below performs the table lookup.
-- Longer term, these should all be changed in Xider to make all params called paramN
local arg_names_default = {'key', 'value'}
local arg_names = {
    SET = {'key', 'value', function(i) return 'params'..tostring(i-2) end},
    DEL = {function(i) return 'keys', tostring(i) end},
    HSET = {'key', function(i) return 'data', tostring(math.floor(i/2)), 'field' end, function(i) return 'data', tostring(math.floor(i/2)), 'value' end},
    HGET = {'key', 'field'},
    HDEL = {'key', function(i) return 'fields', tostring(i-1) end},
    HINCRBY = {'key', 'field', 'increment'},
    INCRBY = {'key', 'increment'},
    WATCH = {function(i) return 'keys', tostring(i) end},
    -- Other commands use the arg_names_default above
}

function _M._xider_command(cmd, ...)
    -- Send a Redis command and its args to Xider via an in-process call
    xider:kill()
    cmd = cmd:upper()
    -- Map args to Xider-specific arg_names for this particular command.
    -- Most of the rest of this except M.call() can go away once Xider supports consistent params and return values
    local names = arg_names[cmd] or arg_names_default
    local backtrack = 0
    local subscripts = {}
    -- Loop through command's args, storing each in the `xider(name)` where arg `name` is taken from arg_names table
    for i, arg in ipairs({...}) do
        if i-backtrack > #names then
            -- When we run out of entries in the arg_names table, backup to the beginning of the callables
            -- so they get used on repeat for the remaining args. For example HSET can have an infinite number of parameters
            while backtrack<i and type(names[i-1-backtrack])=='function' do  backtrack = backtrack+1  end
        end
        if i-backtrack > #names then
            return string.format("ERR wrong number of arguments for '%s' command", cmd)
        end
        local subscript = names[i-backtrack]
        if type(subscript)=='function' then  subscript = {subscript(i)}  end
        subscripts = type(subscript)=='table' and subscript or {subscript}
        ydb.set('xider', subscripts, arg)
    end
    if _M.logging < _M.logging_DEBUG then  io.stderr:write('Calling with:\n  ', ydb.dump('xider'):gsub('\n','\n  '), '\n => ')  end
    local status = tonumber(M.call(cmd))

    -- Handle transactions and EXEC specially, to dequeue multiple responses
    if cmd == 'EXEC' then
        local queue = xider_multi_queue
        xider_multi_queue = nil  -- stop queuing
        if status == -3 or not queue then  return "-ERR EXEC without MULTI"  end
        if status == -5 then  return nil  end  -- Aborted transaction because a WATCHed key was modified
        if status == -6 then  return "-EXECABORT Transaction discarded because of previous errors."  end
        -- extract list of responses from xiderRet by repeated calls to _M._process_result()
        local responses = {}
        for i, cmd in ipairs(queue) do
            local response = _M._process_result(cmd, tonumber(xiderStatus[i].__), xiderRet[i])
            table.insert(responses, response)
        end
        return responses
    elseif cmd == 'DISCARD' then  xider_multi_queue=nil  -- stop queuing
    elseif cmd == 'MULTI' and status==0 then  xider_multi_queue={}  return 'OK'  end  -- start queuing commands to pair against incoming results
    if xider_multi_queue then
        table.insert(xider_multi_queue, cmd)
        -- Testing xiderMulti is not in the published API: it's a hack to check whether the error returned cancels the transaction.
        -- In future this should be detected when, say, -10<status<0
        if xiderMulti.__ ~= '-1' then  return 'QUEUED'  end
        xider_multi_queue = nil  -- stop queuing on fatal errors
        -- fall through to report any status errors
    end

    return _M._process_result(cmd, status)
end

function _M._process_result(cmd, status, result_node)
    -- Process and return a command result.
    -- `result_node` defaults to xider_ret; must be a lua-yottadb node instance where the command results are expected to
    -- be found in the database (this is a different location when processing an EXEC response queue)
    result_node = result_node or xider_ret  -- default
    -- handle non-orthogonal responses: should be fixed in Xider
    if status >= 0 and ({DEL=1, HSET=1, HDEL=1, HLEN=1, INCRBY=1, HINCRBY=1})[cmd] then
        if status == 0 and (cmd == 'INCRBY' or cmd == 'HINCRBY') then
            status = 1   -- these return result in result_node.__
        else  return status  end
    end
    if status==0 then  return 'OK'
    elseif status == 1 then
        local retval = result_node.__
        -- special-case commands that must return an integer type but which (unlike HLEN) are not returned directly in status
        if cmd=='INCRBY' or cmd=='HINCRBY' then  retval = tonumber(retval)  end
        return retval
    end
    -- exception for commands that return odd (non-orthogonal) constants meaning "no data"
    if cmd == 'GET' and status==-3 then  return nil  end
    if cmd == 'HGET' and (status==-7 or status==-5) then  return nil  end
    -- still need to handle case of status==2 (large string returned) and error codes: fix in Xider to return error messages
    return string.format("Error %s: Not implemented", status)
end

function _M.xider_command(cmd, ...)
    -- Logged version of _command: writes "<cmd> <args> <...> => <result>" to stderr; max 100 chars per element
    if _M.logging <= _M.logging_INFO then
        io.stderr:write('Command: ', cmd, ' ')
        for _, arg in ipairs({...}) do  io.stderr:write(arg:sub(1, 100), ' ')  end
        io.stderr:write('=> ')
    end
    local retval = _M._xider_command(cmd, ...)
    if _M.logging <= _M.logging_INFO then
        io.stderr:write(type(retval)=='string' and ("'"..retval:sub(1, 100).."'") or tostring(retval), '\n')
    end
    return retval
end


--- Define driver object, instances of which act like a redis-lua connection object

local driver = {}  -- metatable to hold driver methods

local function Xider()  -- Return initialized driver
    _M.xider_init()
    return setmetatable({}, driver)
end

function driver:close(...)  return _M.xider_terminate(...)  end

-- Make it so driver_instance:CMD(...) invokes _M.driver_command(CMD, ...)
function driver:__index(method)
    -- create function name in connection instance on first use for faster use next time
    local function f(self, ...)  return _M.xider_command(method, ...)  end

    -- emulate redis-lua error behaviour when trying to invoke a non-lower-case method
    if method ~= method:lower() then
        error("attempt to call method "..method.." (a nil value)", 2)
    end
    self[method] = f
    return f
end

-- Return an object representing a connection to Xider; analogous to redis.connect()
-- Ignore _addr and _port because in-process Xider connections don't need them
function _M.connect(_addr, _port)  return Xider()  end


--- Detect whether this file was called as a script or imported as a module
if pcall(debug.getlocal, 4, 1) then
    return _M   -- if imported as a module
end

--- Perform as a script if run from the command line

-- If xider is run from within the Xider repository directory structure then automatically find xider.m routines directory
local driver_dir = debug.getinfo(1).source:match("@?(.*/)")
local routines_dir = driver_dir .. '../../routines'
local exists = io.open(routines_dir .. '/xider.m')
if exists then
    exists:close()
    ydb.set("$ZROUTINES", os.getenv("ydb_routines") .. " " .. routines_dir)
end

local help = false
while arg[1] and arg[1]:sub(1,1)=='-' do
    local val = arg[1]
    table.remove(arg, 1)
    if val=='-h' or val=='--help' then  help=true  end
    local _ = _M
    if val=='-v' or val=='--verbose' then
        _.logging = _.logging==_.logging_WARNING and _.logging_INFO-5 or _.logging-5
    end
    if val=='-vv' then  _.logging=_.logging_INFO-5*2  end
    if val=='-vvv' then  _.logging=_.logging_INFO-5*3  end
end
if help or #arg == 0 then
    print("Xider module for Lua which may also be run as a Xider CLI to send commands to Xider")
    print("Usage: xider.lua [options] cmd1 [arg1 arg2 ...] [-- cmd2 [arg1 arg2 ...]] ...")
    print("Where options may be:")
    print("  -v, --verbose   Specify increasing levels of logging each time it is used")
    print("  -h, --help      Show this help message")
    print("  --              Command separator")
    return
end

-- loop through and send all commands on command line one at a time
local db = _M.connect()
local start,i = 1,1
repeat
    while arg[i] and arg[i] ~= '--' do  i=i+1  end
    response = _M.xider_command(table.unpack(arg, start, i-1))
    print(response)
    i = i+1  start = i
until not arg[i]
_M.xider_terminate()
