#!/usr/bin/env python3
# Xider driver for Python

#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#  This source code contains the intellectual property	        #
#  of its copyright holder(s), and is made available            #
#  under a license.  If you do not know the terms of            #
#  the license, please stop and do not read further.            #
#                                                               #
#################################################################

""" Implement a Xider driver for Python based on subclassing Redis-py for compatibility with existing Redis-py clients. """

import os
import re
import logging

import yottadb as ydb

from redis import *
from redis.connection import (
    UnixDomainSocketConnection,
    ConnectionPool,
)
try:
    from redis.connection import AbstractConnection
except ImportError:
    from redis.connection import UnixDomainSocketConnection as AbstractConnection  # Make it work with older versions of redis

__all__ = [
    "__version__", "__version_info__",
    "Driver",
    "Xider", "Redis",
]

def tuple_version(string):
    """ Split version number into a tuple for easy version number comparison """
    return tuple(n if n.isdecimal() else n for n in re.match(r'(\d+)\.(\d+)\.(\d+)(.*)', string).groups() if n)

# Note the following driver version increments independently from the main Xider version
__version__ = '0.0.1'
__version_info__ = tuple_version(__version__)

logger = logging.getLogger(__name__)

class Mfuncs:
    """ Provide access to M functions defined in `xider.ci` as if they were methods of this class.
        `xider.ci` must reside in the the same directory as this python file """
    def __init__(self, ci_file):
        """ Initialise M access using the supplied call-in file ci_file """
        ci_handle = ydb.open_ci_table(ci_file)
        ydb.switch_ci_table(ci_handle)

    def __getattr__(self, key):
        """ Make Mfuncs instance.ROUTINE() invoke M routine ROUTINE via call-in table """
        def caller(*args, has_retval=None):
            if type(has_retval) != bool:
                func = caller.__qualname__
                raise TypeError(f"{func}: keyword argument 'has_retval' must be boolean")
            return ydb.cip(key, args, has_retval=has_retval)
        ret = caller
        return ret

M = Mfuncs(os.path.join(os.path.dirname(__file__), 'xider.ci'))

# predefine most-used access keys to YottaDB database
xider = ydb.Key('xider')
xider_ret = xider['ret']
xiderRet = ydb.Key('xiderRet')
xiderStatus = ydb.Key('xiderStatus')
xiderMulti = ydb.Key('xiderMulti')

class Driver:
    """ Python driver for Xider.
        To print commands and results, call logging.basicConfig(level=logging.[INFO|DEBUG])
            In INFO mode it prints the command+result after it returns.
            In DEBUG mode it aslo prints the command before it is invoked """
    def __init__ (self, decode_responses=True):
        """ Initialize Xider
            decode_responses if True will decode in utf-8; otherwise using whatever decoder (string) it is set to """
        self.decode_responses = 'utf-8' if decode_responses is True else decode_responses
        self.init()  # Initialize the Xider connection
        self.reset()  # Reset current transaction

    def __enter__(self): return self
    def __exit__ (self, _exc_type, _exc_val, _exc_tb): self.terminate()
    def close(self): self.terminate()

    def init(self):
        """ Initialize Xider; return Xider meta constants in self.xider_version and self.keyFieldMaxLength. """
        xider['driverName'].value = 'Python'
        xider['driverVersion'].value = __version__
        xider['description'].value = 'Redis-compatible Python driver for fast in-process API for Xider database'
        xider['noParamsValidation'] = '0'
        M.init(has_retval=False)
        self.keyFieldMaxLength = int(xider['keyFieldMaxLength'].value)
        xider_version = xider['version'].value.decode('utf-8')
        self.xider_version = tuple_version(xider_version)

    def reset(self):
        """ Reset driver including current transaction """
        self.multi_queue = None  # queue of commands sent during MULTI, or None if not between MULTI and EXEC

    def terminate(self):
        """ Opposite of init(): terminate use of xider, which removes the xider session.
            If this doesn't get called then the session will be removed when the Helper process
            detects that the PID doesn't exists anymore. """
        M.terminate(has_retval=False)

    def decode(self, data):
        """ Decode data if decoding is turned on """
        return data.decode(self.decode_responses) if self.decode_responses else data

    # Map args to Xider-specific argnames for this command; cf. MR #152.
    # This is a table of argument names applicable to each different Xider command.
    # The code in command() below performs the table lookup.
    # Longer term, these should all be changed in Xider to make all params called paramN
    arg_names_default = ('key', 'value')
    arg_names = {
        'SET': ('key', 'value', lambda i: (f'params{i-2}',)),
        'DEL': (lambda i: ('keys', str(i)),),
        'HSET': ('key', lambda i: ('data',str(i//2),'field'), lambda i: ('data',str(i//2),'value')),
        'HGET': ('key', 'field'),
        'HDEL': ('key', lambda i: ('fields', str(i-1))),
        'HINCRBY': ('key', 'field', 'increment'),
        'INCRBY': ('key', 'increment'),
        'WATCH': (lambda i: ('keys', str(i)),),
        # Other commands use the args_names_default above
    }

    def _command(self, cmd, *args):
        """ Send a Redis command and its args to Xider via an in-process call """
        xider.delete_tree()
        cmd = cmd.upper()
        # Map args to Xider-specific arg_names for this particular command.
        # Most of the rest of this except M.call() can go away once Xider supports consistent params and return values
        arg_names = self.arg_names.get(cmd, self.arg_names_default)
        backtrack = 0
        subscripts = []
        # Loop through command's args, storing each in the `xider(name)` where arg `name` is taken from arg_names table
        for i, arg in enumerate(args):
            if i-backtrack >= len(arg_names):
                while backtrack<=i and callable(arg_names[i-1-backtrack]): backtrack += 1  # repeat the callables for the remaining args
            if i-backtrack >= len(arg_names):
                return f"ERR wrong number of arguments for '{cmd}' command"
            subscript = arg_names[i-backtrack]
            if callable(subscript): subscript = subscript(i+1)
            subscripts = subscript if isinstance(subscript, tuple) else (subscript,)
            if isinstance(arg, int): arg = str(arg)
            ydb.set('xider', subscripts, arg)
        if logging.root.level < logging.DEBUG:  # only print if more logging than DEBUG is specified
            xider_tree = ydb.load_tree(xider, [], {}, first_call=True)  # equivalent to {xider.load_tree()} but works around bug !58 in current release version of YDBPython
            logger.debug(f' Parameter tree sent to Xider: xider={xider_tree}')
        status = int(M.call(cmd, has_retval=True))

        # Handle transactions and EXEC specially, to dequeue multiple responses
        if cmd == 'EXEC':
            queue = self.multi_queue
            self.multi_queue = None  # stop queuing
            if status == -5: return None  # Aborted transaction because a WATCHed key was modified
            if status == -3 or queue is None: return self.decode(b"-ERR EXEC without MULTI")
            if status == -6: return self.decode(b"-EXECABORT Transaction discarded because of previous errors.")
            # extract list of responses from xiderRet by repeated calls to self._process_result()
            return [
                self._process_result(cmd, int(xiderStatus[str(i)].value), result_key=xiderRet[str(i)])
                    for i,cmd in enumerate(queue, start=1)
            ]
        elif cmd == 'DISCARD': self.multi_queue = None  # stop queuing
        elif cmd == 'MULTI' and status==0:
            self.multi_queue = []  # start queuing commands to pair against incoming results
            return self.decode(b'OK')
        if self.multi_queue is not None:
            self.multi_queue.append(cmd)
            # Testing xiderMulti is not in the published API: it's a hack to check whether the error returned cancels the transaction.
            # In future this should be detected when, say, -10<status<0
            if xiderMulti.value != '-1': return self.decode(b'QUEUED')
            self.multi_queue = None  # stop queuing on fatal errors
            # fall through to report any status errors

        return self._process_result(cmd, status)

    def _process_result(self, cmd, status, result_key=xider_ret):
        """ Process and return a command result
            `result_key` must be a YDBPython key instance where the command results are expected to be
            found in the database (this differs between processing individual commands and an EXEC queue ) """
        # handle non-orthogonal responses: should be fixed in Xider
        if status >= 0 and cmd in ('DEL', 'HSET', 'HDEL', 'HLEN', 'INCRBY', 'HINCRBY'):
            if status == 0 and (cmd == 'INCRBY' or cmd == 'HINCRBY'): status = 1   # these return result in result_key.value
            else: return status
        if status == 0: return self.decode(b'OK')
        elif status == 1:
            retval = result_key.value
            # special-case commands that must return an integer type but which (unlike HLEN) are not returned directly in status
            if cmd in ['INCRBY', 'HINCRBY']: retval = int(retval)
            if self.decode_responses and isinstance(retval, bytes): retval = retval.decode(self.decode_responses)
            return retval
        # exception for commands that return odd (non-orthogonal) constants meaning "no data"
        if cmd == 'GET' and status == -3: return None
        if cmd == 'HGET' and status in (-7, -5): return None
        # still need to handle case of status==2 (large string returned) and error codes: fix in Xider to return error messages
        return f"Error {status}: Not implemented"

    def command(self, cmd, *args):
        """ Logged version of _command """
        if logging.root.level <= logging.INFO:
            message = f"Command: {cmd} {' '.join(repr(arg) for arg in args)}"
            logger.debug(message)
        retval = self._command(cmd, *args)
        if logging.root.level <= logging.INFO:
            logger.info(f" {message} => {retval[:100] if isinstance(retval, (str, bytes)) else retval!r}")
        return retval

    def __getattr__(self, cmd):
        """ Make driver instance.CMD() invoke Redis command CMD via Xider """
        return lambda *args: self.command(cmd, *args)


# Subclass redis-py classes to make them use in-process Xider instead of a RESP connection

class XiderConnection(AbstractConnection):
    """ Manages in-process communication to and from a Xider server
        by overriding send_command and receive.
        To print commands and results, call logging.basicConfig(level=logging.[INFO|DEBUG])
            In INFO mode it prints the command+result after it returns.
            In DEBUG mode it aslo prints the command before it is invoked """

    def __init__(self, gbldir=None, **kwargs):
        """ Open a connection to YottaDB database at gbldir; defaults to os.getenv(gbldir) """

        if not gbldir:
            gbldir = os.environ.get('ydb_gbldir', "")
        if gbldir:
            os.environ['ydb_gbldir'] = gbldir
        self.gbldir = gbldir
        kwargs.pop('port', None)  # Ignore any port information as Xider doesn't require a port to connect
        if AbstractConnection == UnixDomainSocketConnection:
            # If we used our import hack to support old versions of redis-py via UnixDomainSocketConnection,
            # then open 'socket' at /dev/null since we don't actually use it.
            kwargs['path'] = '/dev/null'
        super().__init__(**kwargs)
        self.driver = Driver(decode_responses=False)
        self._queued_commands = []

    def repr_pieces(self):
        """ Return essential identifiers relating to the connection (shown when printing a connection) """
        pieces = [("gbldir", self.gbldir), ("db", self.db)]
        if self.client_name:
            pieces.append(("client_name", self.client_name))
        return pieces

    def _connect(self):
        """ Connect to database (which does init() for Xider) """
        # let connection do the decoding rather than Xider driver
        self.driver.init()  # This is ok to call twice: it is also called by Driver.__init__()
        return self.driver

    def disconnect(self, *args):
        """ Disconnect from Xider (just calls Xider terminate() as there are no actual connections) """
        if hasattr(self, 'driver'): self.driver.terminate()

    def _host_error(self):
        """ Return path to database gbldir (used when printing errors) """
        return self.gbldir

    def send_command(self, *args, queue_response=False, **kwargs):
        """ Send in-process command to Xider
            queue_response=True makes it able to store more than one response -- useful for pipelines """
        # Do our own logging here, instead of using that in driver.command() so we can show whether there are any kwargs
        sending_message = ''
        if logging.root.level <= logging.INFO:
            sending_message = f"{' '.join(repr(arg)[:100] for arg in args)}" + (f"kwargs={kwargs}" if kwargs else '')
            if logging.root.level <= logging.DEBUG:
                logger.debug("Sending: " + sending_message)
                sending_message = f"  Reply:  {args[0]}"  # store abbreviated form for reply message
            else:
                sending_message = "Sending: " + sending_message
        # save command and response to be returned later by read_response()
        response = self.driver._command(args[0], *args[1:])
        # store more than one response? -- used for pipelines; cf. send_packed_command()
        if not queue_response:
            self._queued_commands = []  # clear previous queue
        self._queued_commands.append( (args, response, sending_message) )

    def read_response(self, disable_decoding=False, **kwargs):
        """ Read the response from a previously sent command """
        try:
            sending_info = self._queued_commands.pop(0)
        except IndexError:
            sending_info = '', {}, '', "read_response() called with nothing queued as sent!"
        args, response, sending_message = sending_info
        if not disable_decoding:
            response = self.encoder.decode(response)
        if logging.root.level <= logging.INFO:
            logger.info(f" {sending_message} => {response[:100] if isinstance(response, (str, bytes)) else response!r}")
        return response

    def send_packed_command(self, command, check_health=True):
        """ Send an already packed command or sequence of commands to the Redis server """
        commands = [command] if isinstance(command, str) else command
        for cmd in commands:
            self.send_command(*cmd, queue_response=True)

    def pack_commands(self, commands):
        """ Pack multiple commands into final protocol for send_packed_command().
            This does nothing for Xider as the commands are already ready to send.
            Each command in commands is itself a sequence args """
        return commands

    def disconnect(self, *args):
        "Disconnects from the server, and reset transaction state"
        self.driver.reset()

class XiderConnectionPool(ConnectionPool):
    """ Subclass of ConnectionPool to override its default connection_class to use a XiderConnection """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, connection_class=XiderConnection, **kwargs)
        self.conn = self.make_connection()

    def get_connection(self, command_name, *keys, **options):
        """ Get a connection from the pool """
        # Overrides the default which re-uses connections from a pool;
        # Xider doesn't need a pool because a 'connection' doesn't really exist;
        # it's just a class instance to hold attributes required by Redis redis-py
        return self.conn

class Redis(Redis):
    """ Subclass of Redis that makes a connection to Xider instead
        This is potentially more compatible but slower than Xider() """

    def __init__(self, *args, **kwargs):
        super().__init__(*args,
            connection_pool=XiderConnectionPool(**kwargs),
            **kwargs)
        self.auto_close_connection_pool = True   # auto-calls self.close() which calls self.disconnect()

class Pipeline(Driver):
    """ Rudimentary version of Redis.Pipeline that simply forwards commands instantly to Xider """
    def __enter__(self): return self
    def __exit__ (self, _exc_type, _exc_val, _exc_tb): pass
    def __bool__ (self): return True  # Pipeline should always appear true
    def __init__ (self, parent):
        """ Initialize Pipeline -- a copy of Driver class except it doesn't init/terminate Xider itself """
        # Don't call super's __init__() because we don't want to re-init the driver
        self.decode_responses = parent.decode_responses
        self.reset()  # Reset current transaction

    def execute(self):
        return self.command('EXEC')

class Xider(Driver):
    """ Redis-like class that sends commands directly to Xider """

    def __init__(self, *args, **kwargs):
        super().__init__()

    def pipeline(self):
        x = Pipeline(self)
        return x

    def execute_command(self, *args, **kwargs):
        self.command(args[0], *args[1:])

    def delete(self, *args):
        return self.command('DEL', *args)
