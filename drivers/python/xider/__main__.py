#!/usr/bin/env python3
# Xider driver for Python

#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#  This source code contains the intellectual property	        #
#  of its copyright holder(s), and is made available            #
#  under a license.  If you do not know the terms of            #
#  the license, please stop and do not read further.            #
#                                                               #
#################################################################

""" Makes the xider module run as a xider-cli (akin to redis-cli), e.g.:
        python -m xider SET x 3 ...
"""

if __name__ == '__main__':
    import sys
    import os
    import logging

    # Allow import of ../xider (current directory) as xider
    driver_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    sys.path.insert(0, driver_dir)
    import xider

    # If xider is run from within the Xider repository directory structure then automatically find xider.m directory
    xider_routines_path = os.path.join(driver_dir, '..', '..', 'routines')
    if os.path.exists(os.path.join(xider_routines_path, 'xider.m')):
        os.environ['ydb_routines'] = f"{os.environ['ydb_routines']} {xider_routines_path}"

    import argparse
    parser = argparse.ArgumentParser(
        description = "Runs Xider's Python module as xider-cli (akin to redis-cli but for Xider)",
        usage = "Usage: python -m xider [options] cmd1 [arg1 arg2 ...] [-- cmd2 [arg1 arg2 ...]] ...",
    )
    parser.add_argument('-v', '--verbose', action='count', help="Turns on INFO level logging (shows Xider commands/results --xider is used); if used 2 or 3 times, add more logging.")
    Options, Args = parser.parse_known_args()
    if Options.verbose: import logging; logging.basicConfig(level=logging.INFO - 5*Options.verbose)

    import itertools
    # Split Args into groups of args for each command separated by '--'
    commands = [list(group) for k, group in itertools.groupby(Args, lambda x: x=='--') if not k]
    if not commands:
        parser.print_help()
        sys.exit()
    with xider.Driver() as x:
        for command in commands:
            result = x.command(*command)
            print('(nil)' if result is None else result)
