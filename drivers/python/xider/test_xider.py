#!/usr/bin/env python3
# Tests for the Xider driver for Python

#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#  This source code contains the intellectual property	        #
#  of its copyright holder(s), and is made available            #
#  under a license.  If you do not know the terms of            #
#  the license, please stop and do not read further.            #
#                                                               #
#################################################################

""" Test xider-py functionality.
Test script may be invoked directly or using pytest """

import os
import yottadb as ydb
import sys
import logging

# Set ydb_routines to ensure that Xider can be found during tests

driver_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

os.environ['ydb_routines'] = os.path.join(driver_dir, '..', '..', 'routines ') + os.environ['ydb_routines']

# Allow import of ../xider (current directory) as xider
sys.path.insert(0, driver_dir)
import xider

db = xider.Xider(decode_responses=True)

def check_xider_driver_direct():
    with xider.Driver() as x:
        print(f"Testing Xider version {'.'.join(map(str,x.xider_version))}")
        x.set('test1', '6')
        assert x.get('test1') == '6'

def test_getset():
    assert db.set('test2', '5') is True
    assert db.get('test2') == '5'

def test_hgetset():
    assert int(db.hset('htest', 'fieldA', '4')) in [0, 1]
    assert db.hget('htest', 'fieldA') == '4'

def test_del():
    db.set('test2', '5')
    assert db.delete('test2') == 1
    assert db.get('test2') == None

def test_hdel():
    assert int(db.hset('htest', 'fieldA', '4')) in [0, 1]
    assert db.hdel('htest', 'fieldA') == 1
    assert db.hget('htest', 'fieldA') == None

def test_incrby():
    db.set('test', '2')
    assert db.incrby('test', '3') == 5
    assert db.get('test') == '5'

def test_hincrby():
    db.hset('htest', 'fieldA', '4')
    assert db.hincrby('htest', 'fieldA', '3') == 7
    assert db.hget('htest', 'fieldA') == '7'

def test_hlen():
    db.hdel('htest')
    db.hset('htest', 'fieldA', '4')
    db.hset('htest', 'fieldB', '6')
    assert db.hlen('htest') == 2

def runtests():
    """ Run all tests in functions that start with 'test_' """
    check_xider_driver_direct()

    tests = {funcname:func for funcname,func in globals().items() if funcname.startswith('test_')}
    for test in tests.values():
        print(f"* {test.__name__} ", end='\n' if logging.root.level<=logging.INFO else '')
        test()
        if logging.root.level>logging.INFO: print()

if __name__ == '__main__':
    if '-v' in sys.argv or '--verbose' in sys.argv:
        logging.basicConfig(level=logging.DEBUG)
    runtests()
