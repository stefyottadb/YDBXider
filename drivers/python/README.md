# Python driver for Xider

The Python driver for Xider is built as a subclass of the redis-py driver. This ensures compatibility for existing users of redis-py.

To use it in your program, simply change `import redis` to `import xider as redis`.

## Installation

Prerequisites:

* [YottaDB](https://yottadb.com/product/get-started/)
* [YDBPython](https://docs.yottadb.com/ApplicationsManual/ydbpython.html#introduction-to-ydbpython): `pip install yottadb`
* [Xider](../../README.md)
* [redis-py](https://github.com/redis/redis-py?tab=readme-ov-file#installation)

If you have installed Xider using the `cmake` installer, then `$ydb_dist/ydb_env_set` will set up your environment and `ydb_routines` as required. Otherwise, prepare your shell directory and environment:

```sh
$ cd drivers/python
$ export ydb_routines="$PWD/../../routines $ydb_routines"
```

To use this module in an application, until a Python installer is written for this driver, you will need to add this directory to your PYTHONPATH as follows, substituting your YDBXider directory:

```sh
export PYTHONPATH="$PYTHONPATH:<YDBXider>/drivers/python"
```

## Operation

Now you are ready to experiment with Xider, which can be done by changing just 1 line of source code: change your Redis import line line to either:

1. Most compatible with Redis as it uses the Redis python layer and just changes the 'connection':
   * `from xider import Redis`
2. Faster (about 20%) because it avoids the Redis python class and uses a simplified version.
   * `from xider import Xider as Redis`

```python
$ python
>>> from xider import Redis  # now simply replace redis.Redis() with xider.Xider() or xider.Redis()
>>> db=Redis(decode_responses=True, verbose=True)  # verbose will print each command being sent
>>> db.set('x','3')
Sending: SET x 3 => OK
True
>>> db.get('x')
Sending: GET x => 3
'3'
```

Notes:

* Within the same program, the two can be intermingled: commands sent from instances of both `xider.Redis()` and `xider.Xider()`. This might be useful to run existing Redis application code using `xider.Redis()` for compatibility, and then for speed-critical parts of the code, send commands via `xider.Xider()`.
* But do not mix use of the two drivers within one transaction, because each driver needs to keep track of how many commands are queued within a transaction between MULTI and EXEC.

## CLI

You can also run xider as a command-line interface (akin to redis-cli). Usage is:

* `Usage: python -m xider [options] cmd1 [arg1 arg2 ...] [-- cmd2 [arg1 arg2 ...]] ...`

where `--` separates commands as follows:

```sh
$ python -m xider [--verbose] SET x 3 -- GET x
OK
3
```

## Testing

You can also run the unit tests:

```sh
$ python -m xider.test_xider
```

## Running the server

Since the Python driver calls Xider using fast in-process calls, it is not absolutely necessary to run the Xider server in the background, but it is advisable to run the server in at least the `--api-only` mode per the [server API documentation](../../docs/API.md#running-the-xider-server).
