<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

> This page is under review.
>
> The page is likely incorrect, contains invalid links, and/or needs technical review. In the future it may change substantially or be removed entirely.

---

# \<string> data type

- [SET](#set)
- [GET](#get)
- [DEL](#del)
- [EXISTS](#exists)
- [APPEND](#append)
- [STRLEN](#strlen)
- [GETRANGE](#getrange)
- [SUBSTR](#substr)
- [SETRANGE](#setrange)
- [RENAME](#rename)
- [RENAMENX](#renamenx)
- [GETDEL](#getdel)
- [MSET](#mset)
- [MSETNX](#msetnx)
- [MGET](#mget)
- [INCR](#incr)
- [INCRBY](#incrby)
- [INCRBYFLOAT](#incrbyfloat)
- [DECR](#decr)
- [DECRBY](#decrby)


> IMPORTANT: The maximum key length is 975 characters. This value may change. Use the value returned by the xider("ret","keyFieldMaxLength") after initialization.

---

## SET

Set key to hold the string value. If key already holds a value, it is overwritten, regardless of its type. Any previous time to live associated with the key is discarded on successful SET operation.

Compatible with [Redis  SET](https://redis.io/docs/latest/commands/set/)


### Syntax:

`set ret=$$SET^xider()` 
`do SET^xider`


### `xider` array entries:

| Name                 | Required | Description                                                                   |
|:---------------------|:---------|:------------------------------------------------------------------------------|
| ("key")              | Yes      | String                                                                        |
| ("value")            | Yes      | String (up to 1M)                                                             |
| OR                   |          |                                                                               |
| ("value",n)          | Yes      | If string is > 1M, chunks of 1M in each n subscript (1 based). Value is EMPTY |
| ("params","EX")      | No       | Number (seconds)                                                              |
| ("params","PX")      | No       | Number (milliseconds)                                                         |
| ("params","EXAT")    | No       | Number (seconds)                                                              |
| ("params","PXAT")    | No       | Number (milliseconds)                                                         |
| ("params","NX")      | No       | Empty string                                                                  |
| ("params","XX")      | No       | Empty string                                                                  |
| ("params","KEEPTTL") | No       | Empty string                                                                  |
| ("params","GET")     | No       | Empty string                                                                  |




### The response

Return value (when called by external library)


| Value | Description                |
|:------|:---------------------------|
| 2     | string in xider("ret",nnn) |
| 1     | string in xider("ret")     |
| 0     | Ok                         |
| -1    | no key provided            |
| -2    | no value provided          |
| -3    | NX, exists                 |
| -4    | XX, not exists             |
| -5    | GET, not exists            |
| -6    | Key too long               |
| -7    | Key is not a string        |


---

## GET

Get the value of key. If the key does not exist the special value nil is returned. An error is returned if the value stored at key is not a string, because GET only handles string values.

Compatible with [Redis  GET](https://redis.io/docs/latest/commands/get/)

### Syntax:

`set ret=$$GET^xider()`
`do GET^xider`


### `xider` array entries:

| Name                 | Required | Description |
|:---------------------|:---------|:------------|
| ("key")              | Yes      | String      |

### The response

| Value | Description                |
|:------|:---------------------------|
| 2     | string in xider("ret",nnn) |
| 1     | string in xider("ret")     |
| -1    | no key provided            |
| -2    | key too long               |
| -3    | key not found              |
| -4    | key is not a string        |

---


## DEL

Removes the specified keys. A key is ignored if it does not exist.

Compatible with [Redis  DEL](https://redis.io/docs/latest/commands/del/)

### Syntax:

`set ret=$$DEL^xider()`
`do SET^xider`


### `xider` array entries:

> The DEL command supports one or many keys to be deleted.

| Name         | Required | Description |
|:-------------|:---------|:------------|
| ("keys",nnn) | Yes      | String      |

> Note that the nnn counter starts at 1
 
### The response

| Value | Description                 |
|:------|:----------------------------|
| >-1   | number of deleted records   |
| -1    | no keys provided            |
| -2    | one of the keys is too long |

---

## EXISTS

Returns if key exists.

Compatible with [Redis  EXISTS](https://redis.io/docs/latest/commands/exists/)


### Syntax:

`set ret=$$EXISTS^xider()`
`do SET^xider`


### `xider` array entries:

The EXISTS command supports one or many keys to be checked.

| Name         | Required | Description |
|:-------------|:---------|:------------|
| ("keys",nnn) | Yes      | String      |

> Note that the nnn counter starts at 1

### The response

| Value | Description                 |
|:------|:----------------------------|
| >-1   | number of existing keys     |
| -1    | no keys provided            |
| -2    | one of the keys is too long |

---

### APPEND

If key already exists and is a string, this command appends the value at the end of the string. If key does not exist it is created and set as an empty string, so APPEND will be similar to SET in this special case.

Compatible with [Redis  APPEND](https://redis.io/docs/latest/commands/append/)

### Syntax:

`set ret=$$APPEND^xider()`
`do SET^xider`


### `xider` array entries:


| Name      | Required | Description |
|:----------|:---------|:------------|
| ("key")   | Yes      | String      |
| ("value") | Yes      | String      |


### The response

| Value | Description         |
|:------|:--------------------|
| >0    | new string length   |
| -1    | no key provided     |
| -2    | no value provided   |
| -3    | key too long        |
| -4    | key is not a string |

---

## STRLEN

Returns the length of the string value stored at key. An error is returned when key holds a non-string value.

Compatible with [Redis  STRLEN](https://redis.io/docs/latest/commands/strlen/)

### Syntax:

`set ret=$$STRLEN^xider()`
`do SET^xider`


### `xider` array entries:


| Name    | Required | Description |
|:--------|:---------|:------------|
| ("key") | Yes      | String      |


### The response

| Value | Description            |
|:------|:-----------------------|
| >0    | string length          |
| -1    | no key provided        |
| -2    | key too long           |
| -3    | key doesm't exists     |
| -4    | key is not a string    |

---

## GETRANGE

Returns the substring of the string value stored at key, determined by the offsets start and end (both are inclusive). Negative offsets can be used in order to provide an offset starting from the end of the string. So -1 means the last character, -2 the penultimate and so forth.

The function handles out of range requests by limiting the resulting range to the actual length of the string.

Compatible with [Redis  GETRANGE](https://redis.io/docs/latest/commands/getrange/)

### Syntax:

`set ret=$$GETRANGE^xider()`
`do SET^xider`


### `xider` array entries:


| Name      | Required | Description |
|:----------|:---------|:------------|
| ("key")   | Yes      | String      |
| ("start") | Yes      | Number      |
| ("end")   | Yes      | Number      |


### The response

| Value | Description                |
|:------|:---------------------------|
| 2     | string in xider("ret",nnn) |
| 1     | string in xider("ret")     |
| -1    | no key provided            |
| -2    | key too long               |
| -3    | no valid start             |
| -4    | no valid end               |
| -5    | key doesn't exists         |
| -6    | key is not a string        |

---

## SUBSTR

Returns the substring of the string value stored at key, determined by the offsets start and end (both are inclusive). Negative offsets can be used in order to provide an offset starting from the end of the string. So -1 means the last character, -2 the penultimate and so forth.

The function handles out of range requests by limiting the resulting range to the actual length of the string.


Compatible with [Redis  SUBSTR](https://redis.io/docs/latest/commands/substr/)

> As of Redis version 2.0.0, this command is regarded as deprecated.
> It can be replaced by GETRANGE when migrating or writing new code.

Syntax:

`SUBSTR^xider()`
`do SET^xider`


### `xider` array entries:


| Name      | Required | Description |
|:----------|:---------|:------------|
| ("key")   | Yes      | String      |
| ("start") | Yes      | Number      |
| ("end")   | Yes      | Number      |


### The response

| Value | Description                |
|:------|:---------------------------|
| 2     | string in xider("ret",nnn) |
| 1     | string in xider("ret")     |
| -1    | no key provided            |
| -2    | key too long               |
| -3    | no valid start             |
| -4    | no valid end               |
| -5    | key doesn't exists         |
| -6    | key is not a string        |

---

## SETRANGE

Overwrites part of the string stored at key, starting at the specified offset, for the entire length of value. If the offset is larger than the current length of the string at key, the string is padded with zero-bytes to make offset fit. Non-existing keys are considered as empty strings, so this command will make sure it holds a string large enough to be able to set value at offset.

Compatible with [Redis  SETRANGE](https://redis.io/docs/latest/commands/setrange/)

### Syntax:

`set ret=$$SETRANGE^xider()`
`do SET^xider`


### `xider` array entries:


| Name       | Required | Description |
|:-----------|:---------|:------------|
| ("key")    | Yes      | String      |
| ("offset") | Yes      | Number      |
| ("value")  | Yes      | String      |


### The response

| Value | Description            |
|:------|:-----------------------|
| >0    | new string length      |
| -1    | no key provided        |
| -2    | key too long           |
| -3    | no valid offset        |
| -4    | no value               |
| -5    | key is not a string    |

---

## RENAME

Renames key to newkey. It returns an error when key does not exist. If newkey already exists it is overwritten, when this happens RENAME executes an implicit DEL operation, so if the deleted key contains a very big value it may cause high latency even if RENAME itself is usually a constant-time operation.

Compatible with [Redis  RENAME](https://redis.io/docs/latest/commands/rename/)

### Syntax:

`set ret=$$RENAME^xider()`
`do SET^xider`


### `xider` array entries:


| Name       | Required | Description |
|:-----------|:---------|:------------|
| ("key")    | Yes      | String      |
| ("oldkey") | Yes      | String      |


### The response

| Value | Description         |
|:------|:--------------------|
| 0     | success             |
| -1    | no key provided     |
| -2    | key too long        |
| -3    | no newkey provided  |
| -4    | newkey too long     |
| -5    | key not found       |

---

## RENAMENX

Renames key to newkey if newkey does not yet exist. It returns an error when key does not exist.

Compatible with [Redis  RENAMENX](https://redis.io/docs/latest/commands/renamenx/)

### Syntax:

`set ret=$$RENAMENX^xider()`
`do SET^xider`


### `xider` array entries:


| Name       | Required | Description |
|:-----------|:---------|:------------|
| ("key")    | Yes      | String      |
| ("oldkey") | Yes      | String      |


### The response

| Value | Description           |
|:------|:----------------------|
| 0     | success               |
| -1    | no key provided       |
| -2    | key too long          |
| -3    | no newkey provided    |
| -4    | newkey too long       |
| -5    | key doesn't exists    |
| -6    | newkey already exists |

---

---

## GETDEL

Get the value of key and delete the key. This command is similar to GET, except for the fact that it also deletes the key on success (if and only if the key's value type is a string).

Compatible with [Redis  GETDEL](https://redis.io/docs/latest/commands/getdel/)


### Syntax:

`set ret=$$GETDEL^xider()`
`do SET^xider`


### `xider` array entries:


| Name    | Required | Description |
|:--------|:---------|:------------|
| ("key") | Yes      | String      |


### The response

| Value | Description         |
|:------|:--------------------|
| 0     | success             |
| -1    | no key provided     |
| -2    | key too long        |
| -3    | key not found       |

---

## MSET

Sets the given keys to their respective values. MSET replaces existing values with new values, just as regular SET. See MSETNX if you don't want to overwrite existing values.


Compatible with [Redis  MSET](https://redis.io/docs/latest/commands/mset/)

### Syntax:

`set ret=$$MSET^xider()`
`do SET^xider`


### `xider` array entries:


| Name           | Required | Description |
|:---------------|:---------|:------------|
| ("keys",nnn)   | Yes      | String      |
| ("values",nnn) | Yes      | String      |

> Note that the nnn counter starts at 1


### The response

| Value | Description          |
|:------|:---------------------|
| 0     | success              |
| -1    | no keys provided     |
| -2    | one key is too long  |
| -3    | one key has no value |

---

## MSETNX

Sets the given keys to their respective values. MSETNX will not perform any operation at all even if just a single key already exists.

Because of this semantic MSETNX can be used in order to set different keys representing different fields of a unique logic object in a way that ensures that either all the fields or none at all are set.

Compatible with [Redis  MSETNX](https://redis.io/docs/latest/commands/msetnx/)

### Syntax:

`set ret=$$MSETNX^xider()`
`do SET^xider`



### `xider` array entries:


| Name           | Required | Description |
|:---------------|:---------|:------------|
| ("keys",nnn)   | Yes      | String      |
| ("values",nnn) | Yes      | String      |

> Note that the nnn counter starts at 1


### The response

| Value | Description                     |
|:------|:--------------------------------|
| 0     | success                         |
| -1    | no keys provided                |
| -2    | one key is too long             |
| -3    | one key has no value            |
| -4    | at least one key already exists |

---

## MGET

Returns the values of all specified keys. For every key that does not hold a string value or does not exist, the special value nil is returned. Because of this, the operation never fails.

Compatible with [Redis  MGET](https://redis.io/docs/latest/commands/mget/)

### Syntax:

`set ret=$$MGET^xider()`
`do SET^xider`


### `xider` array entries:


| Name           | Required | Description |
|:---------------|:---------|:------------|
| ("keys",nnn)   | Yes      | String      |

> Note that the nnn counter starts at 1


### The response

| Value | Description                                                                                                          |
|:------|:---------------------------------------------------------------------------------------------------------------------|
| 0     | Success (data is in xider(ret,nnn) if key doesn't exists or is not a string, xider(ret,nnn) will be an empty string) |
| -1    | no keys provided                                                                                                     |
| -2    | one key is too long                                                                                                  |

---

## INCR

Increments the number stored at key by one. If the key does not exist, it is set to 0 before performing the operation. An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.

Compatible with [Redis  INCR](https://redis.io/docs/latest/commands/incr/)

### Syntax:

`set ret=$$INCR^xider()`
`do SET^xider`


### `xider` array entries:


| Name     | Required | Description |
|:---------|:---------|:------------|
| ("keys") | Yes      | String      |



### The response

| Value | Description            |
|:------|:-----------------------|
| 0     | success, new value is in xider("ret")|
| -1    | no key provided        |
| -2    | key too long           |
| -3    | value not an integer   |
| -4    | data-type not a string |

---

## INCRBY

Increments the number stored at key by increment. If the key does not exist, it is set to 0 before performing the operation. An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.

Compatible with [Redis  INCRBY](https://redis.io/docs/latest/commands/incrby/)

### Syntax:

`set ret=$$INCRBY^xider()`
`do SET^xider`


### `xider` array entries:


| Name          | Required | Description    |
|:--------------|:---------|:---------------|
| ("key")       | Yes      | String         |
| ("increment") | Yes      | signed integer |



### The response

| Value | Description         |
|:------|:--------------------|
| 0     | success, new value is in xider("ret")|
| -1    | no key provided    |
| -2    | key too long |
| -3    | no increment provided    |
| -4    | increment not an integer |
| -5    | value not an integer    |
| -6    | data-type not a string |

---

## INCRBYFLOAT

Increments the number stored at key by increment. If the key does not exist, it is set to 0 before performing the operation. An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.

Compatible with [Redis  INCRBYFLOAT](https://redis.io/docs/latest/commands/incrbyfloat/)

### Syntax:

`set ret=$$INCRBYFLOAT^xider()`
`do SET^xider`


### `xider` array entries:


| Name           | Required | Description            |
|:---------------|:---------|:-----------------------|
| ("key")        | Yes      |  String                |
| ("increment")  | Yes      | floating point integer |


### The response

| Value | Description         |
|:------|:--------------------|
| 0     | success, new value is in xider("ret")|
| -1    | no key provided    |
| -2    | key too long |
| -3    | no increment provided    |
| -4    | increment not a floating point |
| -5    | value not a floating point    |
| -6    | data-type not a string |

---

## DECR

Decrements the number stored at key by one. If the key does not exist, it is set to 0 before performing the operation. An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.

Compatible with [Redis  DECR](https://redis.io/docs/latest/commands/decr/)

### Syntax:

`set ret=DECR^xider()`
`do SET^xider`


### `xider` array entries:


| Name    | Required | Description |
|:--------|:---------|:------------|
| ("key") | Yes      | String      |



### The response

| Value | Description            |
|:------|:-----------------------|
| 0     | success, new value is in xider("ret")|
| -1    | no key provided        |
| -2    | key too long           |
| -3    | value not an integer   |
| -4    | data-type not a string |

---

## DECRBY

The DECRBY command reduces the value stored at the specified key by the specified decrement. If the key does not exist, it is initialized with a value of 0 before performing the operation. If the key's value is not of the correct type or cannot be represented as an integer, an error is returned.

Compatible with [Redis  DECRBY](https://redis.io/docs/latest/commands/decrby/)

### Syntax:

`set ret=$$DECRBY^xider()`
`do SET^xider`


### `xider` array entries:


| Name          | Required | Description    |
|:--------------|:---------|:---------------|
| ("key")       | Yes      | String         |
| ("decrement") | Yes      | signed integer |



### The response

| Value | Description              |
|:------|:-------------------------|
| 0     | success, new value is in xider("ret")|
| -1    | no key provided          |
| -2    | key too long             |
| -3    | no decrement provided    |
| -4    | decrement not an integer |
| -5    | value not an integer     |
| -6    | data-type not a string   |

---
