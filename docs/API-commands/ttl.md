<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

> This page is under review.
>
> The page is likely incorrect, contains invalid links, and/or needs technical review. In the future it may change substantially or be removed entirely.

---

# TTL commands

- [TTL](#ttl)
- [PTTL](#pttl)
- [EXPIRE](#expire)

---

## TTL

Returns the remaining time to live of a key that has a timeout. This introspection capability allows a client to check how many seconds a given key will continue to be part of the dataset.

Compatible with [Redis  TTL](https://redis.io/docs/latest/commands/ttl/)

### Syntax:

`set ret=$$TTL^xider()`
`do TTL^xider`


### `xider` array entries:


| Name         | Required | Description    |
|:-------------|:---------|:---------------|
| ("key"      | Yes      | String         |



### The response

| Value | Description              |
|:------|:-------------------------|
| >-1   | remaining time in seconds          |
| -1    | no key provided          |
| -2    | key too long             |
| -3    | key doesn't exists    |
| -4    | key has no TTL |

---

## PTTL

Like TTL this command returns the remaining time to live of a key that has an expire set, with the sole difference that TTL returns the amount of remaining time in seconds while PTTL returns it in milliseconds.

Compatible with [Redis  PTTL](https://redis.io/docs/latest/commands/pttl/)

### Syntax:

`set ret=PTTL^xider()`
`do PTTL^xider`


### `xider` array entries:


| Name         | Required | Description    |
|:-------------|:---------|:---------------|
| ("key"      | Yes      | String         |



### The response

| Value | Description              |
|:------|:-------------------------|
| >-1   | remaining time in seconds          |
| -1    | no key provided          |
| -2    | key too long             |
| -3    | key doesn't exists    |
| -4    | key has no TTL |

---

## EXPIRE

Set a timeout on key. After the timeout has expired, the key will automatically be deleted. A key with an associated timeout is often said to be volatile in Redis terminology.

Compatible with [Redis  EXPIRE](https://redis.io/docs/latest/commands/expire/)

### Syntax:

`set ret=EXPIRE^xider()`
`do EXPIRE^xider`


### `xider` array entries:


| Name           | Required | Description  |
|:---------------|:---------|:-------------|
| ("key")        | Yes      | String       |
| ("time")       | Yes      | Number       |
| ("params","NX) | No       | Empty string |
| ("params","XX) | No       | Empty string |
| ("params","GT) | No       | Empty string |
| ("params","LT) | No       | Empty string |



### The response

| Value | Description     |
|:------|:----------------|
| 0     | ok              |
| -1    | no key provided |
| -2    | key too long    |
| -3    | no valid time   |
| -4    | key not found   |
| -5    | XX, no TTL exists   |
| -6    | NX, exists   |
| -7    | GT less than TTL   |
| -8    | LT greater than TTL   |

---

