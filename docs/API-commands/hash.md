<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

> This page is under review.
>
> The page is likely incorrect, contains invalid links, and/or needs technical review. In the future it may change substantially or be removed entirely.

---

# \<hash> data type

- [HSET](#hset)
- [HSETNX](#hsetnx)
- [HGET](#hget)
- [HDEL](#hdel)
- [HLEN](#hlen)
- [HSTRLEN](#hstrlen)
- [HKEYS](#hkeys)
- [HVALS](#hvals)
- [HEXISTS](#hexists)
- [HMSET](#hmset)
- [HMGET](#hmget)
- [HGETALL](#hgetall)
- [HRANDFIELD](#hrandfield)
- [HINCRBY](#hincrby)
- [HINCRBYFLOAT](#hincrbyfloat)

> IMPORTANT: 
> The maximum key+field length is 975 characters
> This values may change. Use the value returned by the xider("ret","keyFieldMaxLength") after initialization.

---

## HSET

 Sets the specified fields to their respective values in the hash stored at key.

This command overwrites the values of specified fields that exist in the hash. If key doesn't exist, a new key holding a hash is created.

Compatible with [Redis  HSET](https://redis.io/docs/latest/commands/hset/)


### Syntax:

`set ret=$$HSET^xider()` 
`do HSET^xider`


### `xider` array entries:

| Name                  | Required | Description                                                                   |
|:----------------------|:---------|:------------------------------------------------------------------------------|
| ("key")               | Yes      | String )                                        |
| ("data",nnn,"field")  | Yes      | String )                                 |
| ("data",nnn,"value")  | Yes      | String (up to 1M)                                                             |
| OR                    |          |                                                                               |
| ("data",nnn,value",n) | Yes      | If string is > 1M, chunks of 1M in each n subscript (1 based). Value is EMPTY |




### The response

Return value (when called by external library)


| Value | Description                               |
|:------|:------------------------------------------|
|  >-1	| The number of fields that have been added |
| -1	| no key provided                           |
| -3	| no data                                   |
| -4	| some data has no field                    |
| -5	| some field / key combination too long   |
| -6	| some data has no value                    |
| -7	| key exists, but not hash data-type        |


---

## HSETNX

Sets field in the hash stored at key to value, only if field does not yet exist. 

If key does not exist, a new key holding a hash is created. If field already exists, this operation has no effect.

Compatible with [Redis  HSETNX](https://redis.io/docs/latest/commands/hsetnx/)


### Syntax:

`set ret=$$HSETNX^xider()`
`do HSETNX^xider`


### `xider` array entries:

| Name        | Required | Description                                                                   |
|:------------|:---------|:------------------------------------------------------------------------------|
| ("key")     | Yes      | String )                                        |
| ("field")   | Yes      | String )                                 |
| ("value")   | Yes      | String (up to 1M)                                                             |
| OR          |          |                                                                               |
| ("value",n) | Yes      | If string is > 1M, chunks of 1M in each n subscript (1 based). Value is EMPTY |




### The response

Return value (when called by external library)


| Value    | Description                         |
|:---------|:------------------------------------|
| 0	       | Success                             |
| -1	      | no key provided                     |
| -3	      | no field                            |
| -4	      | field / key combination too long                      |
| -5	      | no value                            |
| -6	      | key not found                       |
| -7	      | key exists, but not hash data-type  |
| -8	      | field already exists                |


---

## HGET

Returns the value associated with field in the hash stored at key.

Compatible with [Redis  HGET](https://redis.io/docs/latest/commands/hget/)


### Syntax:

`set ret=$$HGET^xider()`
`do HGET^xider`


### `xider` array entries:

| Name        | Required | Description                                                                   |
|:------------|:---------|:------------------------------------------------------------------------------|
| ("key")     | Yes      | String )                                        |
| ("field")   | Yes      | String )                                 |




### The response

Return value (when called by external library)


| Value | Description                        |
|:------|:-----------------------------------|
| 2	    | string in xider("ret",nnn)         |
| 1	    | string in xider("ret")             |
| -1	   | no key provided                    |
| -3	   | no field                           |
| -4	   | field / key combination too long                     |
| -5	   | key not found                      |
| -6	   | key exists, but not hash data-type |
| -7	   | field doesn't exists               |


---

## HDEL

Removes the specified fields from the hash stored at key. Specified fields that do not exist within this hash are ignored. Deletes the hash if no fields remain.

Compatible with [Redis  HDEL](https://redis.io/docs/latest/commands/hdel/)


### Syntax:

`set ret=$$HDEL^xider()`
`do HDEL^xider`


### `xider` array entries:

| Name           | Required | Description                                                                   |
|:---------------|:---------|:------------------------------------------------------------------------------|
| ("key")        | Yes      | String )                                        |
| ("fields",nnn) | Yes      | String )                                 |




### The response

Return value (when called by external library)


| Value | Description                                 |
|:------|:--------------------------------------------|
| >-1	  | The number of fields that have been deleted |
| -1	   | no key provided                             |
| -3	   | no fields                                   |
| -4	   | some field / key combination too long                         |
| -5	   | key not found                               |
| -6	   | key exists, but not hash data-type          |


---

## HLEN

Returns the number of fields contained in the hash stored at key.

Compatible with [Redis  HLEN](https://redis.io/docs/latest/commands/hlen/)


### Syntax:

`set ret=$$HLEN^xider()`
`do HLEN^xider`


### `xider` array entries:

| Name           | Required | Description                                                                   |
|:---------------|:---------|:------------------------------------------------------------------------------|
| ("key")        | Yes      | String )                                        |




### The response

Return value (when called by external library)


| Value | Description                        |
|:------|:-----------------------------------|
| >-0	  | The number of fields in the hash   |
| -1	   | no key provided                    |
| -2	   | key too long                       |
| -3	   | key not found                      |
| -4	   | key exists, but not hash data-type |


---

## HSTRLEN

Returns the number of fields contained in the hash stored at key.

Compatible with [Redis  HSTRLEN](https://redis.io/docs/latest/commands/hstrlen/)


### Syntax:

`set ret=$$HSTRLEN^xider()`
`do HSTRLEN^xider`



### `xider` array entries:

| Name           | Required | Description                                                                   |
|:---------------|:---------|:------------------------------------------------------------------------------|
| ("key")        | Yes      | String )                                        |
| ("field")   | Yes      | String )                                 |




### The response

Return value (when called by external library)


| Value | Description                        |
|:------|:-----------------------------------|
| >-0	  | the value length                   |
| -1	   | no key provided                    |
| -3	   | no field                          |
| -4	   | field / key combination too long                |
| -5	   | key not found                      |
| -6	   | key exists, but not hash data-type |
| -7	   | field doesn't exists |


---

## HKEYS

Returns all field names in the hash stored at key.

Compatible with [Redis  HKEYS](https://redis.io/docs/latest/commands/hkeys/)


### Syntax:

`set ret=HKEYS^xider()`
`do HKEYS^xider`



### `xider` array entries:

| Name           | Required | Description                                                                   |
|:---------------|:---------|:------------------------------------------------------------------------------|
| ("key")        | Yes      | String )                                        |




### The response

Return value (when called by external library)


| Value | Description                        |
|:------|:-----------------------------------|
| 0	    | success, fields are in xider("ret",nnn)|
| -1	   | no key provided                    |
| -2	   | key too long                       |
| -3	   | key not found                      |
| -4	   | key exists, but not hash data-type |


---

## HVALS

Returns all values in the hash stored at key.

Compatible with [Redis  HVALS](https://redis.io/docs/latest/commands/hvals/)


### Syntax:

`set ret=HVALS^xider()`
`do HVALS^xider`



### `xider` array entries:

| Name           | Required | Description                                                                   |
|:---------------|:---------|:------------------------------------------------------------------------------|
| ("key")        | Yes      | String )                                        |




### The response

Return value (when called by external library)


| Value | Description                             |
|:------|:----------------------------------------|
| 0	    | success, values are in xider("ret",nnn) |
| -1	   | no key provided                         |
| -2	   | key too long                            |
| -3	   | key not found                           |
| -4	   | key exists, but not hash data-type      |


---

## HEXISTS

Returns if field is an existing field in the hash stored at key.


Compatible with [Redis  HEXISTS](https://redis.io/docs/latest/commands/hexists/)


### Syntax:

`set ret=HEXISTS^xider()`
`do HEXISTS^xider`



### `xider` array entries:

| Name      | Required | Description                                                                   |
|:----------|:---------|:------------------------------------------------------------------------------|
| ("key")   | Yes      | String )                                        |
| ("field") | Yes      | String )                                        |




### The response

Return value (when called by external library)


| Value | Description                        |
|:------|:-----------------------------------|
| 0	    | success, field exists              |
| -1	   | no key provided                    |
|-3		|				no field|
| -4	|					field / key combination too long|
| -5	|					key not found|
| -6	|					key exists, but not hash data-type|
| -7	|					field doesn't exists|


---

## HMSET

As of Redis version 4.0.0, this command is regarded as deprecated.

It can be replaced by HSET with multiple field-value pairs when migrating or writing new code.

Compatible with [Redis  HMSET](https://redis.io/docs/latest/commands/hmset/)


### Syntax:

`set ret=$$HMSET^xider()`
`do HMSET^xider`


### `xider` array entries:

| Name                  | Required | Description                                                                   |
|:----------------------|:---------|:------------------------------------------------------------------------------|
| ("key")               | Yes      | String )                                        |
| ("data",nnn,"field")  | Yes      | String )                                 |
| ("data",nnn,"value")  | Yes      | String (up to 1M)                                                             |
| OR                    |          |                                                                               |
| ("data",nnn,value",n) | Yes      | If string is > 1M, chunks of 1M in each n subscript (1 based). Value is EMPTY |




### The response

Return value (when called by external library)


| Value | Description                               |
|:------|:------------------------------------------|
|  >-1	| The number of fields that have been added |
| -1	| no key provided                           |
| -3	| no data                                   |
| -4	| some data has no field                    |
| -5	| some field / key combination too long                    |
| -6	| some data has no value                    |
| -7	| key exists, but not hash data-type        |


---

## HMGET

Returns the values associated with the specified fields in the hash stored at key.

Compatible with [Redis  HMSET](https://redis.io/docs/latest/commands/hmget/)


### Syntax:

`set ret=$$HMGET^xider()`
`do HMGET^xider`


### `xider` array entries:

| Name                  | Required | Description                                                                   |
|:----------------------|:---------|:------------------------------------------------------------------------------|
| ("key")               | Yes      | String )                                        |
| ("fields",nnn)        | Yes      | String )                                 |




### The response

Return value (when called by external library)


| Value | Description                                            |
|:------|:-------------------------------------------------------|
| 0     | success, values are returned in xider("ret",nnn)=value |
| -1	 | no key provided                                        |
| -3	 | no fields                                              |
| -4	 | some field / key combination too long                                 |
| -5	 | key not found                                          |
| -6	 | key exists, but not hash data-type                     |


---

## HGETALL

Returns the values associated with the specified fields in the hash stored at key.

Compatible with [Redis  HGETALL](https://redis.io/docs/latest/commands/hgetall/)


### Syntax:

`set ret=$$HGETALL^xider()`
`do HGETALL^xider`


### `xider` array entries:

| Name                  | Required | Description                                                                   |
|:----------------------|:---------|:------------------------------------------------------------------------------|
| ("key")               | Yes      | String )                                        |




### The response

Return value (when called by external library)


| Value | Description                                       |
|:------|:--------------------------------------------------|
| 0     | success, values are returned in xider("ret",nnn)  |
| -1	   | no key provided                                   |
| -2	   | key too long                                      |
| -3	   | key not found                                     |
| -4	   | key exists, but not hash data-type                |


---

## HRANDFIELD

When called with just the key argument, return a random field from the hash value stored at key.

If the provided count argument is positive, return an array of distinct fields. The array's length is either count or the hash's
number of fields (HLEN), whichever is lower.

If called with a negative count, the behavior changes and the command is allowed to return the same field multiple times. In this
case, the number of returned fields is the absolute value of the specified count.

The optional WITHVALUES modifier changes the reply so it includes the respective values of the randomly selected hash fields.

Compatible with [Redis  HRANDFIELD](https://redis.io/docs/latest/commands/hrandfield/)


### Syntax:

`set ret=$$HRANDFIELD^xider()`
`do HRANDFIELD^xider`


### `xider` array entries:

| Name            | Required | Description                |
|:----------------|:---------|:---------------------------|
| ("key")         | Yes      | String                     |
| ("count")       | No       | (optional) signed integer  |
| ("withValues")  | No       | (optional) empty string    |




### The response

Return value (when called by external library)

The returned data structure changes depending on the request:

>If there is an error condition [0>xider("status")]:
> 
>xider("ret")=value

>If the call was successful [0=xider("status")]:
> 
>xider("ret",cnt,fld)="" OR value



| Value | Description                                   |
|:------|:----------------------------------------------|
| 0     | (data is in xider("ret",cnt,fld)="" OR value) |
| -1    | no key provided                               |
| -2    | key too long                                  |
| -3    | invalid count                                 |
| -4    | withValues needs count set                    |
| -5    | key not found                                 |
| -6    | key exists, but not hash data-type            |


---

## HINCRBY

Increments the number stored at field in the hash stored at key by increment. 

If key does not exist, a new key holding a hash is created. 

If field does not exist the value is set to 0 before the operation is performed.

Compatible with [Redis  HINCRBY](https://redis.io/docs/latest/commands/hincrby/)


### Syntax:

`set ret=$$HINCRBY^xider()`
`do HINCRBY^xider`


### `xider` array entries:

| Name          | Required | Description                            |
|:--------------|:---------|:---------------------------------------|
| ("key")       | Yes      | String ) |
| ("field")     | Yes      | String ) |
| ("increment") | 	 Yes    | 		 signed integer                      |




### The response

Return value (when called by external library)



| Value | Description              |
|:------|:-------------------------|
| 0     | success, new value is in xider("ret") |
| -1	   | no key provided          |
| -3	   | no field provided        |
| -4	   | field / key combination too long           |
| -5	   | no increment provider    |
| -6	   | increment not an integer |
| -7	   | data type not an hash    |
| -8	   | value not an integer     |


---

## HINCRBYFLOAT

Increment the specified field of a hash stored at key, and representing a floating point number, by the specified increment. 

If the increment value is negative, the result is to have the hash field value decremented instead of incremented. 

If the field does not exist, it is set to 0 before performing the operation. 

An error is returned if one of the following conditions occur:
- The key contains a value of the wrong type (not a hash).
- The current field content or the specified increment are not parsable as a double precision floating point number.

Compatible with [Redis  HINCRBYFLOAT](https://redis.io/docs/latest/commands/hincrbyfloat/)


### Syntax:

`set ret=$$HINCRBYFLOAT^xider()`
`do HINCRBYFLOAT^xider`


### `xider` array entries:

| Name          | Required | Description                            |
|:--------------|:---------|:---------------------------------------|
| ("key")       | Yes      | String  |
| ("field")     | Yes      | String  |
| ("increment") | 	 Yes    | 		 signed fp                           |




### The response

Return value (when called by external library)



| Value | Description                          |
|:------|:-------------------------------------|
| 0     | success, new value is in xider("ret")|
| -1	   | no key provided                      |
| -3	   | no field provided                    |
| -4	   | field / key combination too long                       |
| -5	   | no increment provider                |
| -6	   | increment not an fp                  |
| -7	   | data type not an hash                |
| -8	   | value not an fp                      |


---
