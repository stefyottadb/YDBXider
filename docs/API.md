<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

> This page is under review. 
> 
> The page is likely incorrect, contains invalid links, and/or needs technical review. In the future it may change substantially or be removed entirely.

---


# YottaDB Xider API

## Installation

The YottaDB Xider API allows you to access all the database functionality using an in-process interface.

In order to use the API, you must install the YottaDB Language plugin supporting your favorite programming language.

Additionally, (for the time being) you wi`ll need to add the following to your $zroutines:

`/YDBXIDER/objects*(/YDBXIDER/routines)`

This is needed to make sure the routines / object is/are loaded in your YottaDB image.

## Running the xider server

Running the xider server is optional, but highly advised.

If you do NOT run the xider server, you will miss the following functionality:

- Expiring tokens will not be deleted
- The journaling will not be switched and will grow. By deleting the files, you crasah the database
- Eventual dead sessions terminated by fatal error will still display as "alive" when querying the sessions using the appropriate commands

To run the server, execute:

`/YDBXIDER/commands/xider-server --api-only`

The `--api-only` switch will not run the Socket server, but only the background process to handle background processing.

The `--help` will display all the help.

## Life-cycle of the API

The life-cycle of the API is made up of 3 steps:

- Initialize the API
- Execute calls
- Terminate the API

The `initialization procedure` registers the application in the Xider database and initializes the needed variables.

The `terminate procedure` removes the application from the Xider database.

## Supported commands description

- [\<string> data type](./API-commands/string.md)
- [\<hash> data type](./API-commands/hash.md)
- [TTL](./API-commands/ttl.md)


## Initializing the API

In order to initialize the API, the application will need to execute one routine / function:

`do init^xider()`

`set ret=$$init^xider()`

You can, optionally, pass information about your driver and the host application, that it will be displayed in Xider using the management commands (to display the currently running sessions).



using the equivalent in your Plugin to execute routines / function.

> The function will always return an empty string


#### (optional) initialization PARAMETERS

The following parameters can be passed (all optional, if not passed, the field will be filled with N/A):

- `xider("driverName")`
- `xider("driverVersion")`
- `xider("description")`
- `xider("noParamsValidation")`

`description`

A description of the application.

---

`driverName`

The driver name used to connect.

---

`driverVersion`

The driver version used to connect.

---

`xider("noParamsValidation")`

Set to 0 to apply params validation

---

Once the function / method got executed, server information will be returned inside the `xider` array:

- `xider("ret","keyFieldMaxLength")` The maximum length for a key or a key/field combination.
- `xider("ret","version")` The API version


## Terminating the API

Before terminating the application process, we recommend that it terminate the API thus: (list function, add for each supported language)

`do terminate^xider`

`set ret=$$terminate^xider`

using the equivalent in your Plugin to execute routines / functions.

> The function will always return an empty string

If it doesn't get called, then the session is visible in the session list and will be removed when the Helper process will detect that the PID doesn't exists anymore.



## Passing and retrieving data to the commands functions

In order to pass the data (parameters) to each function, we use an array called `xider`.

The reason why we use this array instead of function parameters are the following:

- We may have to pass strings longer than 1M for values
- We may have to return strings longer than 1M

For each command, we describe the full parameters and return structures needed to execute the function.

> The function gets execute in three steps:
>
> - You populate the `xider` array with the needed subscripts
> - You either: 
>   - call the function $$SET^xider and get the status  as function-value
>   - call the procedure and get the status back into xider("status")
> - Depending on the status, you may have to get your data from xider("ret")

## Important notes about the maximum length of the key / field fields

The maximum length for either a `key` or a `key/field` combination, is the value returned by the init() function:

`xider("ret","keyFieldMaxLength")`

and it is around 900 character.

