<!--
/****************************************************************
 *                                                              *
 * Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.      *
 * All rights reserved.                                         *
 *                                                              *
 * This source code contains the intellectual property          *
 * of its copyright holder(s), and is made available            *
 * under a license.  If you do not know the terms of            *
 * the license, please stop and do not read further.            *
 *                                                              *
 ****************************************************************/
-->

> This page is under review. The page is likely incorrect, contains invalid links, and/or needs technical review. In the future it may change substantially or be removed entirely.

---


# Database structure

The Xider database is stored in several globals:

`^%ydbxider`
`^%ydbxiderK`
`^%ydbxiderKDT`
`^%ydbxiderKT`


---
### Keys 

All the keys, for all data-types, are stored into:

`^%ydbxiderK`


#### Key location

The key is stored in the next subscript:

`^%ydbxiderK(key)`

Its content meaning changes, depending on the data-type.

#### Key data-type

The data-type related to each key is stored into:

`^%ydbxiderKDT(key)=data-type`

At the moment, the only valid values are:

- "string"
- "hash"


---
### \<string> data type

#### Key and values location

The \<string> data-type values are stored directly after the key:
`^%ydbxiderK(key)`

> If the value length is < 1Mib, then the node value is the key value.

>If the value length is > 1 Mib, then:
>
>the node value is empty
>
>and
>
>chunks of 1 Mib are stored under the numerical incremental subscript:

Example:

`^%ydbxiderK(key,1)="first 1Mib chunk`

`^%ydbxiderK(key,2)="second 1Mib chunk`

`^%ydbxiderK(key,3)="second 1Mib chunk`

#### TTL

While the TTL requires a registration in the `^%ydbxider("TTL")`, a node gets created under the key to quickly identify a key with TTL:

`^%ydbxiderKT(key)=unix_time`



---

### \<hash> data type 

The hash is located inside the `^%ydbxiderK(key)` global.

Its value is the number of fields in the hash.

The fields are subscripts to the key:

`^%ydbxiderK(key,fieldName)=value`


---
### Sessions

The sessions are stored into:
`^%ydbxider("S")`

The current open session counter is:
`^%ydbxider("S")`

The API automatically increase and descrease the counter.

This node gets reset at startup.


### TTL

The TTL is handled as a subscript node in the `^%ydbxider` global: `^%ydbxider("TTL")`

It has two subscripts, `DATA` and `IX`.

The `DATA` node has the following subscripts:

`^%ydbxider("TTL","DATA",TTL,cnt)=nodeName`

Where:

- TTL is the TTL value in Unix time
- cnt is a counter, to avoid having two entries with the same TTL overwriting each other
- nodeName is the $name representation of the target node in the keys global

The `IX` node has the following subscripts:

`^%ydbxider("TTL","IX",nodeName)=TTL^cnt`

Where:

- nodeName is the $name representation of the target node in the keys global
- TTL^cnt is a 2 piece string as:
  - TTL is the TTL value in Unix time
  - cnt is a counter, to avoid having two entries with the same TTL overwriting each other


---

