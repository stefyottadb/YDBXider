#!/bin/bash
#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#    This source code contains the intellectual property	      #
#    of its copyright holder(s), and is made available	        #
#    under a license.  If you do not know the terms of	        #
#    the license, please stop and do not read further.	        #
#                                                               #
#################################################################

source /YDBXIDER/dev
export ydb_tls_passwd_ydbxider="$(echo ydbxider | /opt/yottadb/current/plugin/gtmcrypt/maskpass | cut -d ":" -f2 | tr -d '[:space:]')"

echo "Host name is: "$HOSTNAME

if [ "$1" = "test" ]; then
	echo "Running tests..."

	# for storing artifacts
	mkdir -p /mylogs

	if [ "$3" = "node-speed" ]; then
		redis-server &
		xider --test-mode |& tee /mylogs/xider-logs.txt &
		exec npm test -- test/speedTest
	elif [ "$3" = "node" ]; then
		redis-server &
		xider --test-mode |& tee /mylogs/xider-logs.txt &
		exec npm test -- test/node.js
	elif [ "$3" = "mut" ]; then
		exec test/mut.sh
	fi
else
  redis-server &
  yottadb -run start^%ydbgui --port 8089  --readwrite
fi
