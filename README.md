
> This page is a work in progress. The page is likely incorrect, contains invalid links, and/or needs technical review. In the future it may change substantially or be removed entirely.

---

# YottaDB Xider

YottaDB Xider software implements the functionality of the Redis database and extra features. It is built on the extremely fast and proven [YottaDB](https://yottadb.com/) database system. The software is licensed under [GNU Affero GPL v3](LICENSE).

Xider's core is an M-language implementation of a Redis server that may be run in one or both of two configurations:

* As a RESP protocol server like Redis, in which case standard Redis RESP clients and Redis language drivers may run against it, including `redis-cli` and the various Redis clients for specific languages (Python, C#, Node.js, Java, Go).
* Or, on the same machine, may be invoked directly by a client application using extremely fast in-process calls, by using Xider in-process API or the supplied language drivers for `node.js` and `Python` (others still to be written).

Here you will find notes on installing Xider. Documentation on each language driver is in the individual driver's directory (e.g. [drivers/python](drivers/python/README.md)).

---

## Installing Xider

To install, create a build sub-directory in the source directory, then in the build directory, run cmake .., make and sudo make install. For example:

```
cd /tmp/
git clone --depth 1 https://gitlab.com/YottaDB/DB/YDBXider.git
cd YDBXider
mkdir build && cd build
cmake .. && make && sudo make install
```

---

## Running Xider
To launch the server, run the following:

```
. /usr/local/etc/ydb_env_set
xider
```

Command switches:

| Name                   | Description                                              |
|:-----------------------|:---------------------------------------------------------|
| `--api-only`           | Disable the Socket server and allow in-process API only |
| `--help`               | Display this text"                                       |
| `--logging {level}`    | Select out of: debug, verbose, notice, warning, nothing |
| `--no-journal-switch`  | Disable the automatic journal switching                 |
| `--no-ttl`             | Disable the expiring of entries (TTL)                    |
| `--port {nnn}`         | Changes the default socket number (3000)                |
| `--reset-db`           | Re-initialize the database                              |
| `--ttl-pool-time {ms}` | The frequency of the TTL checking in milliseconds       |
| `--version (-v)`       | Display the software version                             |

---

### Building and using the docker image

To build the image, from the project root:

`docker image build --progress=plain -t ydbxider .`

To run the image:

`docker run -d --init --name=ydbxider ydbxider`

To shell into the container:

`docker exec -it ydbxider bash`

### Running the development environment 

You can enter development mode by launching the following command (from the project root):

`commands/xider-dev`

It will use the docker compose to build and run the docker image, with all the development directories automatically mounted.

In the file: `compose.yml` you can see which directory gets mounted. 

Once the docker is running, any change in the source code will be reflected in the docker image.

> You can use this feature to develop your own code by mounting new directories in the file `compose.yml`.

---

### Architecture

![arch imnage](img/xider-arch.png)


## API

The Xider has a development API.

Detailed information can be found [here](docs/API.md)

