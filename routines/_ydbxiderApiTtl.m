;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
;
; -----------------------------------------------
; This is the <TTL> section.
; -----------------------------------------------
;
; DATABASE INFO
; The <string> data type is stored in:
; ^%ydbxider("KEYS")
; The TTL is stored in:
; ^%ydbxider("TTL","DATA")
; ^%ydbxider("TTL","IX")
;
;
; ***************************************
; TTL
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
;  >-1				remaining time in seconds
; -1				no key provided
; -2				key is too long
; -3				key doesn't exists
; -4				key has no TTL
; ***************************************
TTL
	new ret,socketResp,ttl
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","TTL"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("TTL received")
	;
	; verify params
	if '$zlength($get(xider("key"))) do  goto TTLquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto TTLquit
	;
	if '$data(^%ydbxiderK(xider("key"))) do  goto TTLquit
	. if socketResp set xider("ret")=":-2"_CRLF
	. else  set ret=-3
	;
	if '$data(^%ydbxiderKT(xider("key"))) do  goto TTLquit
	. if socketResp set xider("ret")=":-1"_CRLF
	. else  set ret=-4
	;
	set ttl=(^%ydbxiderKT(xider("key"))-$zut)\1E6
	if socketResp set xider("ret")=":"_ttl_CRLF
	else  set ret=ttl
	;
TTLquit
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; PTTL
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
;  >-1				remaining time in milliseconds
; -1				no key provided
; -2				key is too long
; -3				key doesn't exists
; -4				key has no TTL
; ***************************************
PTTL
	new ret,socketResp,ttl
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","PTTL"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("PTTL received")
	;
	; verify params
	if '$zlength($get(xider("key"))) do  goto PTTLquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto PTTLquit
	;
	if '$data(^%ydbxiderK(xider("key"))) do  goto PTTLquit
	. if socketResp set xider("ret")=":-2"_CRLF
	. else  set ret=-3
	;
	if '$data(^%ydbxiderKT(xider("key"))) do  goto PTTLquit
	. if socketResp set xider("ret")=":-1"_CRLF
	. else  set ret=-4
	;
	set ttl=(^%ydbxiderKT(xider("key"))-$zut)\1E3
	if socketResp set xider("ret")=":"_ttl_CRLF
	else  set ret=ttl
	;
PTTLquit
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; EXPIRE
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
; ("time")			Number of seconds to wait until key will expire
; ("params","NX")		Empty string
; ("params","XX")		Empty string
; ("params","GT")		Empty string
; ("params","LT")		Empty string
;
; Return (when called by external library)
;  0				Ok
; -1				no key provided
; -2				Key too long
; -3				No valid time
; -4				key not found
; -5				XX, no TTL exists
; -6				NX, exists
; -7				GT less than TTL
; -8				LT greater than TTL
; ***************************************
EXPIRE
	new exists,ret,socketResp,exit
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","EXPIRE"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("EXPIRE received")
	;
	; verify params
	if '$zlength($get(xider("key"))) do  goto EXPIREquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto EXPIREquit
	;
	if '+$get(xider("time")) do  goto EXPIREquit
	. if socketResp set xider("ret")="-no valid time"_CRLF
	. else  set ret=-3
	;
	if '$data(^%ydbxiderK(xider("key"))) do  goto EXPIREquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-4
	;
	set exists=$data(^%ydbxiderKT(xider("key"))) ; used to flag pre-existence of a key TTL
	;
	if $get(xider("params","XX")),'exists do  goto EXPIREquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-5
	;
	if $get(xider("params","NX")),exists do  goto EXPIREquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-6
	;
	if $get(xider("params","GT")),exists,^%ydbxiderKT(xider("key"))<=($zut+(xider("time")*1E6)) do  goto EXPIREquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-7
	;
	if $get(xider("params","LT")),exists,^%ydbxiderKT(xider("key"))>=($zut+(xider("time")*1E6)) do  goto EXPIREquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-8
	;
	set ^%ydbxiderKT(xider("key"))=$zut+(xider("time")*1E6)
	if socketResp set xider("ret")=":1"_CRLF
	else  set ret=0
	;
EXPIREquit
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
EXPIRETIME
	;
	quit
	;
	;
EXPIREAT
	;
	quit
	;
	;
PEXPIRE
	;
	quit
	;
	;
PEXPIREAT
	;
	quit
	;
	;
