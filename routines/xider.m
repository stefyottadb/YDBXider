;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; This is the data layer external references entry points.
;
; All the labels listed here can be called from outside
; They are all listed here, as it is easier for the caller to use the same routine name for all commands.
; The code is here dispatching the call to the <data type> it belongs, in order to keep the code clean
;
; *****************************************1******
; Initialization entry point
; ***********************************************
init(socketMode) goto init^%ydbxiderAPI
;
; *****************************************1******
; Termination entry point
; ***********************************************
terminate() goto terminate^%ydbxiderAPI
;
; *****************************************1******
; Single entry point for all commands -- for simpler call-in file
; ***********************************************
call(cmd) quit $$@cmd
;
; *****************************************1******
; <COMMAND> namespace
; ***********************************************
COMMAND() goto COMMAND^%ydbxiderApiCommand
;
; *****************************************1******
; <STRING> data type entry points
; ***********************************************
SET() goto SET^%ydbxiderApiString
SETcontinue() goto SETcontinue^%ydbxiderApiString
;
GET() goto GET^%ydbxiderApiString
GETcontinue() goto GETcontinue^%ydbxiderApiString
;
DEL() goto DEL^%ydbxiderApiString
DELcontinue() goto DELcontinue^%ydbxiderApiString
;
EXISTS() goto EXISTS^%ydbxiderApiString
EXISTScontinue() goto EXISTScontinue^%ydbxiderApiString
;
APPEND() goto APPEND^%ydbxiderApiString
APPENDcontinue() goto APPENDcontinue^%ydbxiderApiString
;
STRLEN() goto STRLEN^%ydbxiderApiString
STRLENcontinue() goto STRLENcontinue^%ydbxiderApiString
;
SUBSTR() goto GETRANGE^%ydbxiderApiString
SUBSTRcontinue() goto GETRANGEcontinue^%ydbxiderApiString
;
GETRANGE() goto GETRANGE^%ydbxiderApiString
GETRANGEcontinue() goto GETRANGEcontinue^%ydbxiderApiString
;
SETRANGE() goto SETRANGE^%ydbxiderApiString
SETRANGEcontinue() goto SETRANGEcontinue^%ydbxiderApiString
;
RENAME() goto RENAME^%ydbxiderApiString
RENAMEcontinue() goto RENAMEcontinue^%ydbxiderApiString
;
RENAMENX() goto RENAMENX^%ydbxiderApiString
RENAMENXcontinue() goto RENAMENXcontinue^%ydbxiderApiString
;
GETDEL() goto GETDEL^%ydbxiderApiString
GETDELcontinue() goto GETDELcontinue^%ydbxiderApiString
;
MSET() goto MSET^%ydbxiderApiString
MSETcontinue() goto MSETcontinue^%ydbxiderApiString
;
MSETNX() goto MSETNX^%ydbxiderApiString
MSETNXcontinue() goto MSETNXcontinue^%ydbxiderApiString
;
MGET() goto MGET^%ydbxiderApiString
MGETcontinue() goto MGETcontinue^%ydbxiderApiString
;
INCR() goto INCR^%ydbxiderApiString
INCRcontinue() goto INCRcontinue^%ydbxiderApiString
;
INCRBY() goto INCRBY^%ydbxiderApiString
INCRBYcontinue() goto INCRBYcontinue^%ydbxiderApiString
;
INCRBYFLOAT() goto INCRBYFLOAT^%ydbxiderApiString
INCRBYFLOATcontinue() goto INCRBYFLOATcontinue^%ydbxiderApiString
;
DECR() goto DECR^%ydbxiderApiString
DECRcontinue() goto DECRcontinue^%ydbxiderApiString
;
DECRBY() goto DECRBY^%ydbxiderApiString
DECRBYcontinue() goto DECRBYcontinue^%ydbxiderApiString
;
; *****************************************1******
; <TTL> namespace
; ***********************************************
EXPIRE() goto EXPIRE^%ydbxiderApiTtl
EXPIREcontinue() goto EXPIREcontinue^%ydbxiderApiTtl
;
EXPIREAT() goto EXPIREAT^%ydbxiderApiTtl
EXPIREATcontinue() goto EXPIREATcontinue^%ydbxiderApiTtl
;
EXPIRETIME() goto EXPIRETIME^%ydbxiderApiTtl
EXPIRETIMEcontinue() goto EXPIRETIMEcontinue^%ydbxiderApiTtl
;
PEXPIRE() goto PEXPIRE^%ydbxiderApiTtl
PEXPIREcontinue() goto PEXPIREcontinue^%ydbxiderApiTtl
;
PEXPIREAT() goto PEXPIREAT^%ydbxiderApiTtl
PEXPIREATcontinue() goto PEXPIREATcontinue^%ydbxiderApiTtl
;
PTTL() goto PTTL^%ydbxiderApiTtl
PTTLcontinue() goto PTTLcontinue^%ydbxiderApiTtl
;
TTL() goto TTL^%ydbxiderApiTtl ;
TTLcontinue() goto TTLcontinue^%ydbxiderApiTtl ;
;
; *****************************************1******
; <HASH> data type entry points
; ***********************************************
HSET() goto HSET^%ydbxiderApiHash
HSETcontinue() goto HSETcontinue^%ydbxiderApiHash
;
HSETNX() goto HSETNX^%ydbxiderApiHash
HSETNXcontinue() goto HSETNXcontinue^%ydbxiderApiHash
;
HGET() goto HGET^%ydbxiderApiHash
HGETcontinue() goto HGETcontinue^%ydbxiderApiHash
;
HDEL() goto HDEL^%ydbxiderApiHash
HDELcontinue() goto HDELcontinue^%ydbxiderApiHash
;
HLEN() goto HLEN^%ydbxiderApiHash
HLENcontinue() goto HLENcontinue^%ydbxiderApiHash
;
HVALS() goto HVALS^%ydbxiderApiHash
HVALScontinue() goto HVALScontinue^%ydbxiderApiHash
;
HSTRLEN() goto HSTRLEN^%ydbxiderApiHash
HSTRLENcontinue() goto HSTRLENcontinue^%ydbxiderApiHash
;
HSCAN() goto HSCAN^%ydbxiderApiHash
HSCANcontinue() goto HSCANcontinue^%ydbxiderApiHash
;
HRANDFIELD() goto HRANDFIELD^%ydbxiderApiHash
HRANDFIELDcontinue() goto HRANDFIELDcontinue^%ydbxiderApiHash
;
HGETALL() goto HGETALL^%ydbxiderApiHash
HGETALLcontinue() goto HGETALLcontinue^%ydbxiderApiHash
;
HMSET() goto HSET^%ydbxiderApiHash
HMSETcontinue() goto HSETcontinue^%ydbxiderApiHash
;
HMGET() goto HMGET^%ydbxiderApiHash
HMGETcontinue() goto HMGETcontinue^%ydbxiderApiHash
;
HPTTL() goto HPTTL^%ydbxiderApiHash
HPTTLcontinue() goto HPTTLcontinue^%ydbxiderApiHash
;
HTTL() goto HTTL^%ydbxiderApiHash
HTTLcontinue() goto HTTLcontinue^%ydbxiderApiHash
;
HKEYS() goto HKEYS^%ydbxiderApiHash
HKEYScontinue() goto HKEYScontinue^%ydbxiderApiHash
;
HPEXPIRETIME() goto HPEXPIRETIME^%ydbxiderApiHash
HPEXPIRETIMEcontinue() goto HPEXPIRETIMEcontinue^%ydbxiderApiHash
;
HPEXPIREAT() goto HPEXPIREAT^%ydbxiderApiHash
HPEXPIREATcontinue() goto HPEXPIREATcontinue^%ydbxiderApiHash
;
HPEXPIRE() goto HPEXPIRE^%ydbxiderApiHash
HPEXPIREcontinue() goto HPEXPIREcontinue^%ydbxiderApiHash
;
HPERSIST() goto HPERSIST^%ydbxiderApiHash
HPERSISTcontinue() goto HPERSISTcontinue^%ydbxiderApiHash
;
HINCRBYFLOAT() goto HINCRBYFLOAT^%ydbxiderApiHash
HINCRBYFLOATcontinue() goto HINCRBYFLOATcontinue^%ydbxiderApiHash
;
HINCRBY() goto HINCRBY^%ydbxiderApiHash
HINCRBYcontinue() goto HINCRBYcontinue^%ydbxiderApiHash
;
HEXPIRETIME() goto HEXPIRETIME^%ydbxiderApiHash
HEXPIRETIMEcontinue() goto HEXPIRETIMEcontinue^%ydbxiderApiHash
;
HEXPIREAT() goto HEXPIREAT^%ydbxiderApiHash
HEXPIREATcontinue() goto HEXPIREATcontinue^%ydbxiderApiHash
;
HEXPIRE() goto HEXPIRE^%ydbxiderApiHash
HEXPIREcontinue() goto HEXPIREcontinue^%ydbxiderApiHash
;
HEXISTS() goto HEXISTS^%ydbxiderApiHash
HEXISTScontinue() goto HEXISTScontinue^%ydbxiderApiHash
;
; *****************************************1******
; <Transaction> namespace
; ***********************************************
WATCH() goto WATCH^%ydbxiderApiTransactions
;
UNWATCH() goto UNWATCH^%ydbxiderApiTransactions
;
MULTI() goto MULTI^%ydbxiderApiTransactions
;
EXEC() goto EXEC^%ydbxiderApiTransactions
;
DISCARD() goto DISCARD^%ydbxiderApiTransactions
;
;
