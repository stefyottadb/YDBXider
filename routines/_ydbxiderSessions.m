;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; -------------------------------------------
; add(params)
;
; Creates a new entry
;
; params("type")                    "A" (API) "S" (socket) or "H" (helper process)
; params("pid")						OPTIONAL (used only by the helper)
; params("driverName")              OPTIONAL
; params("driverVersion")           OPTIONAL
; params("ipNumber")                OPTIONAL
; params("description")             OPTIONAL
; -------------------------------------------
add(params)
	new pid
	;
	set pid=$get(params("pid"),$job)
	;
	set ^%ydbxider("S",pid,"connectTime")=$ZUT
	set ^%ydbxider("S",pid,"type")=params("type")
	set ^%ydbxider("S",pid,"commandsCount")=0
	set ^%ydbxider("S",pid,"driverName")=$select($zlength($get(params("driverName"))):params("driverName"),1:"N/A")
	set ^%ydbxider("S",pid,"driverVersion")=$select($zlength($get(params("driverVersion"))):params("driverVersion"),1:"N/A")
	set ^%ydbxider("S",pid,"ipNumber")=$select($zlength($get(params("ipNumber"))):params("ipNumber"),1:"N/A")
	set ^%ydbxider("S",pid,"description")=$select($zlength($get(params("description"))):params("description"),1:"N/A")
	;
	; increment the counter if type '= "H"
	set:^%ydbxider("S",pid,"type")'="H" ret=$increment(^%ydbxider("S"))
	;
	quit
	;
	;
; -------------------------------------------
; edit(params)
;
; params("driverName")              OPTIONAL
; params("driverVersion")           OPTIONAL
; params("ipNumber")                OPTIONAL
; params("description")             OPTIONAL
; -------------------------------------------
edit(params)
	;
	set ^%ydbxider("S",$job,"driverName")=$select($zlength($get(params("driverName"))):params("driverName"),1:"N/A")
	set ^%ydbxider("S",$job,"driverVersion")=$select($zlength($get(params("driverVersion"))):params("driverVersion"),1:"N/A")
	set ^%ydbxider("S",$job,"ipNumber")=$select($zlength($get(params("ipNumber"))):params("ipNumber"),1:"N/A")
	set ^%ydbxider("S",$job,"description")=$select($zlength($get(params("description"))):params("description"),1:"N/A")
	;
	quit
	;
	;
; -------------------------------------------
; delete()
;
; Delete the entry for the xcurrent PID
; -------------------------------------------
delete()
	new ret
	;
	set ret=$increment(^%ydbxider("S"),-1)
	kill ^%ydbxider("S",$job)
	;
	quit
	;
	;
; -------------------------------------------
; initialize()
;
; initialize the SESSIONS node
; -------------------------------------------
initialize()
	;
	kill ^%ydbxider("S")
	set ^%ydbxider("S")=0
	;
	quit
	;
	;
