;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; ***************************************
; helper process
; ***************************************
start()
	new journalTicks,sessionTicks
	;
	new $etrap
	set $etrap="goto mainErrorHandler^%ydbxiderHelper"
	;
	hang .5
	do:%ydbxiderParams("logging")>=logNOTICE&'%ydbxiderParams("testMode") log^%ydbxiderLogger("Helper program started with pid "_$job_". TTL processing poll time: "_%ydbxiderParams("ttlPoolTime"))
	;
	; set $ZTIMEOUT
	set (sessionTicks,journalTicks)=0
	set $ztimeout="1:do timerElapse^%ydbxiderHelper"
	;
poolTtl
	; ---------------------------------------
	; TTL timer
	; ---------------------------------------
	hang %ydbxiderParams("ttlPoolTime")/1000
	do process^%ydbxiderTtl
	;
	goto poolTtl
	;
	;
timerElapse
	set journalTicks=journalTicks+1
	set sessionTicks=sessionTicks+1
	;
	do:journalTicks>%ydbxiderParams("journalSwitchPoolTime")
	. set journalTicks=0
	. do processJournal
	;
	do:sessionTicks>%ydbxiderParams("validateSessionPoolTime")
	. set sessionTicks=0
	. do processSession
	; reset interval
	set $ztimeout="1:do timerElapse^%ydbxiderHelper"
	;
	quit
	;
	;
processJournal
	do:%ydbxiderParams("logging")>=logVERBOSE&'%ydbxiderParams("testMode") log^%ydbxiderLogger("Checking journal file...")
	;
	;
	;
	quit
	;
	;
processSession
	do:%ydbxiderParams("logging")>=logVERBOSE&'%ydbxiderParams("testMode") log^%ydbxiderLogger("Checking processes...")
	;
	quit
	;
	;
mainErrorHandler
	use zpout
	;
	write !!,"**********************************"
	write !,"*** An internal error occurred ***"
	write !,"**********************************",!
	write !,"Location",?19,$zpiece($zs,",",2)
	write !,"Error code",?19,$zpiece($zs,",",1)
	write !,"Mnemonic",?19,$zpiece($zs,",",3)
	; the description in $zstatus can contain many commas, so just find where we left off and extract to the max $zstatus length
	write !,"Description",?18,$zextract($zstatus,$zfind($zstatus,$zpiece($zstatus,",",3))+1,2048)
	write !
	;
	set dsm1=$stack(-1)
	write !,"$stack(-1):",dsm1
	for l=dsm1:-1:0 do
	. write !,l
	. for i="ecode","place","mcode" write ?5,i,?15,$stack(l,i),!
	;
	do:$ZSYSLOG("Fatal: "_$zstatus)
	. ; do logging
	. do:%ydbxiderParams("logging")>=logWARNING&'%ydbxiderParams("testMode") log^%ydbxiderLogger("ERROR: Helper program unexpectely terminated due to an internal error")
	. ; halt the process
	. zhalt 5
	;
	;
