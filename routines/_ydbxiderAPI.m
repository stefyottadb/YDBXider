;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
;
init ; This is the initialization of the data layer
	new params
	;
	; The following variables are those of the caller
	set xiderSTRINGMAXKEYLENGTH=925
	set UPA="^"
	set oneMib=1048576
	;
	set socketMode=$get(socketMode,0)
	set driverName=$get(xider("driverName"),"N/A")
	set driverVersion=$get(xider("driverVersion"),"N/A")
	set description=$get(xider("description"),"N/A")
	set noParamsValidation=$get(xider("noParamsValidation"),0)
	set wrongDataType="-WRONGTYPE Operation against a key holding the wrong kind of value"
	set valueNotAnInteger="-ERR value is not an integer or out of range"
	;
	set %ydbxiderParams("sessionStatistics")=0
	;
	; ****************************************
	; Prepare log constants
	; ****************************************
	;these variables gets new'd by the caller
	set logWARNING=1,logNOTICE=2,logVERBOSE=3,logDEBUG=4
	;
	; ----------------------
	; create a new session node if not in socket mode
	; ----------------------
	do:'socketMode
	. set params("type")="A"
	. set params("description")=description
	. set params("driverName")=driverName
	. set params("driverVersion")=driverVersion
	. ;
	. do add^%ydbxiderSessions(.params)
	;
	set xider("keyFieldMaxLength")=xiderSTRINGMAXKEYLENGTH
	set xider("version")=$$getVersionNumber^%ydbxiderServer
	;
	quit:$quit ""  quit
	;
	;
terminate
	do delete^%ydbxiderSessions
	;
	quit:$quit ""  quit
	;
	;
verifyKeyLength(key)
	if xiderSTRINGMAXKEYLENGTH<$zlength($get(key,xider("key"))) set xider("ret")="-"_$select($zlength($get(key)):"new",1:"")_"key too long"_socketResp quit 0
	;
	quit 1
	;
	;
verifyKeysLength()
	new key,ret
	set key="",ret=1
	;
	for  set key=$order(xider("keys",key)) quit:'$zlength(key)  set:xiderSTRINGMAXKEYLENGTH<$zlength(xider("keys",key)) ret=0 quit:'ret
	;
	set:'ret xider("ret")="-key: "_key_" too long"_socketResp
	;
	quit ret
	;
	;
isInteger(x)
	quit $zlength(x)&($char(0)]]x)&'(x[".")
	;
	;
isFloat(x)
	quit $zlength(x)&($char(0)]]x)
	;
	;
