;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; This routine process the command line switches and the configuration file
;
	; SUPPORTED PARAMETERS:
	; --port nnn
	; --api-only
	; --logging level
	; --help
	; --reset-db
	; --no-ttl
	; --ttl-pool-time
	; --no-journal-switch
	; --test-mode
	;
	; TO IMPLEMENT (including file validation and parse)
	; /path/to/ydb-redis.conf
	; --log-file /path/to/file
	;
parse(params)
	new paramsA,param,ix,ret,debugMode,found
	;
	; sanitize the string and split it
	set *paramsA=$$SPLIT^%MPIECE($$^%MPIECE(params))
	;
	set (param,ix)="",ret=1
	for  set ix=$order(paramsA(ix)) quit:'$zlength(ix)  do  quit:'ret
	. ; ignore the version, as it is pre-processed
	. quit:paramsA(ix)="--version"!(paramsA(ix)="-v")
	. ;
	. ; ******************************
	. ; --help
	. ; ******************************
	. if paramsA(ix)="--help" do dumpHelp zhalt 0
	. ;
	. ; ******************************
	. ; --port
	. ; ******************************
	. if paramsA(ix)="--port" set param="--port" quit
	. ;
	. ; ******************************
	. ; --ttl-pool-time
	. ; ******************************
	. if paramsA(ix)="--ttl-pool-time" set param="--ttl-pool-time" quit
	. ;
	. ; ******************************
	. ; --api-only
	. ; ******************************
	. if paramsA(ix)="--api-only",'$zlength(param) set %ydbxiderParams("apiOnly")=1 quit
	. ;
	. ; ******************************
	. ; --test-mode
	. ; ******************************
	. if paramsA(ix)="--test-mode",'$zlength(param) set %ydbxiderParams("testMode")=1 quit
	. ;
	. ; ******************************
	. ; --reset-db
	. ; ******************************
	. if paramsA(ix)="--reset-db",'$zlength(param) set %ydbxiderParams("resetDb")=1 quit
	. ;
	. ; ******************************
	. ; --no-ttl
	. ; ******************************
	. if paramsA(ix)="--no-ttl",'$zlength(param) set %ydbxiderParams("noTtl")=1 quit
	. ;
	. ; ******************************
	. ; --no-journal-switch
	. ; ******************************
	. if paramsA(ix)="--no-journal-switch",'$zlength(param) set %ydbxiderParams("noJournalSwitch")=1 quit
	. ;
	. ; ******************************
	. ; --logging
	. ; ******************************
	. if paramsA(ix)="--logging" set param="--logging" quit
	. ;
	. ; ******************************
	. ; BAD PARAM
	. ; ******************************
	. if '$zlength(param) set ret=0,param="" write !,"Parameter: ",paramsA(ix)," not supported.",!!,"Quitting",!! quit
	. ;
	. ; ******************************
	. ; --port value
	. ; ******************************
	. set:+paramsA(ix)>0&(param="--port") %ydbxiderParams("port")=paramsA(ix),param=""
	. ;
	. ; ******************************
	. ; --ttl-pool-time
	. ; ******************************
	. set:+paramsA(ix)>0&(param="--ttl-pool-time") %ydbxiderParams("ttlPoolTime")=paramsA(ix),param=""
	. ;
	. ; ******************************
	. ; --logging value
	. ; ******************************
	. if param="--logging" do  set param=""
	. . set found=0 for debugMode="debug","verbose","notice","warning","nothing" set:paramsA(ix)=debugMode found=1 quit:found
	. . if 'found set ret=0 write !,"Parameter: ",paramsA(ix)," not supported.",!!,"Quitting",!! quit
	. . set %ydbxiderParams("logging")=paramsA(ix)
	;
	if $zlength(param) set ret=0 write !,"Parameter for "_param_" not specified or invalid.",!!,"Quitting",!!
	;
	quit ret
	;
	;
dumpHelp
	write !,"YottaDB Xider version "_%ydbxiderParams("version"),!
	write !,"Available parameters:"
	write !,"--version (-v)",?25,"Display the software version"
	write !,"--port {nnn}",?25,"Changes the default socket number (3000)"
	write !,"--api-only",?25,"Disable the Socket server and allow in-process API only"
	write !,"--logging {level}",?25,"Select out of: debug, verbose, notice, warning, nothing"
	write !,"--help",?25,"Display this text"
	write !,"--reset-db",?25,"Re-initialize the database"
	write !,"--no-ttl",?25,"Disable the expiring entries (TTL)"
	write !,"--ttl-pool-time {ms}",?25,"The frequency of the TTL checking in milliseconds"
	write !,"--no-journal-switch",?25,"Disable the automatic journal switching"
	write !!
	;
	quit
	;
	;
