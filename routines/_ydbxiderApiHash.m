;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; -----------------------------------------------
; This is the <HASH> data type data layer.
; -----------------------------------------------
;
; ***************************************
; HSET
; Sets the specified fields to their respective values in the hash stored at key.
; This command overwrites the values of specified fields that exist in the hash.
; If key doesn't exist, a new key holding a hash is created.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("data",nnn,"field")		String
; ("data",nnn,"value")		String (up to 1Mib)
; OR
; ("data",nnn,"value",n)	If string is > 1Mib, chunks of 1Mib in each n subscript (1 based). value is EMPTY
;
; Return (when called by external library)
;  >-1				The number of fields that have been added
; -1				no key provided
; -3				no data
; -4				some data has no field
; -5				some field / key combination too long
; -6				some data has no value
; -7				key exists, but not hash data-type
; ***************************************
HSET ;
	new ret,socketResp,ix,error
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HSET"))
	;
	set socketResp=$zlength($get(CRLF)),ret="",error=0
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HSET received")
	;
	goto:noParamsValidation HSETcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HSETquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$data(xider("data",1))  do  goto HSETquit
	. if socketResp set xider("ret")="-no data"_CRLF
	. else  set ret=-3
	;
	; verify data
	set ix="" for  set ix=$order(xider("data",ix)) quit:'$zlength(ix)  do  quit:error
	. if '$zlength($get(xider("data",ix,"field"))) set error=1 do  quit
	. . if socketResp set xider("ret")="-data "_ix_" has no field"_CRLF
	. . else  set ret=-4
	. if '$$verifyKeyLength^%ydbxiderAPI(xider("key")_xider("data",ix,"field")) set error=1 do  quit
	. . if socketResp set xider("ret")="-data "_ix_" field too long"_CRLF
	. . else  set ret=-5
	. if '$data(xider("data",ix,"value"))!('$zlength($get(xider("data",ix,"value")))) set error=1 do
	. . if socketResp set xider("ret")="-data "_ix_" has no value"_CRLF
	. . else  set ret=-6
	;
	goto:error HSETquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HSETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HSET",ret=0
	;
HSETcontinue ;
	new exists,cnt
	set cnt=0,ret=""
	;
	; flag pre-existence of a key
	set exists=$data(^%ydbxiderK(xider("key")))
	;
	; verify key type
	if exists,^%ydbxiderKDT(xider("key"))'="hash" do  goto HSETquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-7
	;
	; create key if it doesn't exists and get a hash Handle
	set:'exists ^%ydbxiderKDT(xider("key"))="hash"
	;
	; store / update data
	set ix="" for  set ix=$order(xider("data",ix)) quit:'$zlength(ix)  do
	. set:'$data(^%ydbxiderK(xider("key"),xider("data",ix,"field"))) ret=$increment(^%ydbxiderK(xider("key"))),cnt=cnt+1
	. set ^%ydbxiderK(xider("key"),xider("data",ix,"field"))=xider("data",ix,"value")
	;
	if socketResp set xider("ret")=":"_cnt_CRLF
	else  set ret=cnt
	;
HSETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HSETNX
; Sets field in the hash stored at key to value, only if field does not yet exist.
; If key does not exist, a new key holding a hash is created.
; If field already exists, this operation has no effect.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("field")			String
; ("value")			String (up to 1Mib)
; OR
; ("value",n)	If string is > 1Mib, chunks of 1Mib in each n subscript (1 based). value is EMPTY
;
; Return (when called by external library)
;  0				Success
; -1				no key provided
; -3				no field
; -4				field / key combination too long
; -5				no value
; -6				key not found
; -7				key exists, but not hash data-type
; -8				field already exists
; ***************************************
HSETNX ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HSETNX"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HSETNX received")
	;
	goto:noParamsValidation HSETNXcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HSETNXquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$zlength($get(xider("field")))  do  goto HSETquit
	. if socketResp set xider("ret")="-no field"_CRLF
	. else  set ret=-3
	if '$$verifyKeyLength^%ydbxiderAPI(xider("key")_xider("field")) set ret=-4 goto HSETNXquit
	if '$zlength($get(xider("value")))  do  goto HSETquit
	. if socketResp set xider("ret")="-no value"_CRLF
	. else  set ret=-5
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HSETNXquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HSETNX",ret=0
	;
HSETNXcontinue ;
	set ret=""
	;
	; check key existence
	set:'$data(^%ydbxiderK(xider("key"))) ^%ydbxiderK(xider("key"))=0,^%ydbxiderKDT(xider("key"))="hash"
	;
	; verify key type
	if $data(^%ydbxiderK(xider("key"))),^%ydbxiderKDT(xider("key"))'="hash" do  goto HSETNXquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-7
	;
	; verify if field exists
	if $data(^%ydbxiderK(xider("key"),xider("field"))) do  goto HSETNXquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-8
	;
	; create the new field
	set:$increment(^%ydbxiderK(xider("key"))) ^%ydbxiderK(xider("key"),xider("field"))=xider("value"),ret=0
	set:socketResp xider("ret")=":1"_CRLF
	;
HSETNXquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HGET
; Sets the specified fields to their respective values in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("field")			String
;
; Return (when called by external library)
;  2				string in xider("ret",nnn)
;  1				string in xider("ret")
; -1				no key provided
; -3				no field
; -4				field / key combination too long
; -5				key not found
; -6				key exists, but not hash data-type
; -7				field doesn't exists
; ***************************************
HGET ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HGET"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HGET received")
	;
	goto:noParamsValidation HGETcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HGETquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$zlength($get(xider("field")))  do  goto HGETquit
	. if socketResp set xider("ret")="-no field"_CRLF
	. else  set ret=-3
	if xiderSTRINGMAXKEYLENGTH<$zlength(xider("key")_xider("field")) do  goto HGETquit
	. if socketResp set xider("ret")="-field / key combination too long"_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HGETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HGET",ret=0
	;
HGETcontinue ;
	new data
	set ret=""
	;
	; check key existence
	if '$data(^%ydbxiderK(xider("key"))) do  goto HGETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-5
	;
	; verify key type
	if $data(^%ydbxiderK(xider("key"))),^%ydbxiderKDT(xider("key"))'="hash" do  goto HGETquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-6
	;
	set data=$data(^%ydbxiderK(xider("key"),xider("field")))
	;
	; verify if field exists
	if 'data do  goto HGETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-7
	;
	; get the data
	if 9<data merge xider("ret")=^%ydbxiderK(xider("key"),xider("field")) set ret=2
	else  do
	. ;if socketResp set xider("ret")="+"_^%ydbxiderK(xider("key"),xider("field"))_CRLF,ret=1
	. if socketResp do
	. . new val set val=^%ydbxiderK(xider("key"),xider("field"))
	. . set xider("ret")="$"_$zlength(val)_CRLF_val_CRLF
	. . set ret=1
	. else  set xider("ret")=$select(socketResp:"+",1:"")_^%ydbxiderK(xider("key"),xider("field")),ret=1
	;
HGETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HDEL
; Removes the specified fields from the hash stored at key.
; Specified fields that do not exist within this hash are ignored. Deletes the hash if no fields remain.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("fields",nnn)		String
;
; Return (when called by external library)
;  >-1				The number of fields that have been deleted
; -1				no key provided
; -3				no fields
; -4				field / key combination too long
; -5				key not found
; -6				key exists, but not hash data-type
; ***************************************
HDEL ;
	new ret,socketResp,ix,error
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HDEL"))
	;
	set (ret,error)=0
	set socketResp=$zlength($get(CRLF))
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HDEL received")
	;
	goto:noParamsValidation HDELcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HDELquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$data(xider("fields",1)) do  goto HDELquit
	. if socketResp set xider("ret")="-no fields"_CRLF
	. else  set ret=-3
	;
	; fields length
	set ix="" for  set ix=$order(xider("fields",ix)) quit:'$zlength(ix)  do  quit:error
	. if '$$verifyKeyLength^%ydbxiderAPI(xider("fields",ix)_xider("key")) set ret=-4,error=1 goto HDELquit
	;
	goto:error HDELquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HDELquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HDEL",ret=0
	;
HDELcontinue ;
	new dummy
	set ret=0
	;
	; verify that key exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto HDELquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-5
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HDELquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-6
	;
	; delete fields
	set ix="" for  set ix=$order(xider("fields",ix)) quit:'$zlength(ix)  do  quit:error
	. set:$data(^%ydbxiderK(xider("key"),xider("fields",ix))) dummy=$increment(^%ydbxiderK(xider("key")),-1),ret=ret+1
	. kill ^%ydbxiderK(xider("key"),xider("fields",ix))
	;
	; and key, if hash is empty
	kill:'^%ydbxiderK(xider("key")) ^%ydbxiderK(xider("key"))
	;
	set:socketResp xider("ret")=":"_ret_CRLF
HDELquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HLEN
; Returns the number of fields contained in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
; >0				The number of fields in the hash
; 0				no fields or no key (in accordance with Redis behaviour)
; -1				no key provided
; -2				key too long
; -3				key not found
; -4				key exists, but not hash data-type
; ***************************************
HLEN ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HLEN"))
	;
	set (ret,error)=0
	set socketResp=$zlength($get(CRLF))
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HLEN received")
	;
	goto:noParamsValidation HLENcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HLENquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto HLENquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HLENquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HLEN",ret=0
	;
HLENcontinue ;
	set ret=0
	;
	; verify that key exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto HLENquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=0
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HLENquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-4
	;
	set ret=^%ydbxiderK(xider("key"))
	set:socketResp xider("ret")=":"_ret_CRLF
	;
HLENquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HSTRLEN
; Returns the string length of the value associated with field in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("field")			String
;
; Return (when called by external library)
; >0				the value length
; -1				no key provided
; -3				no field
; -4				field / key combination too long
; -5				key not found
; -6				key exists, but not hash data-type
; -7				field doesn't exists
; ***************************************
HSTRLEN ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HSTRLEN"))
	;
	set socketResp=$zlength($get(CRLF)),ret=0
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HSTRLEN received")
	;
	goto:noParamsValidation HSTRLENcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HSTRLENquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$zlength($get(xider("field")))  do  goto HSTRLENquit
	. if socketResp set xider("ret")="-no field"_CRLF
	. else  set ret=-3
	if xiderSTRINGMAXKEYLENGTH<$zlength(xider("field")_xider("key")) do  goto HSTRLENquit
	. if socketResp set xider("ret")="-field too long"_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HSTRLENquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HSTRLEN",ret=0
	;
HSTRLENcontinue ;
	new data,ix
	set ret=0
	;
	; check key existence
	if '$data(^%ydbxiderK(xider("key"))) do  goto HSTRLENquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-5
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HSTRLENquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-6
	;
	set data=$data(^%ydbxiderK(xider("key"),xider("field")))
	;
	; verify if field exists
	if 'data do  goto HSTRLENquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-7
	;
	; get the length
	set:9>data ret=$zlength(^%ydbxiderK(xider("key"),xider("field")))
	else  do
	. set ix="" for  set ix=$order(^%ydbxiderK(xider("key"),xider("field"),ix)) quit:'$zlength(ix)  set ret=ret+$zlength(^%ydbxiderK(xider("key"),xider("field"),ix))
	set:socketResp xider("ret")=":"_ret_CRLF
	;
HSTRLENquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HKEYS
; Returns all field names in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
; 0				success, fields are in xider("ret",nnn)
; -1				no key provided
; -2				key too long
; -3				key not found
; -4				key exists, but not hash data-type
; ***************************************
HKEYS ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HKEYS"))
	;
	set socketResp=$zlength($get(CRLF))
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HKEYS received")
	;
	goto:noParamsValidation HKEYScontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HKEYSquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto HKEYSquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HKEYSquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HKEYS",ret=0
	;
HKEYScontinue ;
	new ix,cnt
	set ret=0,cnt=0
	;
	; check key existence
	if '$data(^%ydbxiderK(xider("key"))) do  goto HKEYSquit
	. set:socketResp xider("ret")="*0"_CRLF
	. set ret=-3
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HKEYSquit
	. set:socketResp xider("ret")=wrongDataType_CRLF
	. set ret=-4
	;
	set:socketResp xider("ret")=""
	set ix="" for  set ix=$order(^%ydbxiderK(xider("key"),ix)) quit:'$zlength(ix)  do
	. set cnt=cnt+1
	. if socketResp set xider("ret")=xider("ret")_"$"_$zlength(ix)_CRLF_ix_CRLF
	. else  set xider("ret",cnt)=ix
	if socketResp set xider("ret")="*"_cnt_CRLF_xider("ret")
	else  set xider("ret")=cnt
	;
HKEYSquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HVALS
; Returns all values in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
; 0				success, values are in xider("ret",nnn)
; -1				no key provided
; -2				key too long
; -3				key not found
; -4				key exists, but not hash data-type
; ***************************************
HVALS ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HVALS"))
	;
	set socketResp=$zlength($get(CRLF))
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HVALS received")
	;
	goto:noParamsValidation HVALScontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HVALSquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto HVALSquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HVALSquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HVALS",ret=0
	;
HVALScontinue ;
	new ix,cnt
	set ret=0,cnt=0
	;
	; check key existence
	if '$data(^%ydbxiderK(xider("key"))) do  goto HVALSquit
	. set:socketResp xider("ret")="$-1"_CRLF
	. set ret=-3
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HVALSquit
	. set:socketResp xider("ret")=wrongDataType_CRLF
	. set ret=-4
	;
	set:socketResp xider("ret")=""
	set ix="" for  set ix=$order(^%ydbxiderK(xider("key"),ix)) quit:'$zlength(ix)  do
	. set cnt=cnt+1
	. if socketResp set xider("ret")=xider("ret")_"$"_$zlength(^%ydbxiderK(xider("key"),ix))_CRLF_^%ydbxiderK(xider("key"),ix)_CRLF
	. else  set xider("ret",cnt)=^%ydbxiderK(xider("key"),ix)
	if socketResp set xider("ret")="*"_cnt_CRLF_xider("ret")
	else  set xider("ret")=cnt
	;
HVALSquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HEXISTS
; Returns if field is an existing field in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("field")			String
;
; Return (when called by external library)
;  0				success, the field exists
; -1				no key provided
; -3				no field
; -4				field / key combination too long
; -5				key not found
; -6				key exists, but not hash data-type
; -7				field doesn't exists
; ***************************************
HEXISTS ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HEXISTS"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HEXISTS received")
	;
	goto:noParamsValidation HEXISTScontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HEXISTSquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$zlength($get(xider("field")))  do  goto HEXISTSquit
	. if socketResp set xider("ret")="-no field"_CRLF
	. else  set ret=-3
	if xiderSTRINGMAXKEYLENGTH<$zlength(xider("field")_xider("key")) do  goto HEXISTSquit
	. if socketResp set xider("ret")="-field too long"_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HEXISTSquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HEXISTS",ret=0
	;
HEXISTScontinue ;
	set ret=""
	;
	; check key existence
	if '$data(^%ydbxiderK(xider("key"))) do  goto HEXISTSquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-5
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HEXISTSquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-6
	;
	; check existance
	if 'socketResp set ret=$select($data(^%ydbxiderK(xider("key"),xider("field"))):0,1:-7)
	else  set xider("ret")=$select($data(^%ydbxiderK(xider("key"),xider("field"))):":1",1:":0")_CRLF
	;
HEXISTSquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HMGET
; Returns the values associated with the specified fields in the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("fields",nnn)		String
;
; Return (when called by external library)
;  0				success, values are returned in xider("ret",nnn)=value
; -1				no key provided
; -3				no fields
; -4				some field / key combination too long
; -5				key not found
; -6				key exists, but not hash data-type
; ***************************************
HMGET ;
	new ret,socketResp,ix,error
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HMGET"))
	;
	set (ret,error)=0
	set socketResp=$zlength($get(CRLF))
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HMGET received")
	;
	goto:noParamsValidation HMGETcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HMGETquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$data(xider("fields",1)) do  goto HMGETquit
	. if socketResp set xider("ret")="-no fields"_CRLF
	. else  set ret=-3
	;
	; fields length
	set ix="" for  set ix=$order(xider("fields",ix)) quit:'$zlength(ix)  do  quit:error
	. if '$$verifyKeyLength^%ydbxiderAPI(xider("fields",ix)_xider("key")) set ret=-4,error=1 goto HMGETquit
	;
	goto:error HMGETquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HMGETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HMGET",ret=0
	;
HMGETcontinue ;
	new value
	set ret=0
	;
	; verify that key exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto HMGETquit
	. if socketResp do
	. . set xider("ret")="*"_$order(xider("fields",""),-1)_CRLF
	. . set ix="" for  set ix=$order(xider("fields",ix)) quit:'$zlength(ix)  set xider("ret")=xider("ret")_"$-1"_CRLF
	. else  set ret=-5
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HMGETquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-6
	;
	; get the data
	set:socketResp xider("ret")="*"_$order(xider("fields",""),-1)_CRLF
	set ix="" for  set ix=$order(xider("fields",ix)) quit:'$zlength(ix)  do
	. if 'socketResp set xider("ret",ix)=$get(^%ydbxiderK(xider("key"),xider("fields",ix)))
	. else  do
	. . set value=$get(^%ydbxiderK(xider("key"),xider("fields",ix)))
	. . if '$zlength(value) set xider("ret")=xider("ret")_"$-1"_CRLF
	. . else  set xider("ret")=xider("ret")_"$"_$zlength(value)_CRLF_value_CRLF
	;
	set ret=0
	;
HMGETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HGETALL
; Returns all fields and values of the hash stored at key.
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
; 0				Success (data is in xider("ret",nnn))
; -1				no key provided
; -2				key too long
; -3				key not found
; -4				key exists, but not hash data-type
; ***************************************
HGETALL ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HGETALL"))
	;
	set socketResp=$zlength($get(CRLF))
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HGETALL received")
	;
	goto:noParamsValidation HGETALLcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HGETALLquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto HGETALLquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HGETALLquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HGETALL",ret=0
	;
HGETALLcontinue ;
	new ix
	set ret=0
	;
	; verify that key exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto HGETALLquit
	. if socketResp set xider("ret")="*0"_CRLF
	. else  set ret=-3
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HGETALLquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-4
	;
	if 'socketResp merge xider("ret")=^%ydbxiderK(xider("key"))
	else  do
	. set xider("ret")="*"_(^%ydbxiderK(xider("key"))*2)_CRLF
	. set ix="" for  set ix=$order(^%ydbxiderK(xider("key"),ix)) quit:'$zlength(ix)  do
	. . set xider("ret")=xider("ret")_"$"_$zlength(ix)_CRLF_ix_CRLF_"$"_$zlength(^%ydbxiderK(xider("key"),ix))_CRLF_^%ydbxiderK(xider("key"),ix)_CRLF
	;
HGETALLquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HRANDFIELD
; When called with just the key argument, return a random field from the hash value stored at key.
;
; If the provided count argument is positive, return an array of distinct fields.
; The array's length is either count or the hash's number of fields (HLEN), whichever is lower.
;
; If called with a negative count, the behavior changes and the command is allowed to return the same field multiple times.
; In this case, the number of returned fields is the absolute value of the specified count.
;
; The optional WITHVALUES modifier changes the reply so it includes the respective values of the randomly selected hash fields.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("count")			(optional) signed integer
; ("withValues")		(optional) empty string
;
; Return (when called by external library)
; 0				Success (data is in xider("ret",cnt,fld)="" | value)
; -1				no key provided
; -2				key too long
; -3				invalid count
; -4				withValues needs count set
; -5				key not found
; -6				key exists, but not hash data-type
; ***************************************
HRANDFIELD ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HRANDFIELD"))
	;
	set socketResp=$zlength($get(CRLF)),ret=0
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HRANDFIELD received")
	;
	goto:noParamsValidation HRANDFIELDcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HRANDFIELDquit
	. set:socketResp xider("ret")="-no key"_CRLF
	. set ret=-1
	;
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto HRANDFIELDquit
	;
	if $data(xider("count")),xider("count")'=0,'+$get(xider("count")) do  goto HRANDFIELDquit
	. set:socketResp xider("ret")="-invalid count"_CRLF
	. set ret=-3
	;
	if '$data(xider("count")),$data(xider("withValues")) do  goto HRANDFIELDquit
	. set:socketResp xider("ret")="-withValues needs count set"_CRLF
	. set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HRANDFIELDquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HRANDFIELD",ret=0
	;
HRANDFIELDcontinue ;
	new cnt,ix,iy,iz,rnd,i,random,val
	set (ret,cnt)=0
	;
	; verify that key exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto HRANDFIELDquit
	. set:socketResp xider("ret")="$-1"_CRLF
	. set ret=-5
	;
	; verify key type
	if ^%ydbxiderKDT(xider("key"))'="hash" do  goto HRANDFIELDquit
	. set:socketResp xider("ret")=wrongDataType_CRLF
	. set ret=-6
	;
	; execute
	set:socketResp xider("ret")=""
	if '$data(xider("count")) do
	. ; no count, random is 1 <-> # of fields
	. set ix="",rnd=$random(^%ydbxiderK(xider("key")))+1 for  set ix=$order(^%ydbxiderK(xider("key"),ix)) do  quit:cnt=rnd
	. . set cnt=cnt+1 if cnt=rnd do
	. . . if socketResp set xider("ret")="$"_$zlength(ix)_CRLF_ix_CRLF
	. . . else  set xider("ret",1)=ix,xider("ret")=1
	else  do
	. if 0=xider("count") do
	. . if socketResp  set xider("ret")="*"_0_CRLF
	. . else  set xider("ret")=0
	. else  if 0<xider("count") do
	. . ; positive count
	. . if xider("count")>=^%ydbxiderK(xider("key")) do
	. . . ; count >= # of fields, return everything
	. . . set ix="" for i=1:1 set ix=$order(^%ydbxiderK(xider("key"),ix)) quit:'$zlength(ix)  set cnt=cnt+1 do
	. . . . set:socketResp xider("ret")=xider("ret")_"$"_$zlength(ix)_CRLF_ix_CRLF
	. . . . ;
	. . . . if $data(xider("withValues")) do
	. . . . . set val=^%ydbxiderK(xider("key"),ix),cnt=cnt+1
	. . . . . if socketResp set xider("ret")=xider("ret")_"$"_$zlength(val)_CRLF_val_CRLF
	. . . . . else  set xider("ret",i,ix)=val
	. . . . else  set xider("ret",i)=ix
	. . . if socketResp set xider("ret")="*"_cnt_CRLF_xider("ret")
	. . . else  set xider("ret")=i-1
	. . else  do
	. . . ;count < # of fields, no duplicates
	. . . for i=1:1 do  quit:cnt=xider("count")
	. . . . set rnd($random(^%ydbxiderK(xider("key")))+1)=i
	. . . . set cnt=0,ix="" for  set ix=$order(rnd(ix)) quit:'$zlength(ix)  set cnt=cnt+1
	. . . set i="" for  set i=$order(rnd(i)) quit:'$zlength(i)  set random(rnd(i))=i
	. . . ;
	. . . set ix="",cnt=0 for i=1:1 set ix=$order(random(ix)) quit:'$zlength(ix)  do
	. . . . set iy="",iz=0 for  set iy=$order(^%ydbxiderK(xider("key"),iy)),iz=iz+1 quit:'$zlength(iy)  if iz=random(ix) set cnt=cnt+1 do
	. . . . . set:socketResp xider("ret")=xider("ret")_"$"_$zlength(iy)_CRLF_iy_CRLF
	. . . . . ;
	. . . . . if $data(xider("withValues")) do
	. . . . . . set val=^%ydbxiderK(xider("key"),iy),cnt=cnt+1
	. . . . . . if socketResp set xider("ret")=xider("ret")_"$"_$zlength(val)_CRLF_val_CRLF
	. . . . . . else  set xider("ret",i,iy)=val
	. . . . . else  set xider("ret",i)=iy
	. . . if socketResp set xider("ret")="*"_cnt_CRLF_xider("ret")
	. . . else  set xider("ret")=i-1
	. else  do
	. . ; negative count, allow duplicates
	. . set cnt=0 for i=1:1:-xider("count") set rnd=$random(^%ydbxiderK(xider("key")))+1 do
	. . . set ix=0,iy="" for  set iy=$order(^%ydbxiderK(xider("key"),iy)),ix=ix+1 quit:'$zlength(iy)  if ix=rnd set cnt=cnt+1 do
	. . . . set:socketResp xider("ret")=xider("ret")_"$"_$zlength(iy)_CRLF_iy_CRLF
	. . . . ;
	. . . . if $data(xider("withValues")) do
	. . . . . set val=^%ydbxiderK(xider("key"),iy),cnt=cnt+1
	. . . . . if socketResp set xider("ret")=xider("ret")_"$"_$zlength(val)_CRLF_val_CRLF
	. . . . . else  set xider("ret",i,iy)=^%ydbxiderK(xider("key"),iy)
	. . . . else  set xider("ret",i)=iy
	. . if socketResp set xider("ret")="*"_cnt_CRLF_xider("ret")
	. . else  set xider("ret")=i
	;
HRANDFIELDquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HINCRBY
; Increments the number stored at field in the hash stored at key by increment.
; If key does not exist, a new key holding a hash is created.
; If field does not exist the value is set to 0 before the operation is performed.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("field")			String
; ("increment")			signed integer
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -3				field not provided
; -4				field / key combination too long
; -5				no increment provided
; -6				increment not an integer
; -7				data-type not a string
; -8				value not an integer
; ***************************************
HINCRBY ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HINCRBY"))
	;
	set socketResp=$zlength($get(CRLF)),(exists,ret)=0
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HINCRBY received:")
	;
	goto:noParamsValidation HINCRBYcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HINCRBYquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$zlength($get(xider("field")))  do  goto HINCRBYquit
	. if socketResp set xider("ret")="-no field"_CRLF
	. else  set ret=-3
	if xiderSTRINGMAXKEYLENGTH<$zlength(xider("field")_xider("key")) do  goto HINCRBYquit
	. if socketResp set xider("ret")="-field too long"_CRLF
	. else  set ret=-4
	;
	if '$zlength($get(xider("increment")))  do  goto HINCRBYquit
	. if socketResp set xider("ret")="-no increment"_CRLF
	. else  set ret=-5
	if '$$isInteger^%ydbxiderAPI(xider("increment"))!(xider("increment")=0) do  goto HINCRBYquit
	. if socketResp set xider("ret")=valueNotAnInteger_CRLF
	. else  set ret=-6
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HINCRBYquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HINCRBY",ret=0
	;
HINCRBYcontinue ;
	new fieldExists
	set (ret,fieldExists)=0
	;
	; flag pre-existence of a key
	set exists=$data(^%ydbxiderK(xider("key")))
	set:exists fieldExists=$data(^%ydbxiderK(xider("key"),xider("field")))
	;
	; verify key type
	if exists,^%ydbxiderKDT(xider("key"))'="hash" do  goto HINCRBYquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-7
	;
	; and field type
	if exists,fieldExists,'$$isInteger^%ydbxiderAPI(^%ydbxiderK(xider("key"),xider("field"))) do  goto HINCRBYquit
	. if socketResp set xider("ret")="-value not an integer"_CRLF
	. else  set ret=-8
	;
	; create field / hash if they don't exists
	if 'exists set xider("data",1,"field")=xider("field"),xider("data",1,"value")=0 do HSET^xider()
	;
	if socketResp set xider("ret")=":"_$increment(^%ydbxiderK(xider("key"),xider("field")),xider("increment"))_CRLF,ret=0
	else  set xider("ret")=$increment(^%ydbxiderK(xider("key"),xider("field")),xider("increment")),ret=0
	;
HINCRBYquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; HINCRBYFLOAT
; Increments the number stored at field in the hash stored at key by increment.
; If key does not exist, a new key holding a hash is created.
; If field does not exist the value is set to 0 before the operation is performed.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("field")			String
; ("increment")			signed fp
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -3				field not provided
; -4				field / key combination too long
; -5				no increment provided
; -6				increment not an fp
; -7				data-type not a string
; -8				value not an fp
; ***************************************
HINCRBYFLOAT ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","HINCRBYFLOAT"))
	;
	set socketResp=$zlength($get(CRLF)),(exists,ret)=0
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("HINCRBYFLOAT received:")
	;
	goto:noParamsValidation HINCRBYFLOATcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto HINCRBYFLOATquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	;
	if '$zlength($get(xider("field")))  do  goto HINCRBYFLOATquit
	. if socketResp set xider("ret")="-no field"_CRLF
	. else  set ret=-3
	if xiderSTRINGMAXKEYLENGTH<$zlength(xider("field")_xider("key")) do  goto HINCRBYFLOATquit
	. if socketResp set xider("ret")="-field too long"_CRLF
	. else  set ret=-4
	;
	if '$zlength($get(xider("increment")))  do  goto HINCRBYFLOATquit
	. if socketResp set xider("ret")="-no increment"_CRLF
	. else  set ret=-5
	if '$$isFloat^%ydbxiderAPI(xider("increment"))!(xider("increment")=0) do  goto HINCRBYFLOATquit
	. if socketResp set xider("ret")="-increment not a double"_CRLF
	. else  set ret=-6
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto HINCRBYFLOATquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="HINCRBYFLOAT",ret=0
	;
HINCRBYFLOATcontinue ;
	new fieldExists
	set (ret,fieldExists)=0
	;
	; flag pre-existence of a key
	set exists=$data(^%ydbxiderK(xider("key")))
	; and a field
	set:exists fieldExists=$data(^%ydbxiderK(xider("key"),xider("field")))
	;
	; verify key type
	if exists,^%ydbxiderKDT(xider("key"))'="hash" do  goto HINCRBYFLOATquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-7
	;
	; and field type
	if exists,fieldExists,'$$isFloat^%ydbxiderAPI(^%ydbxiderK(xider("key"),xider("field"))) do  goto HINCRBYquit
	. if socketResp set xider("ret")="-ERR value is not a double or out of range"_CRLF
	. else  set ret=-8
	;
	; create field / hash if they don't exists
	if 'fieldExists set xider("data",1,"field")=xider("field"),xider("data",1,"value")=0 do HSET^xider()
	;
	set xider("ret")=$increment(^%ydbxiderK(xider("key"),xider("field")),xider("increment")),ret=0
	set:socketResp xider("ret")=","_xider("ret")_CRLF
	;
HINCRBYFLOATquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
