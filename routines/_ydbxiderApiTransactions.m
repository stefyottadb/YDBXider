;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property          #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; -----------------------------------------------
; This is the <Transaction> namespace.
; -----------------------------------------------
;
; ***************************************
; WATCH
; Marks the given keys to be watched for conditional execution of a transaction.
; ***************************************
; Entries for xider:
;
; ("keys",nnn)			String
;
; Return (when called by external library)
;  0				Success (data is in xider(ret,nnn) if key doesn't exists or is not a string, xider(ret,nnn) will be an empty string)
; -1				no keys provided
; -2				one of the keys is too long
; -7				watch inside multi is not allowed
; ***************************************
WATCH ;
	new ix,socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","WATCH"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("WATCH received")
	;
	; error out if a MULTI is currently in progress
	if $get(xiderMulti) do  goto WATCHquit
	. if socketResp set xider("ret")="-ERR WATCH inside MULTI is not allowed"_CRLF
	. else  set ret=-7
	;
	goto:noParamsValidation WATCHcontinue
	;
	; verify that keys are passed
	if '$data(xider("keys")) do  goto WATCHquit
	. if socketResp set xider("ret")="-no keys"_CRLF
	. else  set ret=-1
	;
	; verify multiple keys length
	if '$$verifyKeysLength^%ydbxiderAPI() set ret=-2 goto WATCHquit
	;
WATCHcontinue ;
	set ix="" for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)  do
	. if $data(^%ydbxiderK(xider("keys",ix))) do
	. . ; add current hash key
	. . if ^%ydbxiderKDT(xider("keys",ix))="hash" merge xiderWatch("hash",xider("keys",ix))=^%ydbxiderK(xider("keys",ix))
	. . ; add current string key
	. . else  set xiderWatch("string",xider("keys",ix))=^%ydbxiderK(xider("keys",ix))
	. ; add non-existent key
	. else  set xiderWatch("none",xider("keys",ix))=""
	;
	if socketResp set xider("ret")="+OK"_CRLF
	else  set ret=0
	;
WATCHquit ;
	kill xider("keys") ; remove so that this data doesn't end up in the command queue (xiderCmd)
	set:'$quit xider("status")=ret
	quit:$quit ret quit
	;
	;
; ***************************************
; UNWATCH
; Flushes all the previously watched keys for a transaction.
; If you call EXEC or DISCARD, there's no need to manually call UNWATCH.
; ***************************************
; Entries for xider:
;
; N/A
;
; Return (when called by external library) [always succeeds]
;  0				Ok
; ***************************************
UNWATCH ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","UNWATCH"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("UNWATCH received")
	;
	kill xiderWatch
	;
	if socketResp set xider("ret")="+OK"_CRLF
	else  set ret=0
	;
	set:'$quit xider("status")=ret
	quit:$quit ret quit
	;
	;
; ***************************************
; MULTI
; Marks the start of a transaction block. Subsequent commands will be queued for atomic execution using EXEC.
; ***************************************
; Entries for xider:
;
; N/A
;
; Return (when called by external library) [always succeeds]
;  0				Ok
; -4				No nested transactions
; ***************************************
MULTI ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","MULTI"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("MULTI received")
	;
	; error out if a MULTI is already in progress
	if $get(xiderMulti) do  goto MULTIquit
	. if socketResp set xider("ret")="-ERR MULTI calls can not be nested"_CRLF
	. else  set ret=-4
	;
	set xiderMulti=1
	;
	if socketResp set xider("ret")="+OK"_CRLF
	else  set ret=0
	;
MULTIquit ;
	set:'$quit xider("status")=ret
	quit:$quit ret quit
	;
	;
; ***************************************
; EXEC
; Executes all previously queued commands in a transaction and restores the connection state to normal.
; When using WATCH, EXEC will execute commands only if the watched keys were not modified, allowing for a check-and-set mechanism.
; ***************************************
; Entries for xider:
;
; N/A
;
; Return (when called by external library) [always succeeds]
;  0				Ok
; -3				No current transaction
; -5				Aborted transaction because a WATCHed key was modified
; -6				Discarded transaction because a queued command returned an error
; ***************************************
EXEC ;
	; create very simple error trap to rollback a transaction and turn off error processing if there is a bug in the code
	new $etrap set $etrap="goto EXECerror^%ydbxiderApiTransactions"
	new $estack,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","EXEC"))
	;
	set socketResp=$zlength($get(CRLF)),xiderRet="",xiderStatus=0
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("EXEC received")
	;
	; discarded transaction because a command returned an error before being queued
	if $get(xiderMulti)=-1 do  goto EXECquit
	. set:socketResp xiderRet="-EXECABORT Transaction discarded because of previous errors."_CRLF
	. else  set xiderStatus=-6
	. kill xiderMulti
	;
	; error out if a MULTI is not currently in progress
	if '$get(xiderMulti) do  goto EXECquit
	. if socketResp set xiderRet="-ERR EXEC without MULTI"_CRLF
	. else  set xiderStatus=-3
	;
	; scope variables for transaction
	new type,key,wFld,kFld,err,i
	; init variables for transaction
	set key="",err=0 set:'($data(xiderCmd)#2) xiderCmd=0
	if socketResp set:xiderBulk xiderRet=xiderBulkReq set xiderRet=xiderRet_"*"_xiderCmd_CRLF
	;
	; restartable transaction
	tstart (xiderMulti,xiderWatch,xiderCmd,xiderRet,key,err):transactionid="batch"
	;
	; process the watch queue
	for type="none","string","hash" do  if err trollback  goto EXECquit
	. for  set key=$order(xiderWatch(type,key)) quit:'$zlength(key)  do  quit:err
	. . ; non-existent keys
	. . if type="none",$data(^%ydbxiderK(key)) do  quit:err
	. . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . else  set xiderStatus=-5
	. . . set err=1
	. . ;
	. . quit:type="none" ; the rest of the tests are not applicable if the key still doesn't exist
	. . ;
	. . ; string and hash keys
	. . if $get(^%ydbxiderKDT(key))'=type do  quit:err  ; key has wrong type
	. . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . else  set xiderStatus=-5
	. . . set err=2
	. . ;
	. . if '$data(^%ydbxiderK(key)) do  quit:err  ; key has been removed
	. . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . else  set xiderStatus=-5
	. . . set err=3
	. . ;
	. . if $get(^%ydbxiderK(key))'=xiderWatch(type,key) do  quit:err  ; key has been modified
	. . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . else  set xiderStatus=-5
	. . . set err=4
	. . ;
	. . ; only hash keys
	. . do:type="hash"
	. . . set (wFld,kFld)="" for  set wFld=$order(xiderWatch(type,key,wFld)),kFld=$order(^%ydbxiderK(key,kFld)) quit:'$zlength(wFld)!'$zlength(kFld)  do  quit:err
	. . . . ; wrong field found
	. . . . if wFld'=kFld do  quit:err
	. . . . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . . . else  set xiderStatus=-5
	. . . . . set err=5
	. . . . ;
	. . . . ; field value was modified
	. . . . if xiderWatch(type,key,wFld)'=^%ydbxiderK(key,kFld) do  quit:err
	. . . . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . . . else  set xiderStatus=-5
	. . . . . set err=6
	. . . ;
	. . . ; wrong number of fields
	. . . if wFld'=kFld do  quit:err
	. . . . if socketResp set xiderRet=xiderBulkReq_"*-1"_CRLF ; (nil) aborted because a WATCHed key was modified
	. . . . else  set xiderStatus=-5
	. . . . set err=7
	;
	; process the command queue
	for i=1:1:xiderCmd do  if err trollback  goto EXECquit
	. ; log for each command here, since socket response is turned off for commands in transactions
	. do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger(xiderCmd(i)_" received during MULTI")
	. ; reset xider for each command
	. kill xider
	. ; set up the input parameters
	. merge xider=xiderCmd(i)
	. ;
	. ; call the data layer at the continue entry point
	. do @(xiderCmd(i)_"continue")^xider
	. ;
	. ; discarded transaction because a queued command returned an error
	. if xider("status")<0 do  quit:err
	. . set:socketResp xiderRet="-EXECABORT Transaction discarded because of previous errors."_CRLF
	. . else  set xiderStatus=-6
	. . set err=8
	. ;
	. ; add command response to the RESP response
	. if socketResp set xiderRet=xiderRet_xider("ret")
	. else  do
	. . ; add command response to the return array
	. . set xiderStatus(i)=xider("status")
	. . merge xiderRet(i)=xider("ret")
	;
	; commit the transaction
	tcommit
	;
	; clean up after last command
	kill xider("ret"),xider("status") zkill xider
	; add command count to return array when not using RESP
	set:'socketResp xiderRet=xiderCmd
	;
EXECquit ;
	; clean up after executing the transaction
	kill xiderMulti,xiderWatch,xiderCmd
	; turn off MULTI bulk requests
	set xiderBulk=0,xiderBulkReq=""
	;
	quit:$quit xiderStatus quit
	;
EXECerror ; error handler for transactions
	trollback:$tlevel
	if socketResp do mainErrorHandler^%ydbxiderServerSession
	else  set xiderRet=$ecode_": "_$zstatus,$ecode="" zgoto -$estack:EXECquit
	;
	;
; ***************************************
; DISCARD
; Flushes all previously queued commands in a transaction and restores the connection state to normal.
; If WATCH was used, DISCARD unwatches all keys watched by the connection.
; ***************************************
; Entries for xider:
;
; N/A
;
; Return (when called by external library)
;  0				Ok
; -3				No current transaction
; ***************************************
DISCARD ;
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","DISCARD"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("DISCARD received")
	;
	; error out if a MULTI is not currently in progress
	if '$get(xiderMulti) do  goto DISCARDquit
	. if socketResp set xider("ret")="-ERR DISCARD without MULTI"_CRLF
	. else  set ret=-3
	;
	kill xiderMulti,xiderWatch,xiderCmd
	;
	if socketResp set xider("ret")="+OK"_CRLF
	else  set ret=0
	;
DISCARDquit ;
	set:'$quit xider("status")=ret
	quit:$quit ret quit
	;
	;
