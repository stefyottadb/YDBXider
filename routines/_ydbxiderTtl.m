;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; ***********************************
; TTL manager
; ***********************************
;
; -----------------------------------
; add(nodeName,TTL)
; -----------------------------------
; adds the TTL to a node. It will delete eventual existing TTL
; Params:
; nodeName			The $name of the node you want to process
; TTL				The TTL in Unix time (in microseconds) unsigned integer
;
add(nodeName,TTL)
	new ttlIx
	;
	do:$data(^%ydbxider("TTL","IX",nodeName)) remove(nodeName)
	;
	set ttlIx=$order(^%ydbxider("TTL","DATA",TTL,""))
	set ^%ydbxider("TTL","DATA",TTL,$increment(ttlIx))=nodeName
	set ^%ydbxider("TTL","IX",nodeName)=TTL_UPA_ttlIx
	;
	quit
	;
	;
; -----------------------------------
; remove(nodeName)
; -----------------------------------
; removes the TTL from a node.
; Params:
; nodeName			The $name of the node you want to process
;
remove(nodeName)
	new pointer
	;
	set pointer=$get(^%ydbxider("TTL","DATA"))
	kill:$zlength(pointer) ^%ydbxider("TTL","IX",nodeName),^%ydbxider("TTL","DATA",$zpiece(pointer,UPA),$zpiece(pointer,UPA,2))
	;
	quit
	;
	;
; -----------------------------------
; process
; -----------------------------------
; Process the TTL and deletes the records according.
; Additionally, it will update the record counter
;
process
	new ut,ix
	;
	set ut=$zut for  set ut=$order(^%ydbxider("TTL","DATA",ut),-1) quit:'$zlength(ut)  do
	. set ix=0 for  set ix=$order(^%ydbxider("TTL","DATA",ut,ix)) quit:'$zlength(ix)  do
	. . ; extract the data type
	. . set data=^%ydbxider("TTL","DATA",ut,ix)
	. . ;
	. . ; deletes the data
	. . kill ^%ydbxider("TTL","IX",^%ydbxider("TTL","DATA",ut,ix)),^%ydbxider("TTL","DATA",ut,ix),@data
	. . ;
	. . ; and decrease the counter
	. . set ix=$increment(^%ydbxider("KEYS"),-1)
	. . do:%ydbxiderParams("logging")>=logVERBOSE&'%ydbxiderParams("testMode") log^%ydbxiderLogger("TTL removed: "_data)
	;
	;
	quit
	;
	;
