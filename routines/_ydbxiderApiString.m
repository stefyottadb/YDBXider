;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; -----------------------------------------------
; This is the <STRING> data type data layer.
; -----------------------------------------------
;
; ***************************************
; SET
; Set key to hold the string value. If key already holds a value, it is overwritten, regardless of its type.
; Any previous time to live associated with the key is discarded on successful SET operation.
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
; ("value")			String (up to 1Mib)
; OR
; ("value",n)			If string is > 1Mib, chunks of 1Mib in each n subscript (1 based). value is EMPTY
; ("params","EX")		Number (seconds)
; ("params","PX")		Number (milliseconds)
; ("params","EXAT")		Number (seconds)
; ("params","PXAT")		Number (milliseconds)
; ("params","NX")		Empty string
; ("params","XX")		Empty string
; ("params","KEEPTTL")		Empty string
; ("params","GET")		Empty string
;
; Return (when called by external library)
;  2				string in xider("ret",nnn)
;  1				string in xider("ret")
;  0				Ok
; -1				no key provided
; -2				no value provided
; -3				NX, exists
; -4				XX, not exists
; -5				GET, not exists
; -6				Key too long
; -7				key not a string
; ***************************************
SET ; Set key to hold the string value
	new ret,socketResp
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","SET"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("SET received")
	;
	goto:noParamsValidation SETcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto SETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-6 goto SETquit
	;
	if '$data(xider("value")) do  goto SETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-2
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto SETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="SET",ret=0
	;
SETcontinue ;
	new exists,oldVal,ttlFlag
	set ret="",ttlFlag=0,exists=$data(^%ydbxiderK(xider("key"))) ; used to flag pre-existence of a key
	;
	; process NX and XX flags
	if $data(xider("params","NX")),exists do  goto SETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-3
	if $data(xider("params","XX")),'exists do  goto SETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-4
	;
	; prepare tmp string for GET flag
	merge:$data(xider("params","GET")) oldVal=^%ydbxiderK(xider("key"))
	;
	; delete eventual TTL according to the KEEPTTL flag
	do:$data(^%ydbxiderKT(xider("key")))&('$data(xider("params","KEEPTTL")))
	. ; kill ttl records and node
	. do remove^%ydbxiderTtl($name(^%ydbxiderK(xider("key"))))
	. kill ^%ydbxiderKT(xider("key"))
	;
	; set the value (we use merge to accomodate eventual 1M chunks on sub subscripts)
	if $data(xider("value",1))=1 merge ^%ydbxiderK(xider("key"))=xider("value")
	else  set ^%ydbxiderK(xider("key"))=xider("value")
	set ^%ydbxiderKDT(xider("key"))="string"
	;
	; process TTL flags
	set:$data(xider("params","EX")) ^%ydbxiderKT(xider("key"))=$zut+(1E6*xider("params","EX")),ttlFlag=1
	set:$data(xider("params","PX")) ^%ydbxiderKT(xider("key"))=$zut+(1E3*xider("params","PX")),ttlFlag=1
	set:$data(xider("params","EXAT")) ^%ydbxiderKT(xider("key"))=xider("params","EXAT")*1E6,ttlFlag=1
	set:$data(xider("params","PXAT")) ^%ydbxiderKT(xider("key"))=xider("params","PXAT")*1E3,ttlFlag=1
	do:ttlFlag add^%ydbxiderTtl($name(^%ydbxiderK(xider("key"))),^%ydbxiderKT(xider("key")))
	;
	if $data(xider("params","GET")) do
	. if '$data(oldVal) do  quit
	. . if socketResp set xider("ret")="$-1"_CRLF
	. . else  set ret=-5
	. if $data(oldVal(1))=1 merge xider("ret")=oldVal kill xider("ret","TTL"),xider("ret","DT") set ret=2
	. else  do
	. . if socketResp set xider("ret")="$"_$zlength(oldVal)_CRLF_oldVal_CRLF
	. . else  set xider("ret")=oldVal
	. . set ret=1
	else  do
	. if socketResp set xider("ret")="+OK"_CRLF
	. else  set ret=0
	;
SETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; GET
; Get the value of key. If the key does not exist the special value nil is returned.
; An error is returned if the value stored at key is not a string, because GET only handles string values.
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
;  2				string in xider("ret",nnn)
;  1				string in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				key not found
; -4				key is not a string
; ***************************************
GET ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","GET"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("GET received")
	;
	goto:noParamsValidation GETcontinue
	;
	; verify params
	if '$zlength($get(xider("key"))) do  goto GETquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto GETquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto GETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="GET",ret=0
	;
GETcontinue ;
	set ret=""
	;
	if $data(^%ydbxiderK(xider("key"))) do
	. if ^%ydbxiderKDT(xider("key"))'="string" do  quit
	. . if socketResp set xider("ret")=wrongDataType_CRLF
	. . else  set ret=-4
	. if 'socketResp do
	. . if $data(^%ydbxiderK(xider("key"),1))=1 merge xider("ret")=^%ydbxiderK(xider("key")) set ret=2
	. . else  set xider("ret")=^%ydbxiderK(xider("key")),ret=1
	. else  set xider("ret")="$"_$zlength(^%ydbxiderK(xider("key")))_CRLF_^%ydbxiderK(xider("key"))_CRLF
	else  do
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-3
	;
GETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret quit
	;
	;
; ***************************************
; DEL
; Removes the specified keys. A key is ignored if it does not exist.
; ***************************************
; Entries for xider:
;
; ("keys",n)			String
;
; Return (when called by external library)
;  >-1				number of deleted records
; -1				no keys provided
; -2				one of the Keys is too long
; ***************************************
DEL ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","DEL"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("DEL received:")
	;
	goto:noParamsValidation DELcontinue
	;
	; verify that keys are passed
	if '$data(xider("keys")) do  goto DELquit
	. if socketResp set xider("ret")="-no keys"_CRLF
	. else  set ret=-1
	;
	; verify multiple keys length
	if '$$verifyKeysLength^%ydbxiderAPI() set ret=-2 goto DELquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto DELquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="DEL",ret=0
	;
DELcontinue ;
	new cnt,ix
	set (cnt,ix)=0,ret=""
	;
	for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)  do:$data(^%ydbxiderK(xider("keys",ix)))
	. ; increment result counter and get name for TTL delete
	. set:$increment(cnt) node=$name(^%ydbxiderK(xider("keys",ix)))
	. ; kill node
	. kill ^%ydbxiderK(xider("keys",ix))
	. ; remove ttl
	. do:$data(^%ydbxiderKT(xider("keys",ix))) remove^%ydbxiderTtl(node)
	;
	if socketResp set xider("ret")=":"_cnt_CRLF
	else  set ret=cnt
	;
DELquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; EXISTS
; Returns if key exists.
; ***************************************
; Entries for xider:
;
; ("keys",n)			String
;
; Return (when called by external library)
;  >-1				number of existing key
; -1				no keys provided
; -2				one of the Keys is too long
; ***************************************
EXISTS ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","EXISTS"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("EXISTS received:")
	;
	goto:noParamsValidation EXISTScontinue
	;
	; verify that keys are passed
	if '$data(xider("keys")) do  goto EXISTSquit
	. if socketResp set xider("ret")="-no keys"_CRLF
	. else  set ret=-1
	;
	; verify multiple keys length
	if '$$verifyKeysLength^%ydbxiderAPI() set ret=-2 goto EXISTSquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto EXISTSquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="EXISTS",ret=0
	;
EXISTScontinue ;
	new cnt,ix
	set (cnt,ix)=0,ret=""
	;
	for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)  set:$data(^%ydbxiderK(xider("keys",ix))) cnt=cnt+1
	;
	if socketResp set xider("ret")=":"_cnt_CRLF
	else  set ret=cnt
	;
EXISTSquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; APPEND
; If key already exists and is a string, this command appends the value at the end of the string.
; If key does not exist it is created and set as an empty string, so APPEND will be similar to SET in this special case.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("value")			String
;
; Return (when called by external library)
;  >0				New string length
; -1				no key provided
; -2				no value provided
; -3				Key too long
; -4				Key is not a string
; ***************************************
APPEND ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","APPEND"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("APPEND received:")
	;
	goto:noParamsValidation APPENDcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto APPENDquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-3 goto APPENDquit
	;
	if '$data(xider("value")) do  goto APPENDquit
	. if socketResp set xider("ret")="-no value"_CRLF
	. else  set ret=-2
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto APPENDquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="APPEND",ret=0
	;
APPENDcontinue ;
	new valueLength,ix,oldIsArray,newIsArray,oldIx
	set oldLength=0,ret=""
	;
	; verify that key is string data type
	if $data(^%ydbxiderK(xider("key"))),^%ydbxiderKDT(xider("key"))'="string" do  goto APPENDquit
	. if socketResp set xider("ret")="-key is not string"_CRLF
	. else  set ret=-4
	;
	; compute value length
	set newIsArray=$data(xider("value",1))
	if newIsArray do
	. ; it is an array of strings
	. set valueLength=0,ix="" for  set ix=$order(xider("value",ix)) quit:'$zlength(ix)  set valueLength=valueLength+$zlength(xider("value",ix))
	; regular string
	else  set valueLength=$zlength(xider("value"))
	;
	; perform the append
	;
	if $data(^%ydbxiderK(xider("key"))) do
	. ; ------------------
	. ; key already exists
	. ; ------------------
	. ;
	. ; verify that key is string data type
	. if ^%ydbxiderKDT(xider("key"))'="string" set xider("ret")="-key is not string" goto APPENDquit
	. ;
	. ; compute length
	. set oldIsArray=$data(^%ydbxiderK(xider("key"),1))
	. ;
	. if oldIsArray do
	. . ; it is an array of strings
	. . set ix="" for  set ix=$order(^%ydbxiderK(xider("key"),ix)) quit:'+ix  set valueLength=valueLength+$zlength(^%ydbxiderK(xider("key"),ix))
	. ; regular string
	. else  set oldLength=$zlength(^%ydbxiderK(xider("key")))
	. ;
	. ; append
	. if oldLength+valueLength<=oneMib set ^%ydbxiderK(xider("key"))=^%ydbxiderK(xider("key"))_xider("value")
	. else  do
	. . ;
	. . set:oldIsArray&'newIsArray ix=$order(^%ydbxiderK(xider("key"),1),1),^%ydbxiderK(xider("key"),ix+1)=xider("value")
	. . set:'oldIsArray&'newIsArray ^%ydbxiderK(xider("key"),1)=^%ydbxiderK(xider("key")),^%ydbxiderK(xider("key"),2)=xider("value") zkill ^%ydbxiderK(xider("key"))
	. . ;
	. . do:'oldIsArray&newIsArray
	. . . set ^%ydbxiderK(xider("key"),1)=^%ydbxiderK(xider("key"))
	. . . set ix="" for  set ix=$order(xider("value",ix)) quit:'$zlength(ix)  set ^%ydbxiderK(xider("key"),ix+1)=xider("value",ix)
	. . . zkill ^%ydbxiderK(xider("key"))
	. . ;
	. . do:oldIsArray&newIsArray
	. . . set oldIx=$order(^%ydbxiderK(xider("key"),""),-1)
	. . . set ix="" for  set ix=$order(xider("value",ix)) quit:'$zlength(ix)  set ^%ydbxiderK(xider("key"),ix+oldIx)=xider("value",ix)
	else  do
	. ; ------------------
	. ; create a new entry
	. ; ------------------
	. if $data(xider("value",1))=1 merge ^%ydbxiderK(xider("key"))=xider("value")
	. else  set ^%ydbxiderK(xider("key"))=xider("value"),valueLength=$zlength(xider("value"))
	. set ^%ydbxiderKDT(xider("key"))="string"
	;
	; return
	if socketResp set xider("ret")=":"_(oldLength+valueLength)_CRLF
	else  set ret=oldLength+valueLength
	;
APPENDquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; STRLEN
; Returns the length of the string value stored at key.
; An error is returned when key holds a non-string value.
; ***************************************
; Entries for xider:
;
; ("key")			String
;
; Return (when called by external library)
;  >0				String length
; -1				no key provided
; -2				Key too long
; -3				Key doesn't exists
; -4				Key is not a string
; ***************************************
STRLEN ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","STRLEN"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("STRLEN received:")
	;
	goto:noParamsValidation STRLENcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto STRLENquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto STRLENquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto STRLENquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="STRLEN",ret=0
	;
STRLENcontinue ;
	set ret=""
	;
	; check node existance
	if '$data(^%ydbxiderK(xider("key"))) do  goto STRLENquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-3
	;
	; verify that key is string data type
	if ^%ydbxiderKDT(xider("key"))'="string" do  goto STRLENquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-4
	;
	; proceed
	if '$data(^%ydbxiderK(xider("key"),1)) do
	. if socketResp set xider("ret")=":"_$zlength(^%ydbxiderK(xider("key")))_CRLF
	. else  set ret=$zlength(^%ydbxiderK(xider("key")))
	else  do
	. new ix,cnt
	. ; multiple 1M chunks
	. set (cnt,ix)=0 for  set ix=$order(^%ydbxiderK(xider("key"),ix)) quit:'+ix  set cnt=cnt+$zlength(^%ydbxiderK(xider("key"),ix))
	. if socketResp set xider("ret")=":"_cnt_CRLF
	. else  set ret=cnt
	;
STRLENquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; GETRANGE
; Returns the substring of the string value stored at key, determined by the offsets start and end (both are inclusive).
; Negative offsets can be used in order to provide an offset starting from the end of the string.
; So -1 means the last character, -2 the penultimate and so forth.
; The function handles out of range requests by limiting the resulting range to the actual length of the string.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("start")			Number
; ("end")			Number
;
; Return (when called by external library)
;  2				String in xider("ret",nnn)
;  1				String in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				no valid start
; -4				no valid end
; -5				key doesn't exists
; -6				key is not string
; ***************************************
GETRANGE ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","GETRANGE"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("GETRANGE received")
	;
	goto:noParamsValidation GETRANGEcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto GETRANGEquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto GETRANGEquit
	;
	if '+$get(xider("start")) do  goto GETRANGEquit
	. if socketResp set xider("ret")=valueNotAnInteger_CRLF
	. else  set ret=-3
	if '+$get(xider("end")) do  goto GETRANGEquit
	. if socketResp set xider("ret")=valueNotAnInteger_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto GETRANGEquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="GETRANGE",ret=0
	;
GETRANGEcontinue ;
	new length
	set ret=""
	;
	; check existance
	if '$data(^%ydbxiderK(xider("key"))) do  goto GETRANGEquit
	. if socketResp set xider("ret")="$0"_CRLF_CRLF
	. else  set ret=-5
	;
	; verify that key is string data type
	if ^%ydbxiderKDT(xider("key"))'="string" do  goto GETRANGEquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-6
	;
	; adjust negative numbers
	do:0>xider("start")!(0>xider("end"))
	. set length=$zlength(^%ydbxiderK(xider("key")))
	. set:0>xider("start") xider("start")=length+xider("start")+1
	. set:0>xider("end") xider("end")=length+xider("end")+1
	;
	set ret=$zextract(^%ydbxiderK(xider("key")),xider("start"),xider("end"))
	if socketResp set xider("ret")="$"_$zlength(ret)_CRLF_ret_CRLF
	else  set xider("ret")=ret,ret=1
	;
GETRANGEquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; SETRANGE
; Overwrites part of the string stored at key, starting at the specified offset, for the entire length of value.
; If the offset is larger than the current length of the string at key, the string is padded with zero-bytes to make offset fit.
; Non-existing keys are considered as empty strings, so this command will make sure it holds a string large enough to be able
; to set value at offset.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("offset")			Number
; ("value")			String
;
; Return (when called by external library)
; >0				New string length after modification
; -1				no key provided
; -2				Key too long
; -3				no valid offset
; -4				no value
; -5				key is not string
; ***************************************
SETRANGE ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","SETRANGE"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("SETRANGE received")
	;
	goto:noParamsValidation SETRANGEcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto SETRANGEquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto SETRANGEquit
	;
	if '+$get(xider("offset")) do  goto SETRANGEquit
	. if socketResp set xider("ret")=valueNotAnInteger_CRLF
	. else  set ret=-3
	if '$zlength($get(xider("value"))) do  goto SETRANGEquit
	. if socketResp set xider("ret")="-no value"
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto SETRANGEquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="SETRANGE",ret=0
	;
SETRANGEcontinue ;
	new str,value
	set ret=""
	;
	; verify that key is string data type
	if $data(^%ydbxiderK(xider("key"))),^%ydbxiderKDT(xider("key"))'="string" do  goto GETRANGEquit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-5
	;
	set str=$get(^%ydbxiderK(xider("key")))
	if $zlength(str)>=xider("offset") set value=$zextract(str,1,xider("offset"))_xider("value")_$extract(str,xider("offset")+$zlength(xider("value"))+1,$zlength(str))
	else  do
	. ; existing string is < than offset, padd it with ASCII(0)
	. set blank="" for ix=$zlength(str):1:xider("offset")-1 set blank=blank_$zchar(0)
	. set value=str_blank_xider("value")
	;
	; update / create entry
	set xider("value")=value
	set ret=$$SET^xider
	;
	; return value
	if socketResp set xider("ret")=":"_$zlength(value)_CRLF
	else  set ret=$zlength(value)
	;
SETRANGEquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; RENAME
; Renames key to newkey. It returns an error when key does not exist.
; If newkey already exists it is overwritten, when this happens RENAME executes an implicit DEL operation, so if the deleted
; key contains a very big value it may cause high latency even if RENAME itself is usually a constant-time operation.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("newkey")			String
;
; Return (when called by external library)
;  0				Success
; -1				no key provided
; -2				Key too long
; -3				no newkey provided
; -4				newKey too long
; -5				key not found
; ***************************************
RENAME ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","RENAME"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("RENAME received")
	;
	goto:noParamsValidation RENAMEcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto RENAMEquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto RENAMEquit
	;
	if '$zlength($get(xider("newkey")))  do  goto RENAMEquit
	. if socketResp set xider("ret")="-no newkey"_CRLF
	. else  set ret=-3
	if '$$verifyKeyLength^%ydbxiderAPI(xider("newkey")) set ret=-4 goto RENAMEquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto RENAMEquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="RENAME",ret=0
	;
RENAMEcontinue ;
	new oldkey
	set ret=""
	;
	; error out if key doesn't exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto RENAMEquit
	. if socketResp set xider("ret")="-ERR no such key"_CRLF
	. else  set ret=-5
	;
	set xider("value")=^%ydbxiderK(xider("key"))
	set oldkey=xider("key"),xider("key")=xider("newkey")
	set ret=$$SET^xider
	;
	set xider("keys",1)=oldkey
	set ret=$$DEL^xider,ret=0
	;
	set:socketResp xider("ret")="+OK"_CRLF
	;
RENAMEquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; RENAMENX
; Renames key to newkey if newkey does not yet exist. It returns an error when key does not exist.
; ***************************************
; Entries for xider:
;
; ("key")			String
; ("newkey")			String
;
; Return (when called by external library)
;  0				Success
; -1				no key provided
; -2				Key too long
; -3				no newkey provided
; -4				newKey too long
; -5				key not found
; -6				newkey already exists
; ***************************************
RENAMENX ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","RENAMENX"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("RENAMENX received")
	;
	goto:noParamsValidation RENAMENXcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto RENAMENXquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto RENAMENXquit
	;
	if '$zlength($get(xider("newkey")))  do  goto RENAMENXquit
	. if socketResp set xider("ret")="-no newkey"_CRLF
	. else  set ret=-3
	if '$$verifyKeyLength^%ydbxiderAPI(xider("newkey")) set ret=-4 goto RENAMENXquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto RENAMENXquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="RENAMENX",ret=0
	;
RENAMENXcontinue ;
	new oldkey
	set ret=""
	;
	; error out if key doesn't exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto RENAMENXquit
	. if socketResp set xider("ret")="-no such key"_CRLF
	. else  set ret=-5
	;
	; error out if newkey doesn't exists
	if $data(^%ydbxiderK(xider("newkey"))) do  goto RENAMENXquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-6
	;
	set xider("value")=^%ydbxiderK(xider("key"))
	set oldkey=xider("key"),xider("key")=xider("newkey")
	set ret=$$SET^xider
	;
	set xider("keys",1)=oldkey
	set ret=$$DEL^xider
	;
	if socketResp set xider("ret")=":1"_CRLF
	else  set ret=0
	;
RENAMENXquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; GETDEL
; Get the value of key and delete the key.
; This command is similar to GET, except for the fact that it also deletes the key on success
; (if and only if the key's value type is a string).
; ***************************************
; Entries for xider:
;
; ("key")			String
;
;  0				Success
; -1				no key provided
; -2				Key too long
; -3				key not found
; ***************************************
GETDEL ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","GETDEL"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("GETDEL received")
	;
	goto:noParamsValidation GETDELcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto GETDELquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto GETDELquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto GETDELquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="GETDEL",ret=0
	;
GETDELcontinue ;
	new keyValue
	set ret=""
	;
	; error out if key doesn't exists
	if '$data(^%ydbxiderK(xider("key"))) do  goto GETDELquit
	. if socketResp set xider("ret")="$-1"_CRLF
	. else  set ret=-3
	;
	if ^%ydbxiderKDT(xider("key"))'="string" do  quit
	. if socketResp set xider("ret")=wrongDataType_CRLF
	. else  set ret=-4
	;
	; execute the get
	set ret=$$GET^xider
	merge keyValue=xider("ret")
	;
	; verify operation
	set:-1<ret xider("keys",1)=xider("key"),ret=$$DEL^xider
	;
	; return value
	merge xider("ret")=keyValue
	set ret=$select($data(keyValue(1)):2,1:1)
	;
	;
GETDELquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; MSET
; Sets the given keys to their respective values.
; MSET replaces existing values with new values, just as regular SET. See MSETNX if you don't want to overwrite existing values.
; ***************************************
; Entries for xider:
;
; ("keys",nnn)			String
; ("values",nnn)		String
;
; Return (when called by external library)
;  0				Success
; -1				no keys provided
; -2				one of the Keys is too long
; -3				one of the keys has no value
; ***************************************
MSET ;
	new ix,socketResp,err,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","MSET"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("MSET received:")
	;
	goto:noParamsValidation MSETcontinue
	;
	; verify that keys are passed
	if '$data(xider("keys")) do  goto MSETquit
	. if socketResp set xider("ret")="-no keys"_CRLF
	. else  set ret=-1
	;
	; verify multiple keys length
	if '$$verifyKeysLength^%ydbxiderAPI() set ret=-2 goto MSETquit
	;
	; verify that all keys have data
	set ix="",err=0 for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)!err  set:'$zlength($get(xider("values",ix))) err=ix
	;
	if err do  goto MSETquit
	. if socketResp set xider("ret")="-key "_err_" has no value"_CRLF
	. else  set ret=-3
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto MSETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="MSET",ret=0
	;
MSETcontinue ;
	new exists
	set ret=""
	;
	; execute the SETs
	set ix="" for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)  do
	. ; flag the exist status for the counters
	. set exists=$data(^%ydbxiderK(xider("keys",ix)))
	. ;
	. ; set the value (we use merge to accomodate eventual 1M chunks on sub subscripts)
	. if $data(xider("values",ix,1))=1 merge ^%ydbxiderK(xider("keys",ix))=xider("values",ix)
	. else  set ^%ydbxiderK(xider("keys",ix))=xider("values",ix)
	. set ^%ydbxiderKDT(xider("keys",ix))="string"
	;
	if socketResp set xider("ret")="+OK"_CRLF
	else  set ret=0
	;
MSETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; MSETNX
; Sets the given keys to their respective values.
; MSETNX will not perform any operation at all even if just a single key already exists.
; Because of this semantic MSETNX can be used in order to set different keys representing different fields
; of a unique logic object in a way that ensures that either all the fields or none at all are set.
; ***************************************
; Entries for xider:
;
; ("keys",nnn)			String
; ("values",nnn)		String
;
; Return (when called by external library)
;  0				Success
; -1				no keys provided
; -2				one of the Keys is too long
; -3				one of the keys has no value
; -4				at least one key already exists
; ***************************************
MSETNX ;
	new ix,socketResp,err,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","MSETNX"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("MSETNX received:")
	;
	goto:noParamsValidation MSETNXcontinue
	;
	; verify that keys are passed
	if '$data(xider("keys")) do  goto MSETNXquit
	. if socketResp set xider("ret")="-no keys"_CRLF
	. else  set ret=-1
	;
	; verify multiple keys length
	if '$$verifyKeysLength^%ydbxiderAPI() set ret=-2 goto MSETNXquit
	;
	; verify that all keys have data
	set ix="",err=0 for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)!err  set:'$zlength($get(xider("values",ix))) err=ix
	;
	if err do  goto MSETquit
	. if socketResp set xider("ret")="-key "_err_" has no value"_CRLF
	. else  set ret=-3
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto MSETNXquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="MSETNX",ret=0
	;
MSETNXcontinue ;
	new exists
	set ret=""
	;
	; verify that all keys do not exists
	set ix="",exists=0 for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)!exists  set:$data(^%ydbxiderK(xider("keys",ix))) exists=1
	;
	if exists do  goto MSETNXquit
	. if socketResp set xider("ret")=":0"_CRLF
	. else  set ret=-4
	;
	; execute the SETs
	set ix="" for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)  do
	. ; set the value (we use merge to accomodate eventual 1M chunks on sub subscripts)
	. if $data(xider("values",ix,1))=1 merge ^%ydbxiderK(xider("keys",ix))=xider("values",ix)
	. else  set ^%ydbxiderK(xider("keys",ix))=xider("values",ix)
	. set ^%ydbxiderKDT(xider("keys",ix))="string"
	;
	if socketResp set xider("ret")=":1"_CRLF
	else  set ret=0
	;
MSETNXquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; MGET
; Returns the values of all specified keys.
; For every key that does not hold a string value or does not exist, the special value nil is returned.
; Because of this, the operation never fails.
; ***************************************
; Entries for xider:
;
; ("keys",nnn)			String
;
; Return (when called by external library)
;  0				Success (data is in xider(ret,nnn) if key doesn't exists or is not a string, xider(ret,nnn) will be an empty string)
; -1				no keys provided
; -2				one of the Keys is too long
; ***************************************
MGET ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","MGET"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("MGET received:")
	;
	goto:noParamsValidation MGETcontinue
	;
	; verify that keys are passed
	if '$data(xider("keys")) do  goto MGETquit
	. if socketResp set xider("ret")="-no keys"_CRLF
	. else  set ret=-1
	;
	; verify multiple keys length
	if '$$verifyKeysLength^%ydbxiderAPI() set ret=-2 goto MGETquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto MGETquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="MGET",ret=0
	;
MGETcontinue ;
	new cnt,ix
	set (cnt,ret)=0
	;
	set:socketResp xider("ret")=""
	set ix="" for  set ix=$order(xider("keys",ix)) quit:'$zlength(ix)  do
	. set cnt=cnt+1
	. if $data(^%ydbxiderK(xider("keys",ix))),^%ydbxiderKDT(xider("keys",ix))="string" do
	. . if 'socketResp do
	. . . if $data(^%ydbxiderK(xider("keys",ix)))=1 set xider("ret",cnt)=^%ydbxiderK(xider("keys",ix))
	. . . else  merge xider("ret",cnt)=^%ydbxiderK(xider("keys",ix))
	. . else  set xider("ret")=xider("ret")_"$"_$zlength(^%ydbxiderK(xider("keys",ix)))_CRLF_^%ydbxiderK(xider("keys",ix))_CRLF
	. else  do
	. . if socketResp set xider("ret")=xider("ret")_"$-1"_CRLF
	. . else  set xider("ret",cnt)=""
	if socketResp set xider("ret")="*"_cnt_CRLF_xider("ret")
	else  set xider("ret")=cnt
	;
MGETquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; INCR
; Increments the number stored at key by one.
; If the key does not exist, it is set to 0 before performing the operation.
; An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				value not an integer
; -4				data-type not a string
; ***************************************
INCR ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","INCR"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("INCR received:")
	;
	goto:noParamsValidation INCRcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto INCRquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto INCRquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto INCRquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="INCR",ret=0
	;
INCRcontinue ;
	new err
	set err=0,ret=""
	;
	if $data(^%ydbxiderK(xider("key"))) do  goto:err INCRquit
	. if ^%ydbxiderKDT(xider("key"))'="string" set err=1 do  quit
	. . if socketResp set xider("ret")=wrongDataType_CRLF
	. . else  set ret=-4
	. if '$$isInteger^%ydbxiderAPI(^%ydbxiderK(xider("key"))) set err=1 do
	. . if socketResp set xider("ret")="-value not a integer"_CRLF
	. . else  set ret=-3
	else  set xider("value")=0 set ret=$$SET^xider
	;
	set xider("ret")=$increment(^%ydbxiderK(xider("key"))),ret=0
	set:socketResp xider("ret")=":"_xider("ret")_CRLF
	;
INCRquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; INCRBY
; Increments the number stored at key by increment.
; If the key does not exist, it is set to 0 before performing the operation.
; An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
; ("increment")			signed integer
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				no increment provided
; -4				increment not an integer
; -5				value not an integer
; -6				data-type not a string
; ***************************************
INCRBY ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","INCRBY"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("INCRBY received:")
	;
	goto:noParamsValidation INCRBYcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto INCRBYquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto INCRBYquit
	;
	if '$zlength($get(xider("increment")))  do  goto INCRBYquit
	. if socketResp set xider("ret")="-no increment"_CRLF
	. else  set ret=-3
	if '$$isInteger^%ydbxiderAPI(xider("increment")) do  goto INCRBYquit
	. if socketResp set xider("ret")="-increment not an integer"_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto INCRBYquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="INCRBY",ret=0
	;
INCRBYcontinue ;
	new err
	set err=0,ret=""
	;
	if $data(^%ydbxiderK(xider("key"))) do  goto:err INCRquit
	. if $get(^%ydbxiderKDT(xider("key")))'="string"  set err=1 do  quit
	. . if socketResp set xider("ret")=wrongDataType_CRLF
	. . else  set ret=-6
	. if '$$isInteger^%ydbxiderAPI(^%ydbxiderK(xider("key"))) set err=1 do
	. . if socketResp set xider("ret")="ERR value is not an integer or out of range"_CRLF
	. . else  set ret=-5
	else  set xider("value")=0 set ret=$$SET^xider
	;
	set xider("ret")=$increment(^%ydbxiderK(xider("key")),xider("increment")),ret=0
	set:socketResp xider("ret")=":"_xider("ret")_CRLF
	;
INCRBYquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; INCRBYFLOAT
; Increments the number stored at key by increment.
; If the key does not exist, it is set to 0 before performing the operation.
; An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
; ("increment")			floating point number
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				no increment provided
; -4				increment not a floating point
; -5				value not a floating point
; -6				data-type not a string
; ***************************************
INCRBYFLOAT ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","INCRBYFLOAT"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("INCRBYFLOAT received:")
	;
	goto:noParamsValidation INCRBYFLOATcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto INCRBYFLOATquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto INCRBYFLOATquit
	;
	if '$zlength($get(xider("increment")))  do  goto INCRBYFLOATquit
	. if socketResp set xider("ret")="-no increment"_CRLF
	. else  set ret=-3
	set:$zextract(xider("increment"),1,2)="0." xider("increment")=$zextract(xider("increment"),2,$zlength(xider("increment")))
	;
	if '$$isFloat^%ydbxiderAPI(xider("increment")) do  goto INCRBYFLOATquit
	. if socketResp set xider("ret")="-increment not a double"_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto INCRBYFLOATquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="INCRBYFLOAT",ret=0
	;
INCRBYFLOATcontinue ;
	new err
	set err=0,ret=""
	;
	if $data(^%ydbxiderK(xider("key"))) do  goto:err INCRBYFLOATquit
	. if $get(^%ydbxiderKDT(xider("key")))'="string"  set err=1 do  quit
	. . if socketResp set xider("ret")=wrongDataType_CRLF
	. . else  set ret=-6
	. if '$$isFloat^%ydbxiderAPI(^%ydbxiderK(xider("key"))) set err=1 do
	. . if socketResp set xider("ret")="-ERR value is not a double or out of range"_CRLF
	. . else  set ret=-5
	else  set xider("value")=0 set ret=$$SET^xider
	;
	set xider("ret")=$increment(^%ydbxiderK(xider("key")),xider("increment")),ret=0
	set:socketResp xider("ret")=","_xider("ret")_CRLF
	;
INCRBYFLOATquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; DECR
; Decrements the number stored at key by one.
; If the key does not exist, it is set to 0 before performing the operation.
; An error is returned if the key contains a value of the wrong type or contains a string that can not be represented as integer.
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				value not an integer
; -4				data-type not a string
; ***************************************
DECR ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","DECR"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("DECR received:")
	;
	goto:noParamsValidation DECRcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto DECRquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto DECRquit
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto DECRquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="DECR",ret=0
	;
DECRcontinue ;
	new err
	set err=0,ret=""
	;
	if $data(^%ydbxiderK(xider("key"))) do  goto:err DECRquit
	. if $get(^%ydbxiderKDT(xider("key")))'="string"  set err=1 do  quit
	. . if socketResp set xider("ret")=wrongDataType_CRLF
	. . else  set ret=-4
	. if '$$isInteger^%ydbxiderAPI(^%ydbxiderK(xider("key"))) set err=1 do
	. . if socketResp set xider("ret")="-value not a integer"_CRLF
	. . else  set ret=-3
	else  set xider("value")=0 set ret=$$SET^xider
	;
	set xider("ret")=$increment(^%ydbxiderK(xider("key")),-1),ret=0
	set:socketResp xider("ret")=":"_xider("ret")_CRLF
	;
DECRquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
; ***************************************
; DECRBY
; The DECRBY command reduces the value stored at the specified key by the specified decrement.
; If the key does not exist, it is initialized with a value of 0 before performing the operation.
; If the key's value is not of the correct type or cannot be represented as an integer, an error is returned.
; ***************************************
; Entries for xider:
;
; ("key")			String (up to xiderSTRINGMAXKEYLENGTH)
; ("increment")			signed integer
;
; Return (when called by external library)
;  0				success, new value is in xider("ret")
; -1				no key provided
; -2				Key too long
; -3				no decrement provided
; -4				increment not an integer
; -5				value not an integer
; -6				data-type not a string
; ***************************************
DECRBY ;
	new socketResp,ret
	;
	if %ydbxiderParams("sessionStatistics") set:$increment(^%ydbxider("S",$job,"commandsCount")) ret=$increment(^%ydbxider("S",$job,"commandsCount","DECRBY"))
	;
	set socketResp=$zlength($get(CRLF)),ret=""
	;
	do:socketResp&(%ydbxiderParams("logging")>=logDEBUG)&'%ydbxiderParams("testMode") log^%ydbxiderLogger("DECRBY received:")
	;
	goto:noParamsValidation DECRBYcontinue
	;
	; verify params
	if '$zlength($get(xider("key")))  do  goto DECRBYquit
	. if socketResp set xider("ret")="-no key"_CRLF
	. else  set ret=-1
	if '$$verifyKeyLength^%ydbxiderAPI() set ret=-2 goto DECRBYquit
	;
	if '$zlength($get(xider("decrement")))  do  goto DECRBYquit
	. if socketResp set xider("ret")="-no decrement"_CRLF
	. else  set ret=-3
	if '$$isInteger^%ydbxiderAPI(xider("decrement")) do  goto DECRBYquit
	. if socketResp set xider("ret")="-decrement not an integer"_CRLF
	. else  set ret=-4
	;
	; if we are in a transaction, just queue up the command and its parameters
	if $get(xiderMulti) do  goto DECRBYquit
	. kill xider("ret")
	. merge xiderCmd($increment(xiderCmd))=xider
	. set:socketResp xider("ret")="+QUEUED"_CRLF
	. set xiderCmd(xiderCmd)="DECRBY",ret=0
	;
DECRBYcontinue ;
	new err
	set err=0,ret=""
	;
	if $data(^%ydbxiderK(xider("key"))) do  goto:err DECRBYquit
	. if $get(^%ydbxiderKDT(xider("key")))'="string"  set err=1 do  quit
	. . if socketResp set xider("ret")=wrongDataType_CRLF
	. . else  set ret=-6
	. if '$$isInteger^%ydbxiderAPI(^%ydbxiderK(xider("key"))) set err=1 do
	. . if socketResp set xider("ret")="ERR value is not an integer or out of range"_CRLF
	. . else  set ret=-5
	else  set xider("value")=0 set ret=$$SET^xider
	;
	set xider("ret")=$increment(^%ydbxiderK(xider("key")),-xider("decrement")),ret=0
	set:socketResp xider("ret")=":"_xider("ret")_CRLF
	;
DECRBYquit ;
	set:ret<0&$get(xiderMulti) xiderMulti=-1
	set:'$quit xider("status")=ret
	quit:$quit ret  quit
	;
	;
