;#################################################################
;#                                                               #
;# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
;# All rights reserved.                                          #
;#                                                               #
;#  This source code contains the intellectual property	         #
;#  of its copyright holder(s), and is made available            #
;#  under a license.  If you do not know the terms of            #
;#  the license, please stop and do not read further.            #
;#                                                               #
;#################################################################
;
; -----------------------------------------------
; This is the <COMMAND> namespace.
; -----------------------------------------------
;
; ***************************************
; COMMAND
; ***************************************
;
; No parameters are passed
;
; Response is: server supported commands and related parameters
;
;
COMMAND ; RESP response for COMMAND INFO <cmd>
	; The tabs self-document the nested array structure
	new ret
	;
	set ret="*1"_CRLF
	;
	; SET
	set ret=ret_"*7"_CRLF
		set ret=ret_"$3"_CRLF_"set"_CRLF
		set ret=ret_":-3"_CRLF
		set ret=ret_"*2"_CRLF
			set ret=ret_"$5"_CRLF_"write"_CRLF
			set ret=ret_"$7"_CRLF_"denyoom"_CRLF
		set ret=ret_":1"_CRLF
		set ret=ret_":1"_CRLF
		set ret=ret_":1"_CRLF
		set ret=ret_"*3"_CRLF
			set ret=ret_"$5"_CRLF_"write"_CRLF
			set ret=ret_"$6"_CRLF_"string"_CRLF
			set ret=ret_"$4"_CRLF_"slow"_CRLF
	set xider("ret")=ret
	;
	do:%ydbxiderParams("logging")>=logDEBUG&'%ydbxiderParams("testMode") log^%ydbxiderLogger(ret)
	do:%ydbxiderParams("logging")>=logDEBUG&'%ydbxiderParams("testMode") log^%ydbxiderLogger("COMMAND received")
	;
	quit
	;
	;
