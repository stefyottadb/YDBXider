#################################################################
#                                                               #
# Copyright (c) 2024 YottaDB LLC and/or its subsidiaries.       #
# All rights reserved.                                          #
#                                                               #
#       This source code contains the intellectual property     #
#       of its copyright holder(s), and is made available       #
#       under a license.  If you do not know the terms of       #
#       the license, please stop and do not read further.       #
#                                                               #
#################################################################

FROM yottadb/yottadb-base:latest-master

#########################################################
# Install pre-requisites
#########################################################
RUN apt-get update && apt-get install -y curl unzip wget cmake git gcc make \
                        npm libssl-dev libconfig-dev libgcrypt-dev  \
                        libgpgme-dev libicu-dev libsodium-dev \
                        curl libcurl4-openssl-dev redis-server \
                        lua5.1 lua5.1-socket liblua5.1-dev

#########################################################
# Install lua-yottadb
#########################################################
RUN cd /tmp && git clone https://github.com/anet-be/lua-yottadb.git
RUN cd /tmp/lua-yottadb && make lua=lua5.1 && make install lua=lua5.1

#########################################################
# Initialize working directory
#########################################################
RUN mkdir -p /YDBXIDER
WORKDIR /YDBXIDER

#########################################################
# Install nodejs
#########################################################
ENV NODE_VERSION=18.19.0
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

#########################################################
# Install Encryption Plugin
#########################################################
ENV ydb_dist="/opt/yottadb/current"
ENV ydb_icu_version="70"
RUN git clone https://gitlab.com/YottaDB/Util/YDBEncrypt
RUN cd YDBEncrypt && make install

#########################################################
# Create Certificates for TLS testing
#########################################################
RUN mkdir -p /YDBXIDER/certs
RUN openssl genrsa -aes128 -passout pass:ydbxider -out /YDBXIDER/certs/ydbxider.key 2048
RUN openssl req -new -key /YDBXIDER/certs/ydbxider.key -passin pass:ydbxider -subj '/C=US/ST=Pennsylvania/L=Malvern/CN=localhost' -out /YDBXIDER/certs/ydbxider.csr
RUN openssl req -x509 -days 365 -sha256 -in /YDBXIDER/certs/ydbxider.csr -key /YDBXIDER/certs/ydbxider.key -passin pass:ydbxider -out /YDBXIDER/certs/ydbxider.pem

# Set-up YottaDB Certificate Config
COPY docker-configuration/ydbxider.ydbcrypt /YDBXIDER/certs/
ENV ydb_crypt_config=/YDBXIDER/certs/ydbxider.ydbcrypt

#########################################################
# Setup environment
#########################################################
ENV ydb_lct_stdnull=1
ENV ydb_lvnullsubs=2

# Tell Node to ignore the self-signed certificate
ENV NODE_TLS_REJECT_UNAUTHORIZED=0

# CMake Install
COPY CMakeLists.txt .
COPY _ydbxider.manifest.json.in .
COPY routines routines
COPY test test
COPY commands commands
RUN mkdir build && cd build && cmake .. && make && make install

COPY docker-configuration/dev .
COPY docker-configuration/docker-startup.sh .

#########################################################
# Install node modules
#########################################################
COPY package.json .
RUN . /opt/yottadb/current/ydb_env_set && npm install

# Mount point directories. Empty by default.
RUN mkdir objects

# Default environment
RUN echo ". /YDBXIDER/dev" >> $HOME/.bashrc

ENTRYPOINT ["/YDBXIDER/docker-startup.sh"]

# to build the image
# docker image build --progress=plain -t ydbxider .

# to run the machine
# docker run -d --init --name=ydbxider ydbxider
# docker exec -it ydbxider bash
# xider
# Test with node js/test.mjs

# to enter development mode
# (passing volumes is optional: but it lets you change the code and see the changes immediately applied on the fly)
# in Windows:
# docker run -d --init --name=ydbxider -v C:\Users\stefa\WebstormProjects\YDBXider/js:/YDBXIDER/js -v C:\Users\stefa\WebstormProjects\YDBXider/routines:/YDBXIDER/routines  -v C:\Users\stefa\WebstormProjects\YDBXider/commands:/YDBXIDER/commands ydbxider
